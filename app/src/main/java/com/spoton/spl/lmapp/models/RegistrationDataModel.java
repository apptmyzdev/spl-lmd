package com.spoton.spl.lmapp.models;

import java.util.List;

public class RegistrationDataModel {

    private String dktNo;
    private int subprojectId;
    private int routeId;
    private int vehicleId;
    private int vendorId;
    private String subprojectName;
    private String vendorName;
    private String vehicleNo;
    private String routeName;
    private NodeModel startNode;
    private NodeModel endNode;
    private List<NodeModel> nodeList;
    private int capturePod; //1 - capture pud ,0- dont capture pud
    private int podCount;


    public String getDktNo() {
        return dktNo;
    }

    public void setDktNo(String dktNo) {
        this.dktNo = dktNo;
    }

    public int getSubprojectId() {
        return subprojectId;
    }

    public void setSubprojectId(int subprojectId) {
        this.subprojectId = subprojectId;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getSubprojectName() {
        return subprojectName;
    }

    public void setSubprojectName(String subprojectName) {
        this.subprojectName = subprojectName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public NodeModel getStartNode() {
        return startNode;
    }

    public void setStartNode(NodeModel startNode) {
        this.startNode = startNode;
    }

    public NodeModel getEndNode() {
        return endNode;
    }

    public void setEndNode(NodeModel endNode) {
        this.endNode = endNode;
    }

    public List<NodeModel> getNodeList() {
        return nodeList;
    }

    public void setNodeList(List<NodeModel> nodeList) {
        this.nodeList = nodeList;
    }

    public int getCapturePod() {
        return capturePod;
    }

    public void setCapturePod(int capturePod) {
        this.capturePod = capturePod;
    }

    public int getPodCount() {
        return podCount;
    }

    public void setPodCount(int podCount) {
        this.podCount = podCount;
    }

    @Override
    public String toString() {
        return "RegistrationDataModel{" +
                "dktNo='" + dktNo + '\'' +
                ", subprojectId=" + subprojectId +
                ", routeId=" + routeId +
                ", vehicleId=" + vehicleId +
                ", vendorId=" + vendorId +
                ", subprojectName='" + subprojectName + '\'' +
                ", vendorName='" + vendorName + '\'' +
                ", vehicleNo='" + vehicleNo + '\'' +
                ", routeName='" + routeName + '\'' +
                ", startNode=" + startNode +
                ", endNode=" + endNode +
                ", nodeList=" + nodeList +
                ", capturePod=" + capturePod +
                ", podCount=" + podCount +
                '}';
    }
}
