package com.spoton.spl.lmapp.models;

public class UserLocationModel {

    private String dktNo;
    private Double latitude;
    private Double longitude;
    private String deviceTimestamp;
    private String mobileNo;
    private String imei;
    private String battery;

    public String getDktNo() {
        return dktNo;
    }

    public void setDktNo(String dktNo) {
        this.dktNo = dktNo;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getDeviceTimestamp() {
        return deviceTimestamp;
    }

    public void setDeviceTimestamp(String deviceTimestamp) {
        this.deviceTimestamp = deviceTimestamp;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }


    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    @Override
    public String toString() {
        return "UserLocationModel{" +
                "dktNo='" + dktNo + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", deviceTimestamp='" + deviceTimestamp + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", imei='" + imei + '\'' +
                ", battery='" + battery + '\'' +
                '}';
    }
}
