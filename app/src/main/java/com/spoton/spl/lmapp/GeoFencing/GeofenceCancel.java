package com.spoton.spl.lmapp.GeoFencing;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.spoton.spl.lmapp.datacache.DataSource;
import com.spoton.spl.lmapp.R;
import com.spoton.spl.lmapp.utils.Constants;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GeofenceCancel implements OnCompleteListener<Void> {

    public static final String TAG = "GeofenceCancel";
    List<String> requestIds = new ArrayList<>();
    Context mContext;
    private GoogleApiClient mGoogleApiClient;
    private List<GeofenceData> geofenceDatas = new ArrayList<>();
    private DataSource dataSource;
    private GeofencingClient mGeofencingClient;

    public void performAction(Context context, String command, int pushMessageId) {
        mContext = context;

        String message = command.substring(command.indexOf("$") + 1);
        Gson gson = new Gson();
        Type token = new TypeToken<List<GeofenceData>>() {
        }.getType();
        geofenceDatas = gson.fromJson(message, token);

//        Globals.constructJsonLog(context, Command.RECIVED_MESSAGE_CODE,
//                true, "Command Received", pushMessageId);

        dataSource = new DataSource(context);

        for (GeofenceData data : geofenceDatas) {
            requestIds.add("" + data.getGid());
        }

        mGeofencingClient = LocationServices.getGeofencingClient(context);

        if (requestIds.size() != 0) {
            mGeofencingClient.removeGeofences(requestIds).addOnCompleteListener(this);
        }

        Intent i = new Intent(context, GeofenceIntentService.class);
        context.stopService(i);

    }

    @Override
    public void onComplete(Task<Void> task) {
        if (task.isSuccessful()) {
            int messageId = R.string.geofences_added;
            Toast.makeText(mContext, mContext.getString(messageId), Toast.LENGTH_SHORT).show();
            appendLog(mContext.getString(messageId), Constants.GEOFENCE_FILE);
        } else {
            String errorMessage = GeofenceErrorMessages.getErrorString(mContext, task.getException());
            Log.w(TAG, errorMessage);
            appendLog(errorMessage, Constants.GEOFENCE_FILE);

            if (requestIds.size() != 0) {
                mGeofencingClient.removeGeofences(requestIds).addOnCompleteListener(this);
            }
        }
    }

    public void appendLog(String text, String filename) {
        File logFile = new File(filename);
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}