package com.spoton.spl.lmapp;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import com.spoton.spl.lmapp.GeoFencing.GatherLocation;
import com.spoton.spl.lmapp.GeoFencing.GeoFenceModel;
import com.spoton.spl.lmapp.GeoFencing.SetGeofence;
import com.spoton.spl.lmapp.datacache.DataSource;
import com.spoton.spl.lmapp.fileutils.ImageLoader;
import com.spoton.spl.lmapp.fileutils.MemoryCache;
import com.spoton.spl.lmapp.models.LatLng;
import com.spoton.spl.lmapp.models.NodeModel;
import com.spoton.spl.lmapp.models.ReasonsModel;
import com.spoton.spl.lmapp.models.RegistrationDataModel;
import com.spoton.spl.lmapp.models.RegistrationModel;
import com.spoton.spl.lmapp.models.UpdateNodeModel;
import com.spoton.spl.lmapp.responses.GeneralResponseModel;
import com.spoton.spl.lmapp.responses.ResgistrationResponseModel;
import com.spoton.spl.lmapp.responses.UpdateNodeResponse;
import com.spoton.spl.lmapp.utils.BitmapScalingUtil;
import com.spoton.spl.lmapp.utils.CameraActivity;
import com.spoton.spl.lmapp.utils.CommonUtils;
import com.spoton.spl.lmapp.utils.Constants;
import com.spoton.spl.lmapp.utils.Globals;
import com.spoton.spl.lmapp.utils.HttpRequest;
import com.spoton.spl.lmapp.utils.LMDUtils;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class TCDetailsScreen extends AppCompatActivity implements GetTcData.TcDataListener {
    public final int TAKE_PHOTO = 1;
    public MemoryCache memoryCache = new MemoryCache();
    Context context;
    DataSource dataSource;
    //    List<RegistrationDataModel> tcList;
    List<RegistrationDataModel> tcSearchList;

    GeneralResponseModel generalResponseModel;
    GeneralResponseModel response;
    UpdateNodeResponse updateNodeResponse;
    RegistrationModel model;
    private ListView listView;
    private TextView startTv, endTv, routeNameTv, vehicleNoTv, startNodeTv,
            endNodeTv, tcNumberTv, versionTv;
    private RelativeLayout pickupPopupRl, imagePopUpRl, reasonsPopup;
    private Button submitBtn, pickUpBtn, sendBtn, captureBtn, endBtn;
    private EditText pickUpCountEt, remarksEt, updatePacketEt;
    private String truckImageFileName;
    private int dktNodeId, nodeId, nodeUpdated = -1;
    private Boolean isEndEnabled = false;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView truckIv, logoutBtn;
    private ArrayList<String> imageFileNames;
    private List<Bitmap> bitmapList;
    private EditText exceptionTypeEt;
    private Button btnReasonSubmit;
    private List<ReasonsModel> reasonsModelList;

    private Gallery imageGallery;
    private ImageAdapter imageAdapter;
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.pickup_btn:
                    UpdateNodeModel model1;
                    if (Globals.registrationDataModel != null) {
                        if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 0) {
                            model1 = new UpdateNodeModel();
                            model1.setNodeId(Globals.registrationDataModel.getStartNode().getNodeId());
                            model1.setDktNodeId(Globals.registrationDataModel.getStartNode().getDktNodeId());
                            model1.setDktNo(Long.parseLong(Globals.registrationDataModel.getDktNo()));
                            SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            Calendar calendar = Calendar.getInstance();
                            String format = s.format(calendar.getTime());
                            if (CommonUtils.isValidString(format)) {
                                model1.setTimestamp(format);
                                Globals.action = Constants.ARRIVED;
                            }

                            GatherLocation gatherlocation = new GatherLocation();

                            Location location = gatherlocation.getLocation(context);
                            if (location != null) {
                                double lat = location.getLatitude();
                                double lng = location.getLongitude();
                                CommonUtils.logD("Lat : " + lat + " , Lng : " + lng);
                                model1.setLatitude(lat);
                                model1.setLongitude(lng);
                            }
                            new NodeUpdateTask().execute(model1);
                        } else if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 1) {
                            pickupPopupRl.setVisibility(View.VISIBLE);
                            pickUpCountEt.setText("");
                        } else if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 2) {
                            model1 = new UpdateNodeModel();
                            model1.setNodeId(Globals.registrationDataModel.getStartNode().getNodeId());
                            model1.setDktNodeId(Globals.registrationDataModel.getStartNode().getDktNodeId());
                            model1.setDktNo(Long.parseLong(Globals.registrationDataModel.getDktNo()));
                            SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            Calendar calendar = Calendar.getInstance();
                            String format = s.format(calendar.getTime());
                            if (CommonUtils.isValidString(format)) {
                                model1.setTimestamp(format);
                                Globals.action = Constants.DEPART;
                            }

                            GatherLocation gatherlocation = new GatherLocation();

                            Location location = gatherlocation.getLocation(context);
                            if (location != null) {
                                double lat = location.getLatitude();
                                double lng = location.getLongitude();
                                CommonUtils.logD("Lat : " + lat + " , Lng : " + lng);
                                model1.setLatitude(lat);
                                model1.setLongitude(lng);
                            }

                            new NodeUpdateTask().execute(model1);
                        } else if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 3) {

                        }
                    }
                    break;


                case R.id.submit_btn:
                    String count = pickUpCountEt.getText().toString();
                    if (CommonUtils.isValidString(count)) {
                        CommonUtils.dissmissKeyboard(pickUpCountEt);
                        if (Globals.registrationDataModel != null) {
                            UpdateNodeModel updateNodeModel = new UpdateNodeModel();
                            updateNodeModel.setDktNo(Long.parseLong(Globals.registrationDataModel.getDktNo()));
                            updateNodeModel.setNodeId(Globals.registrationDataModel.getStartNode().getNodeId());
                            updateNodeModel.setPktCount(Integer.parseInt(count));
                            updateNodeModel.setSubprojectId(Globals.registrationDataModel.getSubprojectId());

                            SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            Calendar calendar = Calendar.getInstance();
                            String format = s.format(calendar.getTime());
                            if (CommonUtils.isValidString(format)) {
                                updateNodeModel.setTimestamp(format);
                            }

                            GatherLocation gatherlocation = new GatherLocation();

                            Location location = gatherlocation.getLocation(context);
                            if (location != null) {
                                double lat = location.getLatitude();
                                double lng = location.getLongitude();
                                CommonUtils.logD("Lat : " + lat + " , Lng : " + lng);
                                updateNodeModel.setLatitude(lat);
                                updateNodeModel.setLongitude(lng);
                            }
//                            else{
//                                CommonUtils.showToastShort(context,getString(R.string.check_location));
//                                break;
//                            }
                            Globals.action = Constants.PICKUP;
                            new WarehousePickupTask().execute(updateNodeModel);


                        }
                    } else {
                        CommonUtils.showToastShort(context, "Please enter valid count");
                    }
                    break;


                case R.id.end_btn:

                    if (isEndEnabled) {
                        updateEndNode();
                    } else if (Globals.registrationDataModel != null && !CommonUtils.isValidArraylist(Globals.registrationDataModel.getNodeList())) {


//                        if (registrationDataModel != null) {
                        if (Globals.registrationDataModel.getEndNode().getDlyStatus() == 0) {
                            model1 = new UpdateNodeModel();
                            model1.setNodeId(Globals.registrationDataModel.getEndNode().getNodeId());
                            model1.setDktNodeId(Globals.registrationDataModel.getEndNode().getDktNodeId());
                            model1.setDktNo(Long.parseLong(Globals.registrationDataModel.getDktNo()));
                            SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            Calendar calendar = Calendar.getInstance();
                            String format = s.format(calendar.getTime());
                            if (CommonUtils.isValidString(format)) {
                                model1.setTimestamp(format);
                                Globals.action = Constants.ARRIVED;
                            }

                            GatherLocation gatherlocation = new GatherLocation();

                            Location location = gatherlocation.getLocation(context);
                            if (location != null) {
                                double lat = location.getLatitude();
                                double lng = location.getLongitude();
                                CommonUtils.logD("Lat : " + lat + " , Lng : " + lng);
                                model1.setLatitude(lat);
                                model1.setLongitude(lng);
                            }
                            new NodeUpdateTask().execute(model1);
                        } else if (Globals.registrationDataModel.getEndNode().getDlyStatus() == 1) {
                            imagePopUpRl.setVisibility(View.VISIBLE);
                            imageFileNames = new ArrayList<>();
                            imageFileNames.clear();
                            bitmapList = new ArrayList<>();
                            bitmapList.clear();
                            imageGallery.setAdapter(null);
                            imageAdapter = new ImageAdapter(context, R.layout.cv_image_gallery,
                                    imageFileNames);
                            imageGallery.setAdapter(imageAdapter);
                            imageAdapter.refresh();

                            dktNodeId = Globals.registrationDataModel.getEndNode().getDktNodeId();
                            nodeId = Globals.registrationDataModel.getEndNode().getNodeId();
                        } else if (Globals.registrationDataModel.getEndNode().getDlyStatus() == 2 || Globals.registrationDataModel.getEndNode().getDlyStatus() == 3) {
                            model1 = new UpdateNodeModel();
                            model1.setNodeId(Globals.registrationDataModel.getEndNode().getNodeId());
                            model1.setDktNodeId(Globals.registrationDataModel.getEndNode().getDktNodeId());
                            model1.setDktNo(Long.parseLong(Globals.registrationDataModel.getDktNo()));
                            SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            Calendar calendar = Calendar.getInstance();
                            String format = s.format(calendar.getTime());
                            if (CommonUtils.isValidString(format)) {
                                model1.setTimestamp(format);
//                                    Globals.action = Constants.DEPART;
                                Globals.action = Constants.END;
                            }

                            GatherLocation gatherlocation = new GatherLocation();

                            Location location = gatherlocation.getLocation(context);
                            if (location != null) {
                                double lat = location.getLatitude();
                                double lng = location.getLongitude();
                                CommonUtils.logD("Lat : " + lat + " , Lng : " + lng);
                                model1.setLatitude(lat);
                                model1.setLongitude(lng);
                            }

                            new NodeUpdateTask().execute(model1);
                        } else if (Globals.registrationDataModel.getEndNode().getDlyStatus() == 3) {

                        }
//                        }


                    }

                    break;

                case R.id.btn_capture_image:
                    truckImageFileName = "";
                    takePicture(context);
                    break;

                case R.id.btn_send:

                    String packetCount = updatePacketEt.getText().toString();
                    String remarks = remarksEt.getText().toString();
                    if (Globals.registrationDataModel != null) {
                        if (Globals.registrationDataModel.getCapturePod() == 1) {
                            if (CommonUtils.isValidString(packetCount)) {
                                if (CommonUtils.isValidString(remarks)) {
                                    List<String> base64List = base64List(bitmapList);

                                    if (CommonUtils.isValidArraylist(base64List)) {
                                        submitData(packetCount, remarks);
                                    } else {
                                        CommonUtils.showToastShort(context, "Please capture POD image");
                                    }
                                } else {
                                    CommonUtils.showToastShort(context, "Please enter valid remarks");
                                }
                            } else {
                                CommonUtils.showToastShort(context, "Please enter valid packet count");
                            }
                        } else {
                            submitData("0", "");
                        }
                    }


                    break;
                case R.id.et_exception_type:
                    List<String> depsList = new ArrayList<>();
                    if (Globals.action == Constants.DELIVER || Globals.action == Constants.DEPART
                            || Globals.action == Constants.END || Globals.action == Constants.PICKUP)
                        depsList.addAll(dataSource.reasonCodes.getReasonsList(Constants.DEPARTURE));
                    else if (Globals.action == Constants.ARRIVED)
                        depsList.addAll(dataSource.reasonCodes.getReasonsList(Constants.ARRIVAL));
                    String[] a = new String[depsList.size()];
                    a = depsList.toArray(a);
                    showExceptionType(exceptionTypeEt, a);
                    break;
                case R.id.btn_submit_reason:
                    String excepDesc = exceptionTypeEt.getText().toString();
                    String exceptionType = "";
                    if (CommonUtils.isValidString(excepDesc)) {
                        String id = dataSource.reasonCodes.getExceptionId(excepDesc);
//                        Globals.action = Constants.DELIVER;
                        if (Globals.updateNodeModel != null) {
                            Globals.updateNodeModel.setTimestamp(CommonUtils.getTime(context));
                            Globals.updateNodeModel.setDelayReasonId(Integer.parseInt(id));
                            if (Globals.action == Constants.PICKUP) {
                                new WarehousePickupTask().execute(Globals.updateNodeModel);
                            } else {
                                new NodeUpdateTask().execute(Globals.updateNodeModel);
                            }
                        }
                        reasonsPopup.setVisibility(View.GONE);
                    } else {
                        CommonUtils.showToastShort(context, "Please select Reason code");
                    }
                    break;
                case R.id.logout:
                    TCDetailsScreen.this.finish();
                    startActivity(new Intent(TCDetailsScreen.this, LoginScreen.class));
                    break;
            }
        }

    };

    private void submitData(String packetCount, String remarks) {


        UpdateNodeModel model = new UpdateNodeModel();
        try {

            List<String> base64List = base64List(bitmapList);

            if (Globals.registrationDataModel != null) {

                GatherLocation gatherlocation = new GatherLocation();

                Location location = gatherlocation.getLocation(context);
                if (location != null) {
                    double lat = location.getLatitude();
                    double lng = location.getLongitude();
                    CommonUtils.logD("Lat : " + lat + " , Lng : " + lng);
                    model.setLatitude(lat);
                    model.setLongitude(lng);
                }
                model.setDktNo(Long.parseLong(Globals.registrationDataModel.getDktNo()));
                model.setSubprojectId((Globals.registrationDataModel.getSubprojectId()));
                model.setDktNodeId(dktNodeId);
                model.setNodeId(nodeId);
                nodeUpdated = nodeId;
                model.setRemarks(remarks);
                model.setPod(base64List);
                model.setPktCount(Integer.parseInt(packetCount));
                SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                Calendar calendar = Calendar.getInstance();
                String format = s.format(calendar.getTime());
                if (CommonUtils.isValidString(format)) {
                    model.setTimestamp(format);
                    Globals.action = Constants.DELIVER;
                }
//                                            CommonUtils.logD(model.toString());
                new NodeUpdateTask().execute(model);
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void updateEndNode() {
        UpdateNodeModel model = new UpdateNodeModel();
        if (Globals.registrationDataModel != null && Globals.registrationDataModel.getEndNode() != null) {
            NodeModel endModel = Globals.registrationDataModel.getEndNode();
            GatherLocation gatherlocation = new GatherLocation();

            Location location = gatherlocation.getLocation(context);
            if (location != null) {
                double lat = location.getLatitude();
                double lng = location.getLongitude();
                CommonUtils.logD("Lat : " + lat + " , Lng : " + lng);
                model.setLatitude(lat);
                model.setLongitude(lng);
            }
            model.setDktNo(Long.parseLong(Globals.registrationDataModel.getDktNo()));
            model.setSubprojectId((Globals.registrationDataModel.getSubprojectId()));
            model.setDktNodeId(endModel.getDktNodeId());
            model.setNodeId(endModel.getNodeId());
            nodeUpdated = endModel.getNodeId();
            SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Calendar calendar = Calendar.getInstance();
            String format = s.format(calendar.getTime());
            if (CommonUtils.isValidString(format)) {
                model.setTimestamp(format);
            }
            Globals.action = Constants.END;
            new EndNodeUpdateTask().execute(model);
        }
    }

    private String encodeFileToBase64Binary(File yourFile) {
        int size = (int) yourFile.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(yourFile));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String encoded = Base64.encodeToString(bytes, Base64.DEFAULT);
        CommonUtils.logD("tst" + encoded);
        return encoded;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tc_details_screen);

        context = TCDetailsScreen.this;

        dataSource = new DataSource(context);

        listView = (ListView) findViewById(R.id.node_points_lv);

        startTv = (TextView) findViewById(R.id.start_tv);
        endTv = (TextView) findViewById(R.id.end_tv);
        routeNameTv = (TextView) findViewById(R.id.route_name_tv);
        vehicleNoTv = (TextView) findViewById(R.id.vehicle_number_tv);
        startNodeTv = (TextView) findViewById(R.id.start_node_details_tv);
        endNodeTv = (TextView) findViewById(R.id.end_node_details_tv);
        tcNumberTv = (TextView) findViewById(R.id.tc_number_tv);
        versionTv = (TextView) findViewById(R.id.tv_version_top);

        logoutBtn = (ImageView) findViewById(R.id.logout);
        logoutBtn.setOnClickListener(onClickListener);

        exceptionTypeEt = (EditText) findViewById(R.id.et_exception_type);
        exceptionTypeEt.setOnClickListener(onClickListener);

        btnReasonSubmit = (Button) findViewById(R.id.btn_submit_reason);
        btnReasonSubmit.setOnClickListener(onClickListener);

        imageGallery = (Gallery) findViewById(R.id.images_gallery);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                model = setRegistrationModel();
                new GetTcData(TCDetailsScreen.this).execute(model);
            }
        });

        pickupPopupRl = (RelativeLayout) findViewById(R.id.layout_pickup_packet_popup);

        imagePopUpRl = (RelativeLayout) findViewById(R.id.layout_image_popup);

        reasonsPopup = (RelativeLayout) findViewById(R.id.layout_reasons_popup);

        pickUpCountEt = (EditText) findViewById(R.id.packets_count_et);
        remarksEt = (EditText) findViewById(R.id.remarks_et);
        updatePacketEt = (EditText) findViewById(R.id.packets_et);
        truckIv = (ImageView) findViewById(R.id.iv_truck);

        submitBtn = (Button) findViewById(R.id.submit_btn);
        submitBtn.setOnClickListener(onClickListener);

        endBtn = (Button) findViewById(R.id.end_btn);
        endBtn.setOnClickListener(onClickListener);

        pickUpBtn = (Button) findViewById(R.id.pickup_btn);
//        pickUpBtn.setBackgroundResource((R.drawable.grey_filled_bg));
//        pickUpBtn.setEnabled(false);
        pickUpBtn.setOnClickListener(onClickListener);

        captureBtn = (Button) findViewById(R.id.btn_capture_image);
        captureBtn.setOnClickListener(onClickListener);

        sendBtn = (Button) findViewById(R.id.btn_send);
        sendBtn.setOnClickListener(onClickListener);


        String versionVal = "";
        try {
            versionVal = String.valueOf(getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        imageFileNames = new ArrayList<>();
        imageFileNames.clear();
        bitmapList = new ArrayList<>();
        tcSearchList = new ArrayList<>();
        bitmapList.clear();
        imageAdapter = new ImageAdapter(context, R.layout.cv_image_gallery,
                imageFileNames);
        imageGallery.setAdapter(imageAdapter);
        imageAdapter.refresh();

        disabledAddImageBtn();

        imageGallery
                .setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                    @Override
                    public boolean onItemLongClick(AdapterView<?> arg0,
                                                   View arg1, int pos, long arg3) {
                        removeImage(pos);
                        return false;
                    }
                });


        versionTv.setText("Ver" + " : " + versionVal);
        model = setRegistrationModel();

//        tcList = new ArrayList<>();

//        new GetTcData().execute(model);
        if (Globals.registrationDataModel != null) {
            setData(Globals.registrationDataModel, false);

        } else {
            goback();
        }
        dataSource.sharedPreferences.set(Constants.LOCATION_INTERVAL, Constants.INTERVAL_VALUE);
//        startService(new Intent(TCDetailsScreen.this, EmployeeLocationService.class));
//        startService(new Intent(TCDetailsScreen.this, GeoFenceArrivalPushService.class));
//        createPdf();
        CommonUtils.dissmissKeyboard(pickUpCountEt);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver();
        if (Globals.registrationDataModel == null) {
            goback();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    private BroadcastReceiver broadcastReceiver;

    private void registerReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean dataModelUpdated = intent.getBooleanExtra("registrationDataModelUpdated", false);

                if(dataModelUpdated) {
                    setData(Globals.registrationDataModel, true);
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("com.spoton.spl.lmapp.tcdata"));
    }

    private void removeImage(final int pos) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(getString(R.string.confirm));
        builder.setMessage(getString(R.string.delete_image_conf));

        builder.setPositiveButton(getString(R.string.ok_btn),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        deleteImage(imageFileNames.get(pos));
                        imageFileNames.remove(pos);
                        bitmapList.remove(pos);
                        imageAdapter.refresh();
                        disabledAddImageBtn();
                        dialog.dismiss();

                    }

                }).setNegativeButton(getString(R.string.cancel_btn),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }

                });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();

    }

    private void deleteImage(String imageName) {
        try {
            ImageLoader imageLoader = CommonUtils.getImageLoader(context);
            imageLoader.fileCache.deleteFile(imageName);
        } catch (Exception e) {
        }
    }

    private RegistrationModel setRegistrationModel() {

        RegistrationModel model = new RegistrationModel();
        if (CommonUtils.isValidString(dataSource.sharedPreferences.getValue(Constants.DEVICE_TOKEN)))
            model.setPushToken(dataSource.sharedPreferences.getValue(Constants.DEVICE_TOKEN));

        if (CommonUtils.isValidString(CommonUtils.getImei(context)))
            model.setImei(CommonUtils.getImei(context));

        if (CommonUtils.isValidString(dataSource.sharedPreferences.getValue(Constants.MOBILE_NUMBER)))
            model.setMobileNo(dataSource.sharedPreferences.getValue(Constants.MOBILE_NUMBER));
        try {
            String versionName = getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName;
            if (CommonUtils.isValidString(versionName)) {
                model.setVersionName(versionName);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return model;
    }

    public void setData(RegistrationDataModel model, boolean isGeofence) {
        if (model != null) {
            Globals.isInGeoFence = false;
            if (nodeUpdated == -1) {
//                if(!isGeofence)
//                createGeoFences(Globals.registrationDataModel); //creating geofences in the home screen for all tcs
            }

            if (CommonUtils.isValidArraylist(model.getNodeList())) {

                MyAdapter adapter = new MyAdapter(context, R.layout.node_point_list_item, model.getNodeList());
                listView.setAdapter(adapter);
            }


            if (CommonUtils.isValidString(model.getRouteName()))
                routeNameTv.setText(model.getRouteName());
            if (CommonUtils.isValidString(model.getDktNo()))
                tcNumberTv.setText((model.getDktNo()));

            if (CommonUtils.isValidString(model.getVehicleNo()))
                vehicleNoTv.setText(model.getVehicleNo());

            if (CommonUtils.isValidString(model.getStartNode().getNodeName()))
                startTv.setText(model.getStartNode().getNodeName());

            if (CommonUtils.isValidString(model.getEndNode().getNodeName()))
                endTv.setText(model.getEndNode().getNodeName());

            if (CommonUtils.isValidString(model.getStartNode().getNodeName()))
                startNodeTv.setText(model.getStartNode().getNodeName());

            if (CommonUtils.isValidString(model.getStartNode().getAddress1()))
                startNodeTv.append("\n" + model.getStartNode().getAddress1());

            if (CommonUtils.isValidString(model.getStartNode().getAddress2()))
                startNodeTv.append("\n" + model.getStartNode().getAddress2());

            startNodeTv.append("\n" + model.getStartNode().getPincode());


            if (CommonUtils.isValidString(model.getEndNode().getNodeName()))
                endNodeTv.setText(model.getEndNode().getNodeName());

            if (CommonUtils.isValidString(model.getEndNode().getAddress1()))
                endNodeTv.append("\n" + model.getEndNode().getAddress1());

            if (CommonUtils.isValidString(model.getEndNode().getAddress2()))
                endNodeTv.append("\n" + model.getEndNode().getAddress2());

            endNodeTv.append("\n" + model.getEndNode().getPincode());

//            dataSource.sharedPreferences.set(Constants.TC_NUMBER, String.valueOf(model.getDktNo()));

//            if (model.getStartNode().getDlyStatus() == 1) {
//                pickUpBtn.setEnabled(false);
//                pickUpBtn.setBackgroundResource((R.drawable.grey_filled_bg));
//
//            }
            for (NodeModel model1 : model.getNodeList()) {
                if (model1.getDlyStatus() == 3) {
                    isEndEnabled = true;
                } else {
                    isEndEnabled = false;
                }
            }
                if (isEndEnabled) {
                    if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 3) {
                        endBtn.setBackgroundResource((R.drawable.orange_filled_bg));
                        endBtn.setEnabled(true);
                    }
                } else if (!CommonUtils.isValidArraylist(Globals.registrationDataModel.getNodeList())) {
                    if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 3) {
                        endBtn.setBackgroundResource((R.drawable.orange_filled_bg));
                        endBtn.setEnabled(true);

                        if (Globals.registrationDataModel.getEndNode().getDlyStatus() == 0) {
                            endBtn.setText(getString(R.string.arrived));
                        } else if (Globals.registrationDataModel.getEndNode().getDlyStatus() == 1) {
                            endBtn.setText(getString(R.string.deliver));
                        } else if (Globals.registrationDataModel.getEndNode().getDlyStatus() == 2) {
                            endBtn.setText(getString(R.string.leaving));
                            //                    endBtn.setText(getString(R.string.end_trip));
                        } else if (Globals.registrationDataModel.getEndNode().getDlyStatus() == 3) {
                            endBtn.setBackgroundResource((R.drawable.grey_filled_bg));
                            endBtn.setEnabled(false);
                            endBtn.setText(getString(R.string.left));
                            updateEndNode();
                        }
                    } else{
                        endBtn.setBackgroundResource((R.drawable.grey_filled_bg));
                        endBtn.setEnabled(false);
                    }
                } else {
                    if (Globals.registrationDataModel.getStartNode().getDlyStatus() != 3) {
                        endBtn.setBackgroundResource((R.drawable.grey_filled_bg));
                        endBtn.setEnabled(false);
                    }
                }


        }

        if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 0) {
            pickUpBtn.setText(getString(R.string.arrived));
        } else if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 1) {
            pickUpBtn.setText(getString(R.string.pick_up));
        } else if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 2) {
            pickUpBtn.setText(getString(R.string.leaving));
        } else if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 3) {
            pickUpBtn.setBackgroundResource((R.drawable.grey_filled_bg));
            pickUpBtn.setEnabled(false);
            pickUpBtn.setText(getString(R.string.left));
        }

            /*if (isEndEnabled) {
                endBtn.setBackgroundResource((R.drawable.grey_filled_bg));
                endBtn.setEnabled(false);
                if (Globals.registrationDataModel.getEndNode().getDlyStatus() == 1) {
                    updateEndNode();
                }
            } else if (!CommonUtils.isValidArraylist(Globals.registrationDataModel.getNodeList())) {
                endBtn.setEnabled(false);
//                endBtn.setBackgroundResource((R.drawable.grey_filled_bg));
                endBtn.setText(getString(R.string.arrived));

                int status = dataSource.geoFences.getTransition(Globals.registrationDataModel.getEndNode().getLat(),
                        Globals.registrationDataModel.getEndNode().getLng());
                if (Globals.registrationDataModel.getEndNode().getDlyStatus() == 0) {
                    endBtn.setText(getString(R.string.arrived));
                } else if ((Globals.registrationDataModel.getStartNode().getDlyStatus() == 2 ||
                        Globals.registrationDataModel.getStartNode().getDlyStatus() == 3) &&
                        Globals.registrationDataModel.getEndNode().getDlyStatus() == 1) {
//                    if(status == 1) {
                        endBtn.setBackgroundResource((R.drawable.orange_filled_bg));
                        endBtn.setEnabled(true);
                        endBtn.setText(getString(R.string.deliver));
//                    } else {
//                        endBtn.setEnabled(false);
//                        endBtn.setBackgroundResource((R.drawable.grey_filled_bg));
//                        endBtn.setText(getString(R.string.deliver));
//                    }
                } else if (Globals.registrationDataModel.getEndNode().getDlyStatus() == 2) {
                    endBtn.setText(getString(R.string.leaving));
//                    endBtn.setText(getString(R.string.end_trip));
                } else if (Globals.registrationDataModel.getEndNode().getDlyStatus() == 3) {
                    endBtn.setBackgroundResource((R.drawable.grey_filled_bg));
                    endBtn.setEnabled(false);
                    endBtn.setText(getString(R.string.left));
                    updateEndNode();
                }
                if( Globals.registrationDataModel.getEndNode().getIsTransitDelayCaptured() == 1) {
                    endBtn.setBackgroundResource((R.drawable.orange_filled_bg));
                    endBtn.setEnabled(true);
                    endBtn.setText(getString(R.string.deliver));
                }
            } else {
                endBtn.setBackgroundResource((R.drawable.grey_filled_bg));
                endBtn.setEnabled(false);
                endBtn.setText(getString(R.string.end_trip));
            }
        }

        int status = dataSource.geoFences.getTransition(Globals.registrationDataModel.getStartNode().getLat(),
                Globals.registrationDataModel.getStartNode().getLng());
        if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 0) {
            pickUpBtn.setEnabled(true);
            pickUpBtn.setText(getString(R.string.arrived));
        } else if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 1) {
//            if(status == 1) {
                pickUpBtn.setBackgroundColor(getResources().getColor(R.color.green));
                pickUpBtn.setEnabled(true);
                pickUpBtn.setText(getString(R.string.pick_up));
//            } else {
//                pickUpBtn.setBackgroundResource((R.drawable.grey_filled_bg));
//                pickUpBtn.setEnabled(false);
//                pickUpBtn.setText(getString(R.string.pick_up));
//            }
        } else if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 2) {
//            pickUpBtn.setBackgroundResource((R.drawable.grey_filled_bg));
            pickUpBtn.setEnabled(true);
            pickUpBtn.setText(getString(R.string.leaving));
        } else if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 3) {
            pickUpBtn.setBackgroundResource((R.drawable.grey_filled_bg));
            pickUpBtn.setEnabled(false);
            pickUpBtn.setText(getString(R.string.left));
        }*/
    }

    /*private void createGeoFences(RegistrationDataModel model) {
        List<GeoFenceModel> assignedGeofences = new ArrayList<>();
        if (CommonUtils.isValidArraylist(model.getNodeList())) {
            for (NodeModel m : model.getNodeList()) {
                GeoFenceModel geoFenceModel = new GeoFenceModel(model.getSubprojectId(), m.getNodeId(), m.getDktNodeId(), Integer.parseInt(model.getDktNo()), 1, m.getLat(), m.getLng(), Constants.GEOFENCE_RADIUS, Constants.FALSE, Constants.FALSE, 0);
                assignedGeofences.add(geoFenceModel);
            }
        }
        if (model.getStartNode() != null && model.getStartNode().getLat() != null && model.getStartNode().getLng() != null) {
            GeoFenceModel geoFenceModel = new GeoFenceModel(model.getSubprojectId(), model.getStartNode().getNodeId(), model.getStartNode().getDktNodeId(), Integer.parseInt(model.getDktNo()), 1, model.getStartNode().getLat(), model.getStartNode().getLng(), Constants.GEOFENCE_RADIUS, Constants.TRUE, Constants.FALSE, 0);
            assignedGeofences.add(geoFenceModel);
        }

        if (model.getEndNode() != null && model.getEndNode().getLat() != null && model.getEndNode().getLng() != null) {
            GeoFenceModel geoFenceModel = new GeoFenceModel(model.getSubprojectId(), model.getEndNode().getNodeId(), model.getEndNode().getDktNodeId(), Integer.parseInt(model.getDktNo()), 1, model.getEndNode().getLat(), model.getEndNode().getLng(), Constants.GEOFENCE_RADIUS, Constants.FALSE, Constants.TRUE, 0);
            assignedGeofences.add(geoFenceModel);
        }

        dataSource.geoFences.insertData(assignedGeofences);
        SetGeofence setGeofence = new SetGeofence();
        setGeofence.performAction(context, assignedGeofences);
    }*/

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (pickupPopupRl.getVisibility() == View.VISIBLE)
                pickupPopupRl.setVisibility(View.GONE);
            else if (imagePopUpRl.getVisibility() == View.VISIBLE)
                imagePopUpRl.setVisibility(View.GONE);

            else
                finish();
        }
        return true;
    }

    private void takePicture(Context context) {
        // Uri imageUri = Uri.fromFile(getTempFile(context));
        // Intent intent = createIntentForCamera(imageUri);
        // startActivityForResult(intent, TAKE_PHOTO);
        CommonUtils.logD("*");
        Intent intent = new Intent(context, CameraActivity.class);
        startActivityForResult(intent, TAKE_PHOTO);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = null;
        switch (requestCode) {


            case TAKE_PHOTO:
                try {
                    CommonUtils.logD("**");

                    Uri uri = (Uri) data.getExtras().get(Intent.EXTRA_STREAM);
                    String imageUri = uri.toString();
                    bitmap = BitmapScalingUtil.bitmapFromUri(context,
                            Uri.parse(imageUri));
                    if (bitmap != null) {
                        int w = bitmap.getWidth();
                        int h = bitmap.getHeight();
                        Matrix mat = new Matrix();
                        if (w > h)
                            mat.postRotate(90);

                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mat, true);
                        Globals.bitmap = bitmap;

                        String diff = dataSource.sharedPreferences
                                .getValue(Constants.TIME_DIFF);
                        long d = 0;
                        if (CommonUtils.isValidString(diff))
                            d = Long.valueOf(diff);

                        addImage(bitmap, CommonUtils.getImageTime(d), false);
                    }
                } catch (Exception e) {

                }

                break;
        }

    }

    public void addImage(Bitmap bitmap, String imageFileName, boolean isCrop) {
        CommonUtils.logD("***");

//        if (truckIv != null && bitmap != null) {
        if (imageAdapter != null && imageGallery != null && bitmap != null) {
            CommonUtils.logD("*****");

            ImageLoader imageLoader = new ImageLoader(context, Constants.TRUCK);
            Uri uri = imageLoader.fileCache.saveBitmapFile(bitmap,
                    imageFileName);
            if (uri != null) {
                if (imageGallery.getVisibility() == View.GONE)
                    imageGallery.setVisibility(View.VISIBLE);
                int alReadyFoundPos = -1;

                for (int i = 0; i < imageFileNames.size(); i++) {
                    CommonUtils.logD("--");

                    if (imageFileName.equalsIgnoreCase(imageFileNames.get(i))) {
                        alReadyFoundPos = i;
                        break;
                    }
                }
                if (alReadyFoundPos != -1) {
                    imageFileNames.set(alReadyFoundPos, imageFileName);
                } else {
//                    doCrop(bitmap, imageFileName);
                    imageFileNames.add(imageFileName);
                    bitmapList.add(bitmap);
                }

                imageAdapter = new ImageAdapter(context, R.layout.cv_image_gallery,
                        imageFileNames);
                imageGallery.setAdapter(imageAdapter);

                imageAdapter.refresh();

                disabledAddImageBtn();

//                if (truckIv.getVisibility() == View.GONE || truckIv.getVisibility() == View.INVISIBLE)
//                    truckIv.setVisibility(View.VISIBLE);

                truckImageFileName = imageFileName;
//                truckIv.setImageBitmap(bitmap);


            }
        }

    }

    private void setImageFileNames(ArrayList<String> list) {

        if (imageFileNames == null)
            imageFileNames = new ArrayList<String>();
        else
            imageFileNames.clear();

        if (CommonUtils.isValidArraylist(list)) {
            for (String name : list) {
                imageFileNames.add(name);
            }
        } else
            imageFileNames.clear();
    }


    private void disabledAddImageBtn() {
        String size = dataSource.sharedPreferences.getValue(Constants.maxImageSize);
        if (CommonUtils.isValidString(size)) {
            if (imageFileNames.size() > Integer.parseInt((size))) {
                captureBtn.setVisibility(View.GONE);
            } else {
                captureBtn.setVisibility(View.VISIBLE);
            }
        }
    }

    private boolean checkGPSStatus() {
        boolean isGpsTurnedon = false;
        LocationManager locationManager = null;
        boolean gps_enabled = false;
        boolean network_enabled = false;
        if (locationManager == null) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        }
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        if (!gps_enabled && !network_enabled) {
            isGpsTurnedon = false;
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setCancelable(false);
            dialog.setMessage(getString(R.string.turn_on_gps));
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //this will navigate user to the device location settings screen
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                    dialog.dismiss();
                }
            });
            AlertDialog alert = dialog.create();
            alert.show();
        } else
            isGpsTurnedon = true;

        return isGpsTurnedon;
    }

    private List<String> base64List(List<Bitmap> bitmapList) {

        List<String> base64List = new ArrayList<>();

        for (Bitmap b : bitmapList) {


            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            byte[] byteArray = stream.toByteArray();

            String base64img = Base64
                    .encodeToString(byteArray, Base64.DEFAULT);

            if (CommonUtils.isValidString(base64img)) {
                base64img = base64img.trim();
                base64img = base64img.replace("\n", "");
                base64List.add(base64img);
            }

        }

        return base64List;
    }

    // Method for creating a pdf file from text, saving it then opening it for display
    public String createandDisplayPdf(List<Bitmap> bitmaps) {
        String base64 = "";
        File filepdf;
        if (CommonUtils.isValidArraylist(bitmapList)) {
            Document doc = new Document();

            try {
                String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Dir";

                File dir = new File(path);
                if (!dir.exists())
                    dir.mkdirs();

                filepdf = new File(dir, "newFile.pdf");
                FileOutputStream fOut = new FileOutputStream(filepdf);

                PdfWriter.getInstance(doc, fOut);

                //open the document
                doc.open();

//                Paragraph p1 = new Paragraph(Constants.LMD_IMAGES);
//                Font paraFont = new Font();
//                p1.setAlignment(Paragraph.ALIGN_CENTER);
//                p1.setFont(paraFont);
//                doc.add(p1);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                for (Bitmap bitmap : bitmaps) {
                    if (bitmap != null) {

                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        try {
                            Image myImg = Image.getInstance(stream.toByteArray());
                            myImg.setAlignment(Image.MIDDLE);
                            doc.add(myImg);
                        } catch (IOException | DocumentException e) {
                            CommonUtils.logE(e.toString());
                        }
                    }
                }

                base64 = encodeFileToBase64Binary(filepdf);

                //add paragraph to document

            } catch (DocumentException de) {
                Log.e("PDFCreator", "DocumentException:" + de);
            } catch (IOException e) {
                Log.e("PDFCreator", "ioException:" + e);
            } finally {
                doc.close();
            }

        }
        return base64;
    }
//        viewPdf("newFile.pdf", "Dir");


    // Method for opening a pdf file
    private void viewPdf(String file, String directory) {

        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/" + directory + "/" + file);
        Uri path = Uri.fromFile(pdfFile);

        // Setting the intent for pdf reader
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "Can't read pdf file", Toast.LENGTH_SHORT).show();
        }
    }

    private void showExceptionType(final EditText exceptionTv,
                                   final String arr[]) {
        int selectedPos = -1;

        String text = URLDecoder
                .decode(exceptionTv.getText().toString().trim());
        if (CommonUtils.isValidString(text)) {
            for (int i = 0; i < arr.length; i++) {
                if (text.equalsIgnoreCase(arr[i].toString())) {
                    selectedPos = i;
                    break;
                }
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setSingleChoiceItems(arr, selectedPos,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int pos) {
                        dialog.dismiss();
                        exceptionTv.setText(arr[pos]);

                        exceptionTypeEt.setText(arr[pos]);
                    }
                })
                .setNegativeButton(getString(R.string.cancel_btn),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        }).show();

    }

    private void goback() {
        Intent intent = new Intent(context, HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void updateTcDataResult() {
        swipeRefreshLayout.setRefreshing(false);

        if (Globals.registrationDataModel != null) {
            if (CommonUtils.isValidArraylist(Globals.tcList)) {
                for (RegistrationDataModel model : Globals.tcList) {
                    if (Globals.registrationDataModel.getDktNo().equalsIgnoreCase(model.getDktNo())) {
                        dataSource.sharedPreferences.set(Constants.TC_NUMBER, String.valueOf(model.getDktNo()));
                        Globals.registrationDataModel = model;
                        setData(Globals.registrationDataModel, false);
                    }
                }
            }
        }
    }

    private class MyAdapter extends ArrayAdapter<NodeModel> {
        private int layoutId;
        private LayoutInflater inflater;
        private List<NodeModel> modelList;

        public MyAdapter(Context context, int layoutId, List<NodeModel> modelList) {
            super(context, 0, modelList);
            this.layoutId = layoutId;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.modelList = modelList;

        }

        @Override
        public int getViewTypeCount() {

            return getCount();
        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = inflater.inflate(layoutId, parent, false);
                holder = new ViewHolder();
                holder.routeDetails = (TextView) convertView.findViewById(R.id.node_details_tv);
                holder.arrivedBtn = (Button) convertView.findViewById(R.id.arrived_btn);


                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            final NodeModel model = (modelList.get(position));


            if (CommonUtils.isValidString(model.getNodeName()))
                holder.routeDetails.setText(model.getNodeName());

            if (CommonUtils.isValidString(model.getAddress1()))
                holder.routeDetails.append("\n" + model.getAddress1());

            if (CommonUtils.isValidString(model.getAddress2()))
                holder.routeDetails.append("\n" + model.getAddress2());

            holder.routeDetails.append("\n" + model.getPincode());
            if(Globals.registrationDataModel != null) {
               /* int status = dataSource.geoFences.getTransition(Globals.registrationDataModel.getEndNode().getLat(),
                        Globals.registrationDataModel.getEndNode().getLng());
                if (model.getDlyStatus() == 0) {
//                    holder.arrivedBtn.setBackgroundResource((R.drawable.grey_filled_bg));
//                    holder.arrivedBtn.setEnabled(false);
                    holder.arrivedBtn.setText(getString(R.string.arrived));
//                    holder.arrivedBtn.setText(getString(R.string.deliver));
                } else if ((Globals.registrationDataModel.getStartNode().getDlyStatus() == 2 ||
                        Globals.registrationDataModel.getStartNode().getDlyStatus() == 3) &&
                        Globals.registrationDataModel.getEndNode().getDlyStatus() == 1) {
//                    if (status == 1) {
                        holder.arrivedBtn.setBackgroundResource((R.drawable.orange_filled_bg));
                        holder.arrivedBtn.setEnabled(true);
                        holder.arrivedBtn.setText(getString(R.string.deliver));
//                    } else {
//                        holder.arrivedBtn.setBackgroundResource((R.drawable.grey_filled_bg));
//                        holder.arrivedBtn.setEnabled(false);
//                        holder.arrivedBtn.setText(getString(R.string.deliver));
//                    }

                } else if (model.getDlyStatus() == 2) {
//                    holder.arrivedBtn.setBackgroundResource((R.drawable.grey_filled_bg));
//                    holder.arrivedBtn.setEnabled(false);
                    holder.arrivedBtn.setText(getString(R.string.leaving));
                } else if (model.getDlyStatus() == 3) {
                    holder.arrivedBtn.setBackgroundResource((R.drawable.grey_filled_bg));
                    holder.arrivedBtn.setEnabled(false);
                    holder.arrivedBtn.setText(getString(R.string.left));
//                holder.arrivedBtn.setText(getString(R.string.left));
                }*/
                holder.arrivedBtn.setEnabled(false);
                holder.arrivedBtn.setBackgroundResource((R.drawable.grey_filled_bg));
                if (Globals.registrationDataModel.getStartNode().getDlyStatus() == 3) {
                    if (model.getDlyStatus() == 0) {
                        holder.arrivedBtn.setBackgroundResource((R.drawable.orange_filled_bg));
                        holder.arrivedBtn.setEnabled(true);
                        holder.arrivedBtn.setText(getString(R.string.arrived));
                    } else if (model.getDlyStatus() == 1) {
                        holder.arrivedBtn.setBackgroundResource((R.drawable.orange_filled_bg));
                        holder.arrivedBtn.setEnabled(true);
                        holder.arrivedBtn.setText(getString(R.string.deliver));
                    } else if (model.getDlyStatus() == 2) {
                        holder.arrivedBtn.setBackgroundResource((R.drawable.orange_filled_bg));
                        holder.arrivedBtn.setEnabled(true);
                        holder.arrivedBtn.setText(getString(R.string.leaving));
                    } else if (model.getDlyStatus() == 3) {
                        holder.arrivedBtn.setBackgroundResource((R.drawable.grey_filled_bg));
                        holder.arrivedBtn.setEnabled(false);
                        holder.arrivedBtn.setText(getString(R.string.left));
                    }
                }
            }


            holder.arrivedBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UpdateNodeModel model1;

                    dktNodeId = model.getDktNodeId();
                    nodeId = model.getNodeId();
                    remarksEt.setText(null);
                    updatePacketEt.setText(null);
                    truckImageFileName = null;
                    truckIv.setImageBitmap(null);

                    if (model.getDlyStatus() == 0) {

                        model1 = new UpdateNodeModel();
                        model1.setNodeId(model.getNodeId());
                        model1.setDktNodeId(model.getDktNodeId());
                        if (Globals.registrationDataModel != null) {
                            model1.setDktNo(Long.parseLong(Globals.registrationDataModel.getDktNo()));
                            model1.setSubprojectId((Globals.registrationDataModel.getSubprojectId()));
                        }
                        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        Calendar calendar = Calendar.getInstance();
                        String format = s.format(calendar.getTime());
                        if (CommonUtils.isValidString(format)) {
                            model1.setTimestamp(format);
                            Globals.action = Constants.ARRIVED;
                        }

                        GatherLocation gatherlocation = new GatherLocation();

                        Location location = gatherlocation.getLocation(context);
                        if (location != null) {
                            double lat = location.getLatitude();
                            double lng = location.getLongitude();
                            CommonUtils.logD("Lat : " + lat + " , Lng : " + lng);
                            model1.setLatitude(lat);
                            model1.setLongitude(lng);
                        }
                        new NodeUpdateTask().execute(model1);

                    } else if (model.getDlyStatus() == 1) {
                        imagePopUpRl.setVisibility(View.VISIBLE);
                        imageFileNames = new ArrayList<>();
                        imageFileNames.clear();
                        bitmapList = new ArrayList<>();
                        bitmapList.clear();
                        imageAdapter = new ImageAdapter(context, R.layout.cv_image_gallery,
                                imageFileNames);
                        imageGallery.setAdapter(imageAdapter);
                        imageAdapter.refresh();

                    } else if (model.getDlyStatus() == 2) {

                        model1 = new UpdateNodeModel();
                        model1.setNodeId(model.getNodeId());
                        model1.setDktNodeId(model.getDktNodeId());
                        if (Globals.registrationDataModel != null)
                            model1.setDktNo(Long.parseLong(Globals.registrationDataModel.getDktNo()));
                        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        Calendar calendar = Calendar.getInstance();
                        String format = s.format(calendar.getTime());
                        if (CommonUtils.isValidString(format)) {
                            model1.setTimestamp(format);
                            Globals.action = Constants.DEPART;
                        }

                        GatherLocation gatherlocation = new GatherLocation();

                        Location location = gatherlocation.getLocation(context);
                        if (location != null) {
                            double lat = location.getLatitude();
                            double lng = location.getLongitude();
                            CommonUtils.logD("Lat : " + lat + " , Lng : " + lng);
                            model1.setLatitude(lat);
                            model1.setLongitude(lng);
                        }

                        new NodeUpdateTask().execute(model1);
                    } else if (model.getDlyStatus() == 3) {

                    }

                }
            });

            return convertView;
        }
    }

    public class ViewHolder {
        public TextView routeDetails;
        private Button arrivedBtn;
    }

    class WarehousePickupTask extends AsyncTask<UpdateNodeModel, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CommonUtils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(UpdateNodeModel... params) {

            try {
                UpdateNodeModel updateNodeModel = params[0];

                String docketId = String.valueOf(updateNodeModel.getDktNo());
                String count = String.valueOf(updateNodeModel.getPktCount());
                String url = LMDUtils.getPickupUpdateUrlTag(docketId, count);


                ObjectMapper mapper = new ObjectMapper();


                String data = mapper.writeValueAsString(updateNodeModel);
                CommonUtils.logD(updateNodeModel.toString());
                updateNodeResponse = (UpdateNodeResponse) HttpRequest.postData(url, data, Constants.CONTENT_TYPE, HttpRequest.CONTENT_TYPE, UpdateNodeResponse.class);

                if (updateNodeResponse != null) {
                    CommonUtils.logD(updateNodeResponse.toString());
                    if (updateNodeResponse.getStatus()) {
                        Globals.lastErrMsg = updateNodeResponse.getMessage();
                    } else {
                        Globals.updateNodeModel = new UpdateNodeModel();
                        Globals.updateNodeModel = updateNodeModel;
                        Globals.lastErrMsg = updateNodeResponse.getMessage();
                    }


                } else {
                    Globals.lastErrMsg = "Response is null";
                }
            } catch (Exception e) {
                if (!CommonUtils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }


            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            CommonUtils.dismissProgressDialog();
            CommonUtils.showErrorDialog(context);
            pickupPopupRl.setVisibility(View.GONE);

            if (updateNodeResponse != null && updateNodeResponse.getStatus()) {
                CommonUtils.showToastShort(context, updateNodeResponse.getMessage());
                model = setRegistrationModel();
                new GetTcData(TCDetailsScreen.this).execute(model);
            } else {
                if (updateNodeResponse != null && updateNodeResponse.getData() != null && updateNodeResponse.getData().isDelayCaptured()) {
                    reasonsPopup.setVisibility(View.VISIBLE);
                    exceptionTypeEt.setText("");
                }
            }

        }
    }

    class NodeUpdateTask extends AsyncTask<UpdateNodeModel, Void, Void> {
        String urlTag = "";
        UpdateNodeModel updateNodeModel = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CommonUtils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(UpdateNodeModel... params) {

            String geofence = "0";//1- api called from geofencing 0 api called when user clicks arrived
            if (Globals.action == Constants.ARRIVED)
                urlTag = LMDUtils.getArrivalUrlTag(geofence);
            else if (Globals.action == Constants.DELIVER)
                urlTag = LMDUtils.getDeliveyUrlTag();
            else if (Globals.action == Constants.DEPART)
                urlTag = LMDUtils.getDepartureUrlTag(geofence);
            else if (Globals.action == Constants.END)
                urlTag = LMDUtils.getEndTripUrlTag(geofence);


            updateNodeModel = params[0];

            ObjectMapper mapper = new ObjectMapper();

            try {

                String data = mapper.writeValueAsString(updateNodeModel);
                CommonUtils.logD(updateNodeModel.toString());
                updateNodeResponse = (UpdateNodeResponse) HttpRequest.postData(urlTag, data, Constants.CONTENT_TYPE, HttpRequest.CONTENT_TYPE, UpdateNodeResponse.class);

                if (updateNodeResponse != null) {
                    CommonUtils.logD(updateNodeResponse.toString());
                    if (updateNodeResponse.getStatus()) {
                        Globals.lastErrMsg = updateNodeResponse.getMessage();
                        HashMap<LatLng, List<GeoFenceModel>> locationMap = dataSource.geoFences.getAllNodesData();
                        for (LatLng latLng : locationMap.keySet()) {
                            if (locationMap.get(latLng).get(0).getDktNo() == updateNodeModel.getDktNo() &&
                                    locationMap.get(latLng).get(0).getNodeId() == updateNodeModel.getNodeId()) {
                                if (Globals.action == Constants.ARRIVED) {
                                    dataSource.geoFences.updateTransition(latLng, 1);
                                } else if (Globals.action == Constants.DEPART || Globals.action == Constants.END) {
                                    dataSource.geoFences.updateTransition(latLng, 0);
                                }
                            }
                        }

                    } else {
                        Globals.updateNodeModel = new UpdateNodeModel();
                        Globals.updateNodeModel = updateNodeModel;
                        Globals.lastErrMsg = updateNodeResponse.getMessage();
                    }


                } else {
                    Globals.lastErrMsg = "Response is null";
                }
            } catch (Exception e) {
                if (!CommonUtils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }


            return null;

        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            CommonUtils.dismissProgressDialog();
            CommonUtils.showErrorDialog(context);
            if (updateNodeResponse != null && updateNodeResponse.getStatus()) {
                if (imagePopUpRl.getVisibility() == View.VISIBLE)
                    imagePopUpRl.setVisibility(View.GONE);
                if (Globals.action == Constants.END) {
                    if (updateNodeModel != null) {
                        dataSource.geoFences.deleteGeofences(updateNodeModel.getDktNo());
                    }
                    goback();
                } else {
                    model = setRegistrationModel();
                    new GetTcData(TCDetailsScreen.this).execute(model);
                }

            } else {
                if (updateNodeResponse != null && updateNodeResponse.getData() != null && updateNodeResponse.getData().isDelayCaptured()) {
                    imagePopUpRl.setVisibility(View.GONE);
                    reasonsPopup.setVisibility(View.VISIBLE);
                    exceptionTypeEt.setText("");
                }
            }
        }

    }

    class EndNodeUpdateTask extends AsyncTask<UpdateNodeModel, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CommonUtils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(UpdateNodeModel... params) {

            String geofence = "0";

            String loginUrl = LMDUtils.getEndTripUrlTag(geofence);//1- api called from geofencing 0 api called when user clicks arrived

            UpdateNodeModel updateNodeModel = params[0];

            ObjectMapper mapper = new ObjectMapper();

            try {

                String data = mapper.writeValueAsString(updateNodeModel);
                CommonUtils.logD(updateNodeModel.toString());
                response = (GeneralResponseModel) HttpRequest.postData(loginUrl, data, Constants.CONTENT_TYPE, HttpRequest.CONTENT_TYPE, GeneralResponseModel.class);

                if (response != null) {
                    CommonUtils.logD(response.toString());
                    if (response.getStatus()) {
                        Globals.lastErrMsg = response.getMessage();

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }


                } else {
                    Globals.lastErrMsg = "Response is null";
                }
            } catch (Exception e) {
                if (!CommonUtils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }


            return null;

        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            CommonUtils.dismissProgressDialog();
            CommonUtils.showErrorDialog(context);
            if (response != null && response.getStatus()) {
//                isEndEnabled = false;
//                endBtn.setBackgroundResource((R.drawable.grey_filled_bg));
//                endBtn.setEnabled(false);
                imagePopUpRl.setVisibility(View.GONE);
                dataSource.geoFences.truncatetable();
                if (Globals.action == Constants.END) {
                    goback();
                } else {
                    model = setRegistrationModel();
                    new GetTcData(TCDetailsScreen.this).execute(model);
                }

            }
        }

    }

    public class ViewHolderImage {
        public ImageView imageView;
        public ProgressBar progressBar;

    }

    private class ImageAdapter extends ArrayAdapter<String> {
        public ImageLoader imageLoader;
        private LayoutInflater inflater;
        private int layoutId;
        private int mGalleryItemBackground;

        public ImageAdapter(Context context, int layoutId,
                            ArrayList<String> objects) {
            super(context, 0, objects);
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.layoutId = layoutId;
            TypedArray a = context.obtainStyledAttributes(R.styleable.Gallery);
            this.mGalleryItemBackground = a.getResourceId(
                    R.styleable.Gallery_android_galleryItemBackground, 0);
            a.recycle();

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolderImage holder;

            if (convertView == null) {
                convertView = inflater.inflate(layoutId, parent, false);
                holder = new ViewHolderImage();
                holder.imageView = (ImageView) convertView
                        .findViewById(R.id.iv_addImg);
                holder.progressBar = (ProgressBar) convertView
                        .findViewById(R.id.pb_addImg);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderImage) convertView.getTag();
            }
            String url = getItem(position);
            imageLoader = CommonUtils.getImageLoader(context);
            holder.imageView.setBackgroundResource(mGalleryItemBackground);

            imageLoader.DisplayImage(url, holder.imageView, holder.progressBar);

            return convertView;
        }

        public void refresh() {
            this.notifyDataSetChanged();
            if (imageGallery != null) {
                if (imageGallery.getAdapter().getCount() > 0)
                    imageGallery.setVisibility(View.VISIBLE);
                else
                    imageGallery.setVisibility(View.GONE);
            }
        }

    }

}
