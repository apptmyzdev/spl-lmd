package com.spoton.spl.lmapp;

import android.content.Context;
import android.os.AsyncTask;

import com.spoton.spl.lmapp.models.RegistrationModel;
import com.spoton.spl.lmapp.responses.ResgistrationResponseModel;
import com.spoton.spl.lmapp.utils.CommonUtils;
import com.spoton.spl.lmapp.utils.Constants;
import com.spoton.spl.lmapp.utils.Globals;
import com.spoton.spl.lmapp.utils.HttpRequest;
import com.spoton.spl.lmapp.utils.LMDUtils;

import org.codehaus.jackson.map.ObjectMapper;

import java.util.ArrayList;

public class GetTcData extends AsyncTask<RegistrationModel, Void, Void> {

    public interface TcDataListener{
        void updateTcDataResult();
    }

    private Context context;
    private TcDataListener tcDataListener;

    public GetTcData(Context context) {
        this.context = context;
        this.tcDataListener = (TcDataListener) context;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        CommonUtils.getProgressDialog(context);
    }

    @Override
    protected Void doInBackground(RegistrationModel... params) {
        String loginUrl = LMDUtils.getDeviceRegistration();
        RegistrationModel registrationModel = params[0];
        ObjectMapper mapper = new ObjectMapper();

        try {
            String data = mapper.writeValueAsString(registrationModel);
            CommonUtils.logD(registrationModel.toString());
            ResgistrationResponseModel response = (ResgistrationResponseModel) HttpRequest.postData(loginUrl, data, Constants.CONTENT_TYPE, HttpRequest.CONTENT_TYPE, ResgistrationResponseModel.class);
            if (response != null) {
                CommonUtils.logD(response.toString());
                if (response.getStatus()) {
                    Globals.tcList.clear();
                    Globals.tcList.addAll(response.getData());
                } else {
                    Globals.lastErrMsg = response.getMessage();
                }
            } else {
                Globals.lastErrMsg = "Response is null";
            }
        } catch (Exception e) {
            if (!CommonUtils.isValidString(Globals.lastErrMsg))
                Globals.lastErrMsg = e.toString();
            if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                Globals.lastErrMsg = context.getString(R.string.server_not_reachable);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        CommonUtils.dismissProgressDialog();
        CommonUtils.showErrorDialog(context);
        tcDataListener.updateTcDataResult();
    }
}
