package com.spoton.spl.lmapp.datacache;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.spoton.spl.lmapp.utils.CommonUtils;

public class DataHelper extends SQLiteOpenHelper {

    public final static int DATABASE_VERSION = 1;
    public final static String DATABASE_NAME = "last_mile_delivery";


    /****************SHARED PREFERENCES *********/

    public static final String SHARED_PREF_TABLE_NAME = "sharedPreferences";
    public static final String SHARED_PREF_KEY_ID = "SHARED_PREF_KEY_ID";
    public static final String SHARED_PREF_COLUMN_KEY = "KEY";
    public static final String SHARED_PREF_COLUMN_VALUE = "VALUE";
    public static final String SHARED_PREF_TABLE_CREATE = "create table "
            + SHARED_PREF_TABLE_NAME + "(" + SHARED_PREF_KEY_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + SHARED_PREF_COLUMN_KEY
            + " TEXT NOT NULL," + SHARED_PREF_COLUMN_VALUE + " TEXT NOT NULL);";
    public static final String[] SHARED_PREF_COLUMNS = {
            SHARED_PREF_COLUMN_KEY, SHARED_PREF_COLUMN_VALUE};


    /***************** GeoFences Table *******************/
//used for creating geofences
    public static final String GEOFENCES_TABLE_NAME = "geofence_list";
    public static final String GEOFENCES_COLUMN_DKT_NODE_ID_NODE_ID = "GEOFENCES_COLUMN_DKT_NODE_ID_NODE_ID";
    public static final String GEOFENCES_COLUMN_NODE_ID = "GEOFENCES_COLUMN_NODE_ID";
    public static final String GEOFENCES_COLUMN_DKT_NODE_ID = "GEOFENCES_COLUMN_DKT_NODE_ID";
    public static final String GEOFENCES_COLUMN_DKT_NO = "GEOFENCES_COLUMN_DKT_NO";
    public static final String GEOFENCES_COLUMN_SUB_PROJECT_ID = "GEOFENCES_COLUMN_SUB_PROJECT_ID";
    public static final String GEOFENCES_COLUMN_LAT = "GEOFENCES_COLUMN_LAT";
    public static final String GEOFENCES_COLUMN_LONG = "GEOFENCES_COLUMN_LONG";
    public static final String GEOFENCES_COLUMN_ISSTARTNODE = "GEOFENCES_COLUMN_ISSTARTNODE";
    public static final String GEOFENCES_COLUMN_ISENDNODE = "GEOFENCES_COLUMN_ISENDNODE";
    public static final String GEOFENCE_TRANSITION_STATUS = "GEOFENCE_TRANSITION_STATUS";

    public static final String GEOFENCES_TABLE_CREATE = "create table "
            + GEOFENCES_TABLE_NAME + "(" + GEOFENCES_COLUMN_DKT_NODE_ID_NODE_ID
            + " TEXT PRIMARY KEY,"
            + GEOFENCES_COLUMN_NODE_ID + " INTEGER NOT NULL,"
            + GEOFENCES_COLUMN_DKT_NODE_ID + " INTEGER NOT NULL,"
            + GEOFENCES_COLUMN_DKT_NO + " INTEGER NOT NULL,"
            + GEOFENCES_COLUMN_SUB_PROJECT_ID + " INTEGER NOT NULL,"
            + GEOFENCES_COLUMN_LAT + " DOUBLE,"
            + GEOFENCES_COLUMN_LONG + " DOUBLE,"
            + GEOFENCES_COLUMN_ISSTARTNODE + " TEXT, "
            + GEOFENCES_COLUMN_ISENDNODE + " TEXT, "
            + GEOFENCE_TRANSITION_STATUS + " INTEGER)";

    public static final String GEOFENCE_ARRIVAL_TABLE_NAME = "geofence_arrival";
    public static final String GEOFENCE_ARRIVAL_COLUMN_ID = "GEOFENCES_COLUMN_ID";
    public static final String GEOFENCE_ARRIVAL_COLUMN_NODE_ID = "GEOFENCES_COLUMN_NODE_ID";
    public static final String GEOFENCE_ARRIVAL_COLUMN_DKT_NODE_ID = "GEOFENCES_COLUMN_DKT_NODE_ID";
    public static final String GEOFENCE_ARRIVAL_COLUMN_DKT_NO = "GEOFENCES_COLUMN_DKT_NO";
    public static final String GEOFENCE_ARRIVAL_COLUMN_SUB_PROJECT_ID = "GEOFENCE_ARRIVAL_COLUMN_SUB_PROJECT_ID";
    public static final String GEOFENCE_ARRIVAL_COLUMN_ARRIVAL_TIME = "GEOFENCE_ARRIVAL_COLUMN_ARRIVAL_TIME";
    public static final String GEOFENCE_ARRIVAL_COLUMN_IS_DEPARTURE = "GEOFENCE_ARRIVAL_COLUMN_IS_DEPARTURE";
    public static final String GEOFENCE_ARRIVAL_COLUMN_LAT = "GEOFENCE_ARRIVAL_COLUMN_LAT";
    public static final String GEOFENCE_ARRIVAL_COLUMN_LONG = "GEOFENCE_ARRIVAL_COLUMN_LONG";
    public static final String GEOFENCE_ARRIVAL_TABLE_CREATE = "create table "
            + GEOFENCE_ARRIVAL_TABLE_NAME + "(" + GEOFENCE_ARRIVAL_COLUMN_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + GEOFENCE_ARRIVAL_COLUMN_NODE_ID + " INTEGER NOT NULL,"
            + GEOFENCE_ARRIVAL_COLUMN_DKT_NODE_ID + " INTEGER NOT NULL,"
            + GEOFENCE_ARRIVAL_COLUMN_DKT_NO + " INTEGER NOT NULL,"
            + GEOFENCE_ARRIVAL_COLUMN_SUB_PROJECT_ID + " INTEGER NOT NULL,"
            + GEOFENCE_ARRIVAL_COLUMN_ARRIVAL_TIME + " TEXT, "
            + GEOFENCE_ARRIVAL_COLUMN_LAT + " DOUBLE, "
            + GEOFENCE_ARRIVAL_COLUMN_LONG + " DOUBLE, "
            + GEOFENCE_ARRIVAL_COLUMN_IS_DEPARTURE + " TEXT) ";





    /*
     * EXCEPTION TYPES TABLE*/

    public static final String REASON_CODES_TABLE_NAME = "reasonCodes";
    public static final String REASON_CODES_KEY_ID = "REASON_CODES_KEY_ID";
    public static final String REASON_CODES_ID = "REASON_CODES_ID";
    public static final String REASON_CODES_NAME = "REASON_CODES_NAME";
    public static final String REASON_CODES_TYPE = "REASON_CODES_TYPE";

    public static final String REASON_CODES_DATABASE_CREATE = "CREATE TABLE "
            + REASON_CODES_TABLE_NAME + "("
            + REASON_CODES_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + REASON_CODES_ID + " INTEGER, "
            + REASON_CODES_TYPE + " TEXT, "
            + REASON_CODES_NAME + " TEXT); ";


    public DataHelper(Context context) {
        super(context, DataHelper.DATABASE_NAME, null,
                DataHelper.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        database.execSQL(DataHelper.SHARED_PREF_TABLE_CREATE);
        database.execSQL(DataHelper.GEOFENCES_TABLE_CREATE);
        database.execSQL(DataHelper.GEOFENCE_ARRIVAL_TABLE_CREATE);
        database.execSQL(DataHelper.REASON_CODES_DATABASE_CREATE);

        CommonUtils.logD(DataHelper.SHARED_PREF_TABLE_CREATE);
        CommonUtils.logD(DataHelper.REASON_CODES_DATABASE_CREATE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        CommonUtils.logI("Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");


    }


}
