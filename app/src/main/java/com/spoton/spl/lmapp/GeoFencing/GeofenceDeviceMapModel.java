package com.spoton.spl.lmapp.GeoFencing;

import java.util.List;

public class GeofenceDeviceMapModel{

	private int deviceId;
	private int active;
	private String timestamp;
	private String updatedTimesatmp;
	private List<DeviceInfoModal> deviceInfos;
	private GeoFenceModel geoFence;
	private int type; //1=assign, 2= cancel
	

	public GeofenceDeviceMapModel() {
	}

	@Override
	public String toString() {
		return "GeofenceDeviceMapModel{" +
				"deviceId=" + deviceId +
				", active=" + active +
				", timestamp='" + timestamp + '\'' +
				", updatedTimesatmp='" + updatedTimesatmp + '\'' +
				", deviceInfos=" + deviceInfos +
				", geoFence=" + geoFence +
				", type=" + type +
				'}';
	}

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}


	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getUpdatedTimesatmp() {
		return updatedTimesatmp;
	}

	public void setUpdatedTimesatmp(String updatedTimesatmp) {
		this.updatedTimesatmp = updatedTimesatmp;
	}


	public List<DeviceInfoModal> getDeviceInfos() {
		return deviceInfos;
	}

	public void setDeviceInfos(List<DeviceInfoModal> deviceInfos) {
		this.deviceInfos = deviceInfos;
	}

	public GeoFenceModel getGeoFence() {
		return geoFence;
	}

	public void setGeoFence(GeoFenceModel geoFence) {
		this.geoFence = geoFence;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}



}