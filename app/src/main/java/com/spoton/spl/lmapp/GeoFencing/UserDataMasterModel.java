package com.spoton.spl.lmapp.GeoFencing;

import java.util.List;

public class UserDataMasterModel {

	private int id;//primary key
	private String appPwd;
	private String appUserName;// user name
	private String currentlyUsed;
	private int is9digitPartNo;
	private int isActive;
	private int isConEntry;
	private int isDelivery;
	private int isOfflinePrinting;
	private int isPickupAppActive;
	private int isPickupRegistration;
	private int isPiecesEntry;
	private String location;
	private int loginStatus;
	private String printerSlNo;
	private String tabId;
	private int userId;
	private String verisonId;
	private List<UserLocationModel> userLocations;

	@Override
	public String toString() {
		return "UserDataMasterModel{" +
				"id=" + id +
				", appUserName='" + appUserName + '\'' +
				", userId=" + userId +
				'}';
	}

	public UserDataMasterModel() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAppPwd() {
		return appPwd;
	}

	public void setAppPwd(String appPwd) {
		this.appPwd = appPwd;
	}

	public String getAppUserName() {
		return appUserName;
	}

	public void setAppUserName(String appUserName) {
		this.appUserName = appUserName;
	}

	public String getCurrentlyUsed() {
		return currentlyUsed;
	}

	public void setCurrentlyUsed(String currentlyUsed) {
		this.currentlyUsed = currentlyUsed;
	}

	public int getIs9digitPartNo() {
		return is9digitPartNo;
	}

	public void setIs9digitPartNo(int is9digitPartNo) {
		this.is9digitPartNo = is9digitPartNo;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public int getIsConEntry() {
		return isConEntry;
	}

	public void setIsConEntry(int isConEntry) {
		this.isConEntry = isConEntry;
	}

	public int getIsDelivery() {
		return isDelivery;
	}

	public void setIsDelivery(int isDelivery) {
		this.isDelivery = isDelivery;
	}

	public int getIsOfflinePrinting() {
		return isOfflinePrinting;
	}

	public void setIsOfflinePrinting(int isOfflinePrinting) {
		this.isOfflinePrinting = isOfflinePrinting;
	}

	public int getIsPickupAppActive() {
		return isPickupAppActive;
	}

	public void setIsPickupAppActive(int isPickupAppActive) {
		this.isPickupAppActive = isPickupAppActive;
	}

	public int getIsPickupRegistration() {
		return isPickupRegistration;
	}

	public void setIsPickupRegistration(int isPickupRegistration) {
		this.isPickupRegistration = isPickupRegistration;
	}

	public int getIsPiecesEntry() {
		return isPiecesEntry;
	}

	public void setIsPiecesEntry(int isPiecesEntry) {
		this.isPiecesEntry = isPiecesEntry;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(int loginStatus) {
		this.loginStatus = loginStatus;
	}

	public String getPrinterSlNo() {
		return printerSlNo;
	}

	public void setPrinterSlNo(String printerSlNo) {
		this.printerSlNo = printerSlNo;
	}

	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getVerisonId() {
		return verisonId;
	}

	public void setVerisonId(String verisonId) {
		this.verisonId = verisonId;
	}

	public List<UserLocationModel> getUserLocations() {
		return userLocations;
	}

	public void setUserLocations(List<UserLocationModel> userLocations) {
		this.userLocations = userLocations;
	}


}