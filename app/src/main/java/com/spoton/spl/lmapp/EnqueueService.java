package com.spoton.spl.lmapp;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.core.app.JobIntentService;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class EnqueueService extends JobIntentService {
    public static final int JOB_ID = 0x01;
    private static final String TAG = "EnqueueService";
    private static final String TAG_BOOT_BROADCAST_RECEIVER = "BOOT_BROADCAST_RECEIVER";

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, EnqueueService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(Intent intent) {
        startServiceDirectly(EnqueueService.this);
    }

    /* Create an repeat Alarm that will invoke the background service for each execution time.
     * The interval time can be specified by your self.  */
    private void startServiceByAlarm(Context context) {
        // Get alarm manager.
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        // Create intent to invoke the background service.
//        Intent intent = new Intent(context, EmployeeLocationService.class);
//        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        long startTime = System.currentTimeMillis();
        long intervalTime = 60 * 1000;

        String message = "Start service use repeat alarm. ";

        Log.d(TAG_BOOT_BROADCAST_RECEIVER, message);

        // Create repeat alarm.
//        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, startTime, intervalTime, pendingIntent);
    }

    /* Start RunAfterBootService service directly and invoke the service every 10 seconds. */
    private void startServiceDirectly(Context context) {
        String message = "BootDeviceReceiver onReceive start service directly.";

        Log.d(TAG_BOOT_BROADCAST_RECEIVER, message);

        // This intent is used to start background service. The same service will be invoked for each invoke in the loop.
        Intent startServiceIntent = new Intent(context, EmployeeLocationService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.d(TAG, "startServiceDirectly: ");
            context.startForegroundService(startServiceIntent);
        } else {
            context.startService(startServiceIntent);
        }
    }


}
