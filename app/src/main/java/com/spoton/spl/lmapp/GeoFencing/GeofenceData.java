package com.spoton.spl.lmapp.GeoFencing;

/**
 * Created by NEERAJA on 24-11-2017.
 */
public class GeofenceData {
    private int gid;
    private double latitude;
    private double longitude;
    private double radius;

    @Override
    public String toString() {
        return "GeofenceData{" +
                "gid=" + gid +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", radius=" + radius +
                '}';
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
