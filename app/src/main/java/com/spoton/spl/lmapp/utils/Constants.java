package com.spoton.spl.lmapp.utils;

/**
 * Created by GAYATRI on 27-JUL-20.
 */

public class Constants {

    public static final String CONTENT_TYPE = "Content-Type";
    public static final String DEVICE_TOKEN = "deviceToken";
    public static final String LOG_TAG = "LastMileDelivery";
    public static final String IMEI = "imei";
    public static final String SHARED_PREF = "lastmiledelivery";
    public static final String IS_LOGIN = "isLogin";
    public static final String TRUE = "TRUE";
    public static final String FALSE = "FALSE";
    public static final String MOBILE_NUMBER = "MOBILE_NUMBER";
    public static final String TIME_DIFF = "TIME_DIFF";
    public static final boolean isUseingFileEncryption = false;
    public static final String LOCATION_INTERVAL = "locationInterval";
    public static final String INTERVAL_VALUE = "1";
    public static final String TC_NUMBER = "TC_NUMBER";
    public static final String RADIUS = "10";
    public static final String maxImageSize = "maxImageSize";
    public static final String TRUCK = "TRUCK";
    public static final String DATE_FORMAT_FILE = "dd-MM-yyyy";




    public static String PROB_WITH_SERVER;
    public static String SERVER_UNAVAILABLE;
    public static String SERVER_NOT_REACHABLE;
    public static String DEVICE_CONNECTIVITY;
    public static String ERROR_PARSING;
    public static String DATA_INVALID;
    public static String PROB_WITH_NETWORK;

    public static final String GEOFENCE_FILE = "sdcard/geofences.txt";
    public static final String LOCATIONS_FILE= "sdcard/locations.txt";
    public static final String LOCATIONS_PATH_FILE= "sdcard/locations_path.txt";


    public static final double GEOFENCE_RADIUS =500d; //1000;
    public static final long GEOFENCE_EXPIRATION_DURATION = 24 * 60 * 60 * 1000; //24 hrs in milliseconds


    public final static int RECIVED_MESSAGE_CODE = 22;

    public static final String LUX_SENSOR_THRESHOLD = "30";



    public static final int maxProgressRange = 100;
    public static final int ARRIVED = 0;
    public static final int DELIVER = 1;
    public static final int DEPART = 2;
    public static final int END = 5;
    public static final int PICKUP = 6;
    public static final String FOLDER_NAME = "LMD_PDF";
    public static final String LMD_IMAGES = "LastMile Delivery Images";
    public static final String ARRIVAL = "T";
    public static final String DEPARTURE = "D";




}
