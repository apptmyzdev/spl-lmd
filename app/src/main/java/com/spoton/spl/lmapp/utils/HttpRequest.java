package com.spoton.spl.lmapp.utils;


import com.spoton.spl.lmapp.responses.GeneralResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;


/**
 * Created by GAYATRI on 27-JUL-20.
 */

public class HttpRequest {
    public static final String CONTENT_TYPE = "application/json";

    public static InputStream getInputStreamFromUrl(String url, String headerName, String headerValue) throws LastMileDeliveryException {
        InputStream content = null;
        CommonUtils.logD("URL: "+ url);
        try {

            HttpGet httpGet = new HttpGet(url);
            if (CommonUtils.isValidString(headerName) && CommonUtils.isValidString(headerValue))
                httpGet.addHeader(headerName, headerValue);
            httpGet.addHeader("Accept", "*/*");
            CommonUtils.logD("Headers"+ "" + headerValue);
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse response = httpClient.execute(httpGet);
            Integer statusCode = response.getStatusLine().getStatusCode();
            CommonUtils.logD("Status code" + statusCode.toString());
            if (statusCode != 200 && statusCode != 201) {
                if (statusCode == 404) {
                    Globals.lastErrMsg = "Server not reachable";
                }
                else {
                    GeneralResponse errorresponse = (GeneralResponse) CommonUtils.parseResponse(response.getEntity().getContent(), GeneralResponse.class);
                    Globals.lastErrMsg = errorresponse.getMessage();
                }
                return null;
            } else {
                content = response.getEntity().getContent();
                return content;
            }

        } catch (UnknownHostException e) {
            e.printStackTrace();
            throw new LastMileDeliveryException("DEVICE_CONNECTIVITY", "");
        } catch (Exception e) {
            e.printStackTrace();
            Globals.lastErrMsg = e.getMessage();
            throw new LastMileDeliveryException("NETWORK_UNAVAILABLE", "");
        }
    }

    public static Object postData(String url, String data, String headerName, String headerValue, Class classOfT) throws LastMileDeliveryException {
        HttpResponse response;
        CommonUtils.logD("URL: "+ url);
        CommonUtils.logD("data: "+ data);
        try {


            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            if (CommonUtils.isValidString(headerName) && CommonUtils.isValidString(headerValue))
//                httpPost.addHeader(headerName, headerValue);
//                httpPost.addHeader(Constants.IMEI, "74875873");
                httpPost.setEntity(new StringEntity(data, "UTF-8"));
            httpPost.setHeader("Content-type", CONTENT_TYPE);
            response = httpClient.execute(httpPost);
            Integer statusCOde = response.getStatusLine().getStatusCode();
            CommonUtils.logD("Statuscode "+ statusCOde.toString());
            if (statusCOde != 200) {
                if (statusCOde == 404) {
                    Globals.lastErrMsg = Constants.SERVER_NOT_REACHABLE;
                } else {
//                    if (statusCOde > 400 && statusCOde < 500) {
//                        Globals.lastErrMsg = Constants.SERVER_NOT_REACHABLE;
//                        throw new LastMileDeliveryException("", Constants.PROB_WITH_SERVER);
//                    } else
                        if (statusCOde > 500) {
                        Globals.lastErrMsg = Constants.PROB_WITH_SERVER;
                        throw new LastMileDeliveryException("", Constants.SERVER_NOT_REACHABLE);
                    }

                    GeneralResponse errorResponse = (GeneralResponse) CommonUtils.parseResponse(response.getEntity().getContent(), GeneralResponse.class);
                    CommonUtils.logD("HTTP Request " + errorResponse.getMessage());
                    Globals.lastErrMsg = errorResponse.getMessage();
                        return  errorResponse;
                }
                return null;
            } else {
                Globals.lastErrMsg = "";
                return CommonUtils.parseResponse(response.getEntity().getContent(),
                        classOfT);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new LastMileDeliveryException(e.getMessage(), "");
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            throw new LastMileDeliveryException(e.getMessage(), "");
        } catch (IOException e) {
            e.printStackTrace();
            throw new LastMileDeliveryException(e.getMessage(), "");
        } catch (Exception e) {
            e.printStackTrace();
            throw new LastMileDeliveryException(e.getMessage(), "");
        }
    }


}
