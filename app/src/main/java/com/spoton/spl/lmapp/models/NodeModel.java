package com.spoton.spl.lmapp.models;

public class NodeModel {

    private int dktNodeId;
    private int nodeId;
    private int nodeTypeId;
    private String nodeType;
    private String nodeName;
    private String custCode;
    private Double lat;
    private Double lng;
    private String address1;
    private String address2;
    private String address3;
    private String state;
    private String stateName;
    private String city;
    private int pincode;
    private String createdTimestamp;
    private String updatedBy;
    private String updatedTimestamp;
    private String nodeSubprojectModels;
    private int sequence;
    private int dlyStatus;
    private boolean captured;
    private int isTransitDelayCaptured;

    public int getIsTransitDelayCaptured() {
        return isTransitDelayCaptured;
    }

    public void setIsTransitDelayCaptured(int isTransitDelayCaptured) {
        this.isTransitDelayCaptured = isTransitDelayCaptured;
    }

    public int getDktNodeId() {
        return dktNodeId;
    }

    public void setDktNodeId(int dktNodeId) {
        this.dktNodeId = dktNodeId;
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public int getNodeTypeId() {
        return nodeTypeId;
    }

    public void setNodeTypeId(int nodeTypeId) {
        this.nodeTypeId = nodeTypeId;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPincode() {
        return pincode;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }

    public String getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(String createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedTimestamp() {
        return updatedTimestamp;
    }

    public void setUpdatedTimestamp(String updatedTimestamp) {
        this.updatedTimestamp = updatedTimestamp;
    }

    public String getNodeSubprojectModels() {
        return nodeSubprojectModels;
    }

    public void setNodeSubprojectModels(String nodeSubprojectModels) {
        this.nodeSubprojectModels = nodeSubprojectModels;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public int getDlyStatus() {
        return dlyStatus;
    }

    public void setDlyStatus(int dlyStatus) {
        this.dlyStatus = dlyStatus;
    }

    public boolean isCaptured() {
        return captured;
    }

    public void setCaptured(boolean captured) {
        this.captured = captured;
    }

    @Override
    public String toString() {
        return "NodeModel{" +
                "dktNodeId=" + dktNodeId +
                ", nodeId=" + nodeId +
                ", nodeTypeId=" + nodeTypeId +
                ", nodeType='" + nodeType + '\'' +
                ", nodeName='" + nodeName + '\'' +
                ", custCode='" + custCode + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", address3='" + address3 + '\'' +
                ", state='" + state + '\'' +
                ", stateName='" + stateName + '\'' +
                ", city='" + city + '\'' +
                ", pincode=" + pincode +
                ", createdTimestamp='" + createdTimestamp + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedTimestamp='" + updatedTimestamp + '\'' +
                ", nodeSubprojectModels='" + nodeSubprojectModels + '\'' +
                ", sequence=" + sequence +
                ", dlyStatus=" + dlyStatus +
                ", captured=" + captured +
                '}';
    }
}
