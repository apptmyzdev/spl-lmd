package com.spoton.spl.lmapp.GeoFencing;

import com.google.gson.Gson;

public class UserLocationModel{

	private int id;
	private float accuracy;
	private String address;
	private String deviceTimestamp;
	private double distance;
	private double latitude;
	private double longitude;
	private String pincode;
	private String timestamp;
	private UserDataModel userDataMaster;
	private DeviceInfoModal deviceInfo;
	private String action;//Delay
	private String conAwb;
	private String scheduleIdPdc;
	private double noOfPieces;
	private long noOfCons;
	private double totalWeight;
	private String customerAddress;
	private String customerName;
	private String updatedTime;
	private String userName;//Delay params - start
	private int flag; // set 1 for delay - Mounika //2 for pickup, 3 for delivery - Twinkle
	private String startTime; // mandatory if flag = 1
	private String endTime; // mandatory if flag = 1
	//Delay params - end
	private double cumulativeDistance;
	private int plotFlag;

	private String battery;
	
	public UserLocationModel() {
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getAccuracy() {
		return this.accuracy;
	}

	public void setAccuracy(float accuracy) {
		this.accuracy = accuracy;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getDistance() {
		return this.distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getDeviceTimestamp() {
		return deviceTimestamp;
	}

	public void setDeviceTimestamp(String deviceTimestamp) {
		this.deviceTimestamp = deviceTimestamp;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public UserDataModel getUserDataMaster() {
		return userDataMaster;
	}

	public void setUserDataMaster(UserDataModel userDataMaster) {
		this.userDataMaster = userDataMaster;
	}

	public DeviceInfoModal getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(DeviceInfoModal deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public double getCumulativeDistance() {
		return cumulativeDistance;
	}

	public void setCumulativeDistance(double cumulativeDistance) {
		this.cumulativeDistance = cumulativeDistance;
	}

	public int getPlotFlag() {
		return plotFlag;
	}

	public void setPlotFlag(int plotFlag) {
		this.plotFlag = plotFlag;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getConAwb() {
		return conAwb;
	}

	public void setConAwb(String conAwb) {
		this.conAwb = conAwb;
	}

	public String getScheduleIdPdc() {
		return scheduleIdPdc;
	}

	public void setScheduleIdPdc(String scheduleIdPdc) {
		this.scheduleIdPdc = scheduleIdPdc;
	}

	public double getNoOfPieces() {
		return noOfPieces;
	}

	public void setNoOfPieces(double noOfPieces) {
		this.noOfPieces = noOfPieces;
	}

	public long getNoOfCons() {
		return noOfCons;
	}

	public void setNoOfCons(long noOfCons) {
		this.noOfCons = noOfCons;
	}

	public double getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(double totalWeight) {
		this.totalWeight = totalWeight;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public String getBattery() {
		return battery;
	}

	public void setBattery(String battery) {
		this.battery = battery;
	}
}