package com.spoton.spl.lmapp.GeoFencing;

import java.util.List;

public class GeoFenceModel {
	private int nodeId;
	private int dktNodeId;
	private int dktNo;
	private int subProjectId;
	private int geofenceType;//1 = SC Geofence, 2 = Customer Geofence
	private int isActive;
	private double latitude;
	private double longitude;
	private String name;
	private double radius;
	private String scCode;
	private String isStartNode;
	private String isEndNode;
	//private CustomerMasterDataModel customerMasterData;
	private List<GeoFenceUpdateModel> geoFenceUpdates;
	private List<GeofenceDeviceMapModel> geofenceDeviceMaps;
	private int isEntered;

	public GeoFenceModel(int nodeId, int dktNodeId, int dktNo, int isActive, double latitude, double longitude, double radius) {
		this.nodeId = nodeId;
		this.dktNodeId = dktNodeId;
		this.dktNo = dktNo;
		this.isActive = isActive;
		this.latitude = latitude;
		this.longitude = longitude;
		this.radius = radius;
	}
	public GeoFenceModel(int subProjectId,int nodeId, int dktNodeId, int dktNo, int isActive, double latitude, double longitude, double radius,String isStartNode,String isEndNode, int isEntered) {
		this.subProjectId = subProjectId;
		this.nodeId = nodeId;
		this.dktNodeId = dktNodeId;
		this.dktNo = dktNo;
		this.isActive = isActive;
		this.latitude = latitude;
		this.longitude = longitude;
		this.radius = radius;
		this.isStartNode = isStartNode;
		this.isEndNode = isEndNode;
		this.isEntered = isEntered;
	}

	public GeoFenceModel() {
	}

	public int getSubProjectId() {
		return subProjectId;
	}

	public void setSubProjectId(int subProjectId) {
		this.subProjectId = subProjectId;
	}

	public int getNodeId() {
		return nodeId;
	}

	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}

	public int getDktNodeId() {
		return dktNodeId;
	}

	public void setDktNodeId(int dktNodeId) {
		this.dktNodeId = dktNodeId;
	}

	public int getDktNo() {
		return dktNo;
	}

	public void setDktNo(int dktNo) {
		this.dktNo = dktNo;
	}

	public int getGeofenceType() {
		return geofenceType;
	}

	public void setGeofenceType(int geofenceType) {
		this.geofenceType = geofenceType;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public String getScCode() {
		return scCode;
	}

	public void setScCode(String scCode) {
		this.scCode = scCode;
	}

	public List<GeoFenceUpdateModel> getGeoFenceUpdates() {
		return geoFenceUpdates;
	}

	public void setGeoFenceUpdates(List<GeoFenceUpdateModel> geoFenceUpdates) {
		this.geoFenceUpdates = geoFenceUpdates;
	}

	public List<GeofenceDeviceMapModel> getGeofenceDeviceMaps() {
		return geofenceDeviceMaps;
	}

	public void setGeofenceDeviceMaps(List<GeofenceDeviceMapModel> geofenceDeviceMaps) {
		this.geofenceDeviceMaps = geofenceDeviceMaps;
	}

	public String getStartNode() {
		return isStartNode;
	}

	public void setStartNode(String startNode) {
		isStartNode = startNode;
	}


	public String getIsEndNode() {
		return isEndNode;
	}

	public void setIsEndNode(String isEndNode) {
		this.isEndNode = isEndNode;
	}

	public String getIsStartNode() {
		return isStartNode;
	}

	public void setIsStartNode(String isStartNode) {
		this.isStartNode = isStartNode;
	}

	public int getIsEntered() {
		return isEntered;
	}

	public void setIsEntered(int isEntered) {
		this.isEntered = isEntered;
	}

	@Override
	public String toString() {
		return "GeoFenceModel{" +
				"nodeId=" + nodeId +
				", dktNodeId=" + dktNodeId +
				", dktNo=" + dktNo +
				", subProjectId=" + subProjectId +
				", geofenceType=" + geofenceType +
				", isActive=" + isActive +
				", latitude=" + latitude +
				", longitude=" + longitude +
				", name='" + name + '\'' +
				", radius=" + radius +
				", scCode='" + scCode + '\'' +
				", isStartNode='" + isStartNode + '\'' +
				", isEndNode='" + isEndNode + '\'' +
				", geoFenceUpdates=" + geoFenceUpdates +
				", geofenceDeviceMaps=" + geofenceDeviceMaps +
				", isEntered=" + isEntered +
				'}';
	}
}