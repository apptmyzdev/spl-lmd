package com.spoton.spl.lmapp.GeoFencing;


public class GeofenceUpdatesHistoryModel{

	private int id;// no need to set from device.
	private int con;
	private String destinationAddress;
	private double destinationLat;
	private double destinationLng;
	private String deviceTimestamp;//To identify a enter record.
	private String enterTimestamp;
	private String etaNotifSentTimestamp;// no need to set from device.
	private int etaNotifstatus;// no need to set from device.
	private String exitTimestamp;
	private double geofenceRadius;
	private String mobileNo;
	private String notifContent;// no need to set from device.
	private String pdc;
	private int pickupScheduleId;
	private String timestamp;// no need to set from device.
	private int type;//1=pickup 2=delivery 3 = Sc geofence.
	private String updatedTimestamp;// no need to set from device.
	private DeviceInfoModal deviceInfo;
	private UserDataMasterModel userDataMaster;
	private String scCode;
	
	// next destination details
	private boolean sendETANotif;// true = send, false = dont send.
	private GeofenceUpdatesHistoryModel nextDestinationDetails;//For next destination(to which ETA should be sent) - lat,lng,type(1 or 2 only),pickupScheduleId,pdc,con(based on type) are mandatory.
	
	//vehicle current location.
	private double vehCurrentLat;
	private double vehCurrentLng;
	private String vehNo;

	@Override
	public String toString() {
		return "GeofenceUpdatesHistoryModel{" +
				"id=" + id +
				", con=" + con +
				", destinationAddress='" + destinationAddress + '\'' +
				", destinationLat=" + destinationLat +
				", destinationLng=" + destinationLng +
				", deviceTimestamp='" + deviceTimestamp + '\'' +
				", enterTimestamp='" + enterTimestamp + '\'' +
				", etaNotifSentTimestamp='" + etaNotifSentTimestamp + '\'' +
				", etaNotifstatus=" + etaNotifstatus +
				", exitTimestamp='" + exitTimestamp + '\'' +
				", geofenceRadius=" + geofenceRadius +
				", mobileNo='" + mobileNo + '\'' +
				", notifContent='" + notifContent + '\'' +
				", pdc='" + pdc + '\'' +
				", pickupScheduleId=" + pickupScheduleId +
				", timestamp='" + timestamp + '\'' +
				", type=" + type +
				", updatedTimestamp='" + updatedTimestamp + '\'' +
				", deviceInfo=" + deviceInfo +
				", userDataMaster=" + userDataMaster +
				", scCode='" + scCode + '\'' +
				", sendETANotif=" + sendETANotif +
				", nextDestinationDetails=" + nextDestinationDetails +
				", vehCurrentLat=" + vehCurrentLat +
				", vehCurrentLng=" + vehCurrentLng +
				", vehNo='" + vehNo + '\'' +
				'}';
	}

	public GeofenceUpdatesHistoryModel() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCon() {
		return con;
	}

	public void setCon(int con) {
		this.con = con;
	}

	public String getDestinationAddress() {
		return destinationAddress;
	}

	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	public double getDestinationLat() {
		return destinationLat;
	}

	public void setDestinationLat(double destinationLat) {
		this.destinationLat = destinationLat;
	}

	public double getDestinationLng() {
		return destinationLng;
	}

	public void setDestinationLng(double destinationLng) {
		this.destinationLng = destinationLng;
	}

	public String getEnterTimestamp() {
		return enterTimestamp;
	}

	public void setEnterTimestamp(String enterTimestamp) {
		this.enterTimestamp = enterTimestamp;
	}

	public String getEtaNotifSentTimestamp() {
		return etaNotifSentTimestamp;
	}

	public void setEtaNotifSentTimestamp(String etaNotifSentTimestamp) {
		this.etaNotifSentTimestamp = etaNotifSentTimestamp;
	}

	public int getEtaNotifstatus() {
		return etaNotifstatus;
	}

	public void setEtaNotifstatus(int etaNotifstatus) {
		this.etaNotifstatus = etaNotifstatus;
	}

	public String getExitTimestamp() {
		return exitTimestamp;
	}

	public void setExitTimestamp(String exitTimestamp) {
		this.exitTimestamp = exitTimestamp;
	}

	public double getGeofenceRadius() {
		return geofenceRadius;
	}

	public void setGeofenceRadius(double geofenceRadius) {
		this.geofenceRadius = geofenceRadius;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getNotifContent() {
		return notifContent;
	}

	public void setNotifContent(String notifContent) {
		this.notifContent = notifContent;
	}

	public String getPdc() {
		return pdc;
	}

	public void setPdc(String pdc) {
		this.pdc = pdc;
	}

	public int getPickupScheduleId() {
		return pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getUpdatedTimestamp() {
		return updatedTimestamp;
	}

	public void setUpdatedTimestamp(String updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}

	public DeviceInfoModal getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(DeviceInfoModal deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public UserDataMasterModel getUserDataMaster() {
		return userDataMaster;
	}

	public void setUserDataMaster(UserDataMasterModel userDataMaster) {
		this.userDataMaster = userDataMaster;
	}

	public boolean isSendETANotif() {
		return sendETANotif;
	}

	public void setSendETANotif(boolean sendETANotif) {
		this.sendETANotif = sendETANotif;
	}

	public GeofenceUpdatesHistoryModel getNextDestinationDetails() {
		return nextDestinationDetails;
	}

	public void setNextDestinationDetails(GeofenceUpdatesHistoryModel nextDestinationDetails) {
		this.nextDestinationDetails = nextDestinationDetails;
	}

	public double getVehCurrentLat() {
		return vehCurrentLat;
	}

	public void setVehCurrentLat(double vehCurrentLat) {
		this.vehCurrentLat = vehCurrentLat;
	}

	public double getVehCurrentLng() {
		return vehCurrentLng;
	}

	public void setVehCurrentLng(double vehCurrentLng) {
		this.vehCurrentLng = vehCurrentLng;
	}

	public String getDeviceTimestamp() {
		return deviceTimestamp;
	}

	public void setDeviceTimestamp(String deviceTimestamp) {
		this.deviceTimestamp = deviceTimestamp;
	}

	public String getScCode() {
		return scCode;
	}

	public void setScCode(String scCode) {
		this.scCode = scCode;
	}

	public String getVehNo() {
		return vehNo;
	}

	public void setVehNo(String vehNo) {
		this.vehNo = vehNo;
	}

	
}