package com.spoton.spl.lmapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.spoton.spl.lmapp.datacache.DataSource;
import com.spoton.spl.lmapp.utils.Constants;


public class SplashScreen extends AppCompatActivity {
    private static final int PERMISSION_CALLBACK_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    LinearLayout splashLl;
    Context context;
    String[] permissionsRequired = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION};
    private TextView versionTv;
    private SharedPreferences permissionStatus;
    private DataSource dataSource;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);


        splashLl = (LinearLayout) findViewById(R.id.logo_ll);
        versionTv = (TextView) findViewById(R.id.app_version_tv);

        context = SplashScreen.this;
        permissionStatus = getSharedPreferences("Mypref", 0);
        dataSource = new DataSource(context);
        String versionVal = "";
        try {
            versionVal = getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        versionTv.append(" : " + versionVal);


        Animation animation = AnimationUtils.loadAnimation(context, R.anim.splash_screen);
        splashLl.setVisibility(View.VISIBLE);

        splashLl.setAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                checkPermissions();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void checkPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
            if (ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[2]) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[3]) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[4]) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(SplashScreen.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                SharedPreferences.Editor editor = permissionStatus.edit();
                editor.putBoolean(permissionsRequired[0], true);
                editor.commit();
            } else {
                proceedAfterPermission();
            }


        } else {

            if (ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[2]) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[3]) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(SplashScreen.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                SharedPreferences.Editor editor = permissionStatus.edit();
                editor.putBoolean(permissionsRequired[0], true);
                editor.commit();
            } else {
                proceedAfterPermission();
            }

        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            boolean allgranted = false;

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {

                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        allgranted = true;
                    } else {
                        allgranted = false;
                        break;
                    }
                }
            } else {
                for (int i = 0; i < (grantResults.length) - 1; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        allgranted = true;
                    } else {
                        allgranted = false;
                        break;
                    }
                }

            }

            if (allgranted) {

                proceedAfterPermission();
            } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {


                if (ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, permissionsRequired[0])
                        || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, permissionsRequired[1])
                        || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, permissionsRequired[2])
                        || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, permissionsRequired[3])
                        || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, permissionsRequired[4])) {
//                    || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, permissionsRequired[2])) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreen.this);
                    builder.setTitle(getString(R.string.need_permissions));
                    builder.setMessage(getString(R.string.device_permission));
                    builder.setPositiveButton(getString(R.string.grant), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ActivityCompat.requestPermissions(SplashScreen.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                        }
                    });
                    builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();

                } else
                    proceedAfterPermission();
            } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, permissionsRequired[0])
                        || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, permissionsRequired[1])
                        || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, permissionsRequired[2])
                        || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, permissionsRequired[3])) {
//                    || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, permissionsRequired[2])) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreen.this);
                    builder.setTitle(getString(R.string.need_permissions));
                    builder.setMessage(getString(R.string.device_permission));
                    builder.setPositiveButton(getString(R.string.grant), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ActivityCompat.requestPermissions(SplashScreen.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                        }
                    });
                    builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();

                } else{
                    proceedAfterPermission();
                }

            } else {
                Toast.makeText(getBaseContext(), getString(R.string.unable_toget_permission), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PERMISSION_SETTING) {

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {

                if (ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[1]) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[2]) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[3]) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[4]) == PackageManager.PERMISSION_GRANTED) {

                    proceedAfterPermission();
                }
            } else {
                if (ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[1]) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[2]) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[3]) == PackageManager.PERMISSION_GRANTED) {

                    proceedAfterPermission();

                }
            }
        }
    }

    private void proceedAfterPermission() {
//        if(checkGPSStatus())
        goNextScreen();
    }

    private void goNextScreen() {
        Intent intent;
        if (dataSource.sharedPreferences.getValue(Constants.IS_LOGIN).equalsIgnoreCase(Constants.TRUE)) {
//            intent = new Intent(context, TCDetailsScreen.class);
            intent = new Intent(context, HomeScreen.class);
        } else
            intent = new Intent(context, LoginScreen.class);
        startActivity(intent);
        finish();
    }

    private boolean checkGPSStatus() {
        boolean isGpsTurnedon = false;
        LocationManager locationManager = null;
        boolean gps_enabled = false;
        boolean network_enabled = false;
        if (locationManager == null) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        }
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        if (!gps_enabled && !network_enabled) {
            isGpsTurnedon = false;
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setCancelable(false);
            dialog.setMessage(getString(R.string.turn_on_gps));
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //this will navigate user to the device location settings screen
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                    dialog.dismiss();
                }
            });
            AlertDialog alert = dialog.create();
            alert.show();
        } else
            isGpsTurnedon = true;

        return isGpsTurnedon;
    }

//    @Override
//    protected void onPostResume() {
//        super.onPostResume();
//        if(checkGPSStatus())
//            goNextScreen();
//    }



}
