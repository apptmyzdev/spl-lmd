package com.spoton.spl.lmapp.utils;

/**
 * Created by GAYATRI on 28-Feb-20.
 */

public class LastMileDeliveryException extends Exception {
    String displayMessage;
    String detailedMessage;

    public LastMileDeliveryException(String displayMessage, String detailedMessage) {
        super();
        this.displayMessage = displayMessage;
        this.detailedMessage = detailedMessage;
    }

    public String getDisplayMessage() {

        return displayMessage;
    }

    public void setDisplayMessage(String displayMessage) {
        this.displayMessage = displayMessage;
    }

    public String getDetailedMessage() {
        return detailedMessage;
    }

    public void setDetailedMessage(String detailedMessage) {
        this.detailedMessage = detailedMessage;
    }

    @Override
    public String toString() {
        return displayMessage + detailedMessage;
    }
}
