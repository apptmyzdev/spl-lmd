package com.spoton.spl.lmapp.GeoFencing;


public class GeoFenceUpdateModel {

	private int id;
	private String assignTimestamp;
	private String enterTimestamp;
	private String exitTimestamp;
	private String timestamp;
	private int type;
	private DeviceInfoModal deviceInfo;
	private GeoFenceModel geoFence;
	private UserDataMasterModel userDataMaster;

	public GeoFenceUpdateModel() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getAssignTimestamp() {
		return assignTimestamp;
	}

	public void setAssignTimestamp(String assignTimestamp) {
		this.assignTimestamp = assignTimestamp;
	}

	public String getEnterTimestamp() {
		return enterTimestamp;
	}

	public void setEnterTimestamp(String enterTimestamp) {
		this.enterTimestamp = enterTimestamp;
	}

	public String getExitTimestamp() {
		return exitTimestamp;
	}

	public void setExitTimestamp(String exitTimestamp) {
		this.exitTimestamp = exitTimestamp;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public DeviceInfoModal getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(DeviceInfoModal deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public GeoFenceModel getGeoFence() {
		return geoFence;
	}

	public void setGeoFence(GeoFenceModel geoFence) {
		this.geoFence = geoFence;
	}

	public UserDataMasterModel getUserDataMaster() {
		return userDataMaster;
	}

	public void setUserDataMaster(UserDataMasterModel userDataMaster) {
		this.userDataMaster = userDataMaster;
	}


}