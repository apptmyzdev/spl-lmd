package com.spoton.spl.lmapp.GeoFencing;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.spoton.spl.lmapp.R;
import com.spoton.spl.lmapp.datacache.DataSource;
import com.spoton.spl.lmapp.utils.Constants;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GeofenceStore implements OnCompleteListener<Void> {

    private final String TAG = this.getClass().getSimpleName();

    /**
     * Context
     */
    private Context mContext;

    /**
     * Google API client object.
     */
    private GoogleApiClient mGoogleApiClient;

    /**
     * Geofencing PendingIntent
     */
    private PendingIntent mPendingIntent;

    /**
     * List of geofences to monitor.
     */
    private ArrayList<Geofence> mGeofences;

    /**
     * Geofence request.
     */
    private GeofencingRequest mGeofencingRequest;

    /**
     * Location Request object.
     */
    private LocationRequest mLocationRequest;

    private DataSource dataSource;
    private List<GeoFenceModel> mGeofenceDatas;

    private GeofencingClient mGeofencingClient;
    private int pushMessageId;

    /**
     * Constructs a new GeofenceStore.
     *
     * @param context   The context to use.
     * @param geofences List of geofences to monitor.
     */
    public GeofenceStore(Context context, ArrayList<Geofence> geofences, List<GeoFenceModel> geofenceDatas, int messageId) {
        pushMessageId = messageId;
        mContext = context;
        dataSource = new DataSource(mContext);
        mGeofences = new ArrayList<Geofence>(geofences);
        mGeofenceDatas = geofenceDatas;
        mPendingIntent = null;

        mGeofencingClient = LocationServices.getGeofencingClient(mContext);

        //Add Geofences
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        //to notify if the device is already in a freshly added geofence
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        if (!checkPermissions()) {
            Toast.makeText(mContext, "Location Permission not granted", Toast.LENGTH_LONG).show();
            return;
        } else {
            mGeofencingRequest = builder.addGeofences(
                    mGeofences).build();

            mPendingIntent = createRequestPendingIntent();

            //mGeofencingClient.addGeofences(mGeofencingRequest, mPendingIntent)
              //      .addOnCompleteListener(this);
        }
    }


    /**
     * This creates a PendingIntent that is to be fired when geofence transitions
     * take place. In this instance, we are using an IntentService to handle the
     * transitions.
     *
     * @return A PendingIntent that will handle geofence transitions.
     */
    private PendingIntent createRequestPendingIntent() {
        if (mPendingIntent == null) {
            Log.v(TAG, "Creating PendingIntent");
            Intent intent = new Intent(mContext, GeofenceIntentService.class);
            mPendingIntent = PendingIntent.getService(mContext, 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        }

        return mPendingIntent;
    }

    @Override
    public void onComplete(Task<Void> task) {
        if (task.isSuccessful()) {

            int messageId = R.string.geofences_added;
            for (Geofence g : mGeofences) {

            }

        } else {
            // Get the status code for the error and log it using a user-friendly message.
            String errorMessage = GeofenceErrorMessages.getErrorString(mContext, task.getException());
//            Log.w(TAG, errorMessage);
            appendLog(errorMessage, Constants.GEOFENCE_FILE);
            //follow up - verify code once
            GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
            mGeofencingRequest = builder.addGeofences(
                    mGeofences).build();

            mPendingIntent = createRequestPendingIntent();

            mGeofencingClient.addGeofences(mGeofencingRequest, mPendingIntent)
                    .addOnCompleteListener(this);
        }
    }

    public void appendLog(String text, String filename) {
        File logFile = new File(filename);
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

}
