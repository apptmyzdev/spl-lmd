package com.spoton.spl.lmapp.responses;

import com.spoton.spl.lmapp.models.LmdDeliveryResponseModel;

public class UpdateNodeResponse {
    private Boolean status;
    private String message;
    private LmdDeliveryResponseModel data;
    private int statusCode;
    private String token;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LmdDeliveryResponseModel getData() {
        return data;
    }

    public void setData(LmdDeliveryResponseModel data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "UpdateNodeResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", statusCode=" + statusCode +
                ", token='" + token + '\'' +
                '}';
    }
}
