package com.spoton.spl.lmapp.utils;

/**
 * Created by GAYATRI on 27-JUL-20.
 */

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.BatteryManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.spoton.spl.lmapp.BuildConfig;
import com.spoton.spl.lmapp.R;
import com.spoton.spl.lmapp.fileutils.FileCache;
import com.spoton.spl.lmapp.fileutils.ImageLoader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class CommonUtils {

    private static final String TAG = CommonUtils.class.getSimpleName();
    private static final Gson gson = new Gson();
    static RelativeLayout rl;
    private static ProgressBar progressBar;
    private static ProgressBar mProgressBar;
    private static ProgressDialog dialog;
    private static AlertDialog alertDialog;
    private static HashMap<String, Double> feeMatrix;


    public static ProgressDialog getProgressDialog(Context context) {
        dismissProgressDialog();
        dialog = new ProgressDialog(context, R.style.StyledDialog);
//        dialog.setTitle(context.getString(R.string.loading));
//        dialog.setMessage(context.getString(R.string.progress_dialog_title));
//        dialog.setIndeterminateDrawable(context.getResources().getDrawable(
//                R.drawable.progress_dialog));
//        dialog.setIndeterminate(true);
//        dialog.setCancelable(false);
//
//        dialog.show();
        try {
            SpannableString msg = new SpannableString("Loading. Please wait...");
            msg.setSpan(new ForegroundColorSpan(Color.BLACK), 0, msg.length(), 0);
            dialog.setIndeterminateDrawable(context.getResources().getDrawable(
                    R.drawable.progress_animation));
            dialog.setMessage(msg);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            CommonUtils.logE(e.toString());
        }
        return dialog;
    }

    public static Dialog LoadingSpinner(Context mContext) {
        Dialog pd = new Dialog(mContext, android.R.style.Theme_Black);
        View view = LayoutInflater.from(mContext).inflate(R.layout.progress_bar_layout, null);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.getWindow().setBackgroundDrawableResource(R.color.transperantWhite);
        pd.setContentView(view);
        return pd;
    }

    public static void getProgressBar(Context context) {
        dismissProgressDialog();
        ViewGroup layout = (ViewGroup) ((Activity) context).findViewById(android.R.id.content)
                .getRootView();

        mProgressBar = new ProgressBar(context, null, android.R.attr.progressBarStyle);
        mProgressBar.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.progress_bar_animation));
        mProgressBar.setBackground(new ColorDrawable(Color.TRANSPARENT));


        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);

        rl = new RelativeLayout(context);

        rl.setGravity(Gravity.CENTER);
        rl.addView(mProgressBar);
        rl.setBackgroundColor(Color.parseColor("#88000000"));

        layout.addView(rl, params);


    }

    public static void dismissProgressDialog() {
//        if (mProgressBar != null)
//            mProgressBar.setVisibility(View.GONE);
//        if (rl != null)
//            rl.setBackground(new ColorDrawable(Color.TRANSPARENT));

        if (dialog != null && dialog.isShowing() == true)
            dialog.dismiss();
    }


    public static Boolean isValidString(String str) {
        if (str != null) {
            str = str.trim();
            if (str.length() > 0)
                return true;
        }
        return false;
    }

    public static Boolean isValidArraylist(List list) {
        if (list != null && list.size() > 0)
            return true;
        else
            return false;
    }

    public static Object parseResponse(InputStream is, Class<?> classOfT) throws Exception {
        try {
            Reader reader = new InputStreamReader(is);
            return gson.fromJson(reader, classOfT);

        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new Exception();
    }

    public static void savePreferences(Context context, String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();


    }

    public static String loadPreferences(Context context, String key) {
        SharedPreferences sharedPreferences = ((Activity) context).getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        String value = "";
        try {
            value = sharedPreferences.getString(key, "");
        } catch (Exception e) {
            CommonUtils.logE(e.getMessage());
            value = "";

        }
        return value;
    }

    //    public static void removePreferences(Context context, String key) {
//        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.clear();
//        editor.commit();
//    }
    public static void removePreferences(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }


    public static void logE(String msg) {
        if (BuildConfig.DEBUG)
            Log.e(Constants.LOG_TAG, msg);
    }

    public static void logV(String msg) {
        if (BuildConfig.DEBUG)
            Log.v(Constants.LOG_TAG, msg);
    }


    public static void logI(String msg) {
        if (BuildConfig.DEBUG)
            Log.i(Constants.LOG_TAG, msg);
    }

    public static void logD(String msg) {
        if (BuildConfig.DEBUG)
            Log.d(Constants.LOG_TAG, msg);
    }

    public static void checkLocationPermissions(Context context, Activity activity) {

        Integer result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        CommonUtils.logD("result" + result.toString());
        if (result != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

//    public static void showErrorDialog(Context context) {
//
//        if (CommonUtils.isValidString(Globals.lastErrMsg)) {
//            alertDialog = new AlertDialog.Builder(context).create();
//            alertDialog.setTitle("Alert");
//            alertDialog.setCancelable(false);
//            Log.e("Tag", Globals.lastErrMsg);
//            alertDialog.setMessage(Globals.lastErrMsg);
//            Globals.lastErrMsg = "";
//            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
//                    alertDialog.dismiss();
//
//                }
//            });
//
//
//            alertDialog.show();
//        }
//    }

    public static Object parseResponsePic(String is, Class<?> classOfT)
            throws Exception {
        try {
            return gson.fromJson(is, classOfT);
        } catch (Exception e) {
            throw e;
        }
    }


    public static boolean showErrorDialog(final Context context) {
        boolean isNotErr = true;
        final AlertDialog errDlg;
        String error;

        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            errDlg = new AlertDialog.Builder(context).create();
            errDlg.setTitle("Alert");
            errDlg.setCancelable(false);
            error = Globals.lastErrMsg;
            errDlg.setMessage(Globals.lastErrMsg);
            Globals.lastErrMsg = "";
            //Utils.dismissProgressDialog();
            isNotErr = false;
            errDlg.setButton("Ok",
                    new DialogInterface.OnClickListener() {
                        //
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            errDlg.dismiss();
                        }
                    });
//            if (!((Activity) context).isFinishing())
//                errDlg.show();
//            else
            Toast.makeText(context, error, Toast.LENGTH_LONG).show();
        }

        return isNotErr;
    }

    public static boolean showDialog(final Context context) {
        boolean isNotErr = true;
        final AlertDialog errDlg;
        String error;

        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            errDlg = new AlertDialog.Builder(context).create();
            errDlg.setTitle("Alert");
            errDlg.setCancelable(false);
            error = Globals.lastErrMsg;
            errDlg.setMessage(Globals.lastErrMsg);
            Globals.lastErrMsg = "";
            //Utils.dismissProgressDialog();
            isNotErr = false;
            errDlg.setButton("Ok",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            errDlg.dismiss();
                        }
                    });
            if (!((Activity) context).isFinishing())
                errDlg.show();

        }

        return isNotErr;
    }


    public static void showToastLong(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();

    }

    public static void showToastShort(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

//    public static String getImei(Context context) {
//        String imei = "";
//        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return null;
//        } else {
//            try {
//                TelephonyManager mTelephony = (TelephonyManager) context
//                        .getSystemService(Context.TELEPHONY_SERVICE);
//                if (mTelephony.getDeviceId() != null) {
//                    imei = mTelephony.getDeviceId();
//                    CommonUtils.logD("Imei" + imei);
//                    return imei;
//                }
//            } catch (Exception e) {
//
//                if (isValidString(FirebaseInstanceId.getInstance().getId())) {
//                    CommonUtils.logD("Imei" + FirebaseInstanceId.getInstance().getId());
//                    return FirebaseInstanceId.getInstance().getId();
//                }
//
//
//            }
//
//
//        }
//        if (!CommonUtils.isValidString(imei)) {
//
//            if (isValidString(FirebaseInstanceId.getInstance().getId())) {
//                CommonUtils.logD("Imei" + FirebaseInstanceId.getInstance().getId());
//                return FirebaseInstanceId.getInstance().getId();
//            }
//
//        }
//
//        return null;
//    }


    public static void dissmissKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static String getImei(Context context) {
        String imei = "";
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        } else {
            try {
                TelephonyManager mTelephony = (TelephonyManager) context
                        .getSystemService(Context.TELEPHONY_SERVICE);
                if (mTelephony.getDeviceId() != null) {
                    imei = mTelephony.getDeviceId();
                    CommonUtils.logD("Imei" + imei);
                    return imei;
                }
            } catch (Exception e) {

                if (isValidString(FirebaseInstanceId.getInstance().getId())) {
                    CommonUtils.logD("Imei" + FirebaseInstanceId.getInstance().getId());
                    return FirebaseInstanceId.getInstance().getId();
                }


            }


        }
        if (!CommonUtils.isValidString(imei)) {

            if (isValidString(FirebaseInstanceId.getInstance().getId())) {
                CommonUtils.logD("Imei" + FirebaseInstanceId.getInstance().getId());
                return FirebaseInstanceId.getInstance().getId();
            }

        }

        return null;
    }


    public static String getImageTime(long diff) {
        String scanTime = "";
        Date d = new Date();
        long t = d.getTime();
        long t1 = t + diff;
        Date date = new Date(t1);
        scanTime = date.getTime() + "";
        return scanTime;
    }

    public static String getSheetFolderName() {
//        String sheetNo = Globals.selectedSheet.getSheetNo();
//        String carrier = Globals.selectedSheet.getCarrier();

        String fileName = Constants.TRUCK;

        return fileName;
    }

    public static ImageLoader getImageLoader(Context context) {
        return new ImageLoader(context, CommonUtils.getSheetFolderName());
    }

    public static String getTime(Context context) {
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        String format = s.format(calendar.getTime());
        return format;
    }

    public static void writeToAudit(Context context, String text) {
        try {
            FileCache fileCache = new FileCache(context, "DATA", FileCache.textTyp);
            String name = "AUDIT - " + CommonUtils.getTime(context);
            fileCache.saveTextFile(text, name);
        } catch (Exception e) {
            CommonUtils.logE(e.toString());
        }
    }

    public static void appendLog(String text, String filename) {
//        if (BuildConfig.DEBUG){
        File logFile = new File(filename);
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        }
    }




    public static int getBatteryPercentage(Context context) {

        if (Build.VERSION.SDK_INT >= 21) {

            BatteryManager bm = (BatteryManager) context.getSystemService(Context.BATTERY_SERVICE);
            return bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

        } else {

            IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = context.registerReceiver(null, iFilter);

            int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
            int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

            double batteryPct = level / (double) scale;

            return (int) (batteryPct * 100);
        }
    }

}
