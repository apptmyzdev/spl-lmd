package com.spoton.spl.lmapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.spoton.spl.lmapp.datacache.DataSource;


public class BootDeviceReceiver extends BroadcastReceiver {

    private static final String TAG_BOOT_BROADCAST_RECEIVER = "BOOT_BROADCAST_RECEIVER";
    private DataSource dataSource;

    @Override
    public void onReceive(Context context, Intent intent) {
        dataSource = new DataSource(context);
//        if (Utils.isValidString(dataSource.sharedPreferences.getValue(Constants.USERNAME_PREF))) {
        String action = intent.getAction();

        String message = "BootDeviceReceiver onReceive, action is " + action;

        Log.d(TAG_BOOT_BROADCAST_RECEIVER, action);

        if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
            EnqueueService.enqueueWork(context, new Intent());
        }
    }
//    }
}
