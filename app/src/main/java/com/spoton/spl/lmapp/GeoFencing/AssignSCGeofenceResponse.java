package com.spoton.spl.lmapp.GeoFencing;

import java.util.List;

/**
 * Created by NEERAJA on 22-12-2017.
 */
public class AssignSCGeofenceResponse {
    private boolean status;
    private String message;
    private List<GeoFenceModel> data;

    @Override
    public String toString() {
        return "AssignSCGeofenceResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GeoFenceModel> getData() {
        return data;
    }

    public void setData(List<GeoFenceModel> data) {
        this.data = data;
    }
}
