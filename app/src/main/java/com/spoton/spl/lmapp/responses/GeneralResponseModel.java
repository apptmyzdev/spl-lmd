package com.spoton.spl.lmapp.responses;

public class GeneralResponseModel {

    private Boolean status;
    private String message;
    private String data;
    private int statusCode;
    private String token;




    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    @Override
    public String toString() {
        return "GeneralResponseModel{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data='" + data + '\'' +
                ", statusCode=" + statusCode +
                ", token='" + token + '\'' +
                '}';
    }
}
