package com.spoton.spl.lmapp.datacache;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.spoton.spl.lmapp.GeoFenceArrivalPushService;
import com.spoton.spl.lmapp.GeoFencing.GeoFenceModel;
import com.spoton.spl.lmapp.models.LatLng;
import com.spoton.spl.lmapp.models.ReasonsModel;
import com.spoton.spl.lmapp.models.UpdateNodeModel;
import com.spoton.spl.lmapp.utils.CommonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataSource {

    public ShardPreferences sharedPreferences;
    public GeoFences geoFences;
    public GeoFenceArrivalData geoFenceArrivalData;
    public ReasonCodes reasonCodes;
    private SQLiteDatabase database;
    private DataHelper dbHelper;

    public DataSource(Context context) {

        dbHelper = new DataHelper(context);
        sharedPreferences = new ShardPreferences();
        geoFences = new GeoFences();
        geoFenceArrivalData = new GeoFenceArrivalData();
        reasonCodes = new ReasonCodes();


    }

    @SuppressLint("NewApi")
    public void open() throws SQLException {
//        Utils.logD("Open Write DB");
        if (database != null) {
            while (database.isOpen()) {
                try {
                    Thread.sleep(50);
                } catch (Exception e) {
                    CommonUtils.logE(e.toString());
                }
            }
        }
        if ((database != null && !database.isOpen()) || database == null) {
            try {
                database = dbHelper.getWritableDatabase();
                // isDatabaseOpen = true;
            }
            // catch (SQLiteDatabaseLockedException e) {
            // Utils.logE(e.toString());
            // }
            catch (Exception e) {
                CommonUtils.logE(e.toString());
            }
        }
    }

    public void openRead() throws SQLException {
//        Utils.logD("Open Read DB");
        if ((database != null && !database.isOpen()) || database == null) {
            database = dbHelper.getReadableDatabase();
            // isDatabaseOpen = true;
        }
    }

    public void close() {
//        Utils.logD("Close DB");
        if ((database != null && database.isOpen())) {
            dbHelper.close();
            // isDatabaseOpen = false;
        }
    }


    /**************** shared preferences ***************/
    public class ShardPreferences {

        public void truncateTable() {
            open();

            if (database != null) {
                String query = "DELETE FROM " + DataHelper.SHARED_PREF_TABLE_NAME;
                database.execSQL(query);
            }

            close();
        }

        public void set(String key, String value) {
            open();
            try {
                if (!CommonUtils.isValidString(value))
                    value = "";

                if (CommonUtils.isValidString(key)) {
                    int id = exists(key);
                    if (id == -1) {
                        create(key, value);
                    } else {
                        update(key, value);
                    }
                }
            } catch (Exception e) {
                CommonUtils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void update(String key, String value) {
            open();
            try {
                ContentValues values = new ContentValues();
                values.put(DataHelper.SHARED_PREF_COLUMN_VALUE, value);

                if (database.update(DataHelper.SHARED_PREF_TABLE_NAME, values,
                        DataHelper.SHARED_PREF_COLUMN_KEY + " = '" + key + "'",
                        null) > 0) {
                } else {
                }
            } catch (Exception e) {
                CommonUtils.logE(e.toString());
            } finally {
                close();
            }

        }

        public void create(String key, String value) {
            open();
            try {
                ContentValues values = new ContentValues();
                values.put(DataHelper.SHARED_PREF_COLUMN_KEY, key);
                values.put(DataHelper.SHARED_PREF_COLUMN_VALUE, value);

                if (database.insert(DataHelper.SHARED_PREF_TABLE_NAME, null,
                        values) > 0) {
                } else {
                }
            } catch (Exception e) {
                CommonUtils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void delete(String key) {
            open();
            try {
                if (database.delete(DataHelper.SHARED_PREF_TABLE_NAME,
                        DataHelper.SHARED_PREF_COLUMN_KEY + " = '" + key + "'",
                        null) > 0) {
                } else {
                }
            } catch (Exception e) {
                CommonUtils.logE(e.toString());
            } finally {
                close();
            }
        }

        public List<SharedPreferenceItem> getAll() {
            openRead();
            List<SharedPreferenceItem> items = new ArrayList<SharedPreferenceItem>();
            Cursor cursor = null;
            try {

                cursor = database.query(DataHelper.SHARED_PREF_TABLE_NAME,
                        DataHelper.SHARED_PREF_COLUMNS, null, null, null, null,
                        null);

                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    SharedPreferenceItem item = cursorToItem(cursor);
                    items.add(item);
                    cursor.moveToNext();
                }

            } catch (Exception e) {
                CommonUtils.logE(e.toString());
                cursor = null;
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return items;
        }

        public String getValue(String key) {
            openRead();

            Cursor cursor = null;
            String value = "";

            try {
                String selectQuery = "SELECT  "
                        + DataHelper.SHARED_PREF_COLUMN_VALUE + " FROM "
                        + DataHelper.SHARED_PREF_TABLE_NAME + " WHERE "
                        + DataHelper.SHARED_PREF_COLUMN_KEY + " = '" + key
                        + "'";

                cursor = database.rawQuery(selectQuery, null);

                if (cursor.moveToFirst()) {
                    value = cursor.getString(0);
                }

            } catch (Exception e) {
                CommonUtils.logE(e.toString());
                cursor = null;
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return value;

        }

        public int exists(String key) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.SHARED_PREF_KEY_ID
                        + " FROM " + DataHelper.SHARED_PREF_TABLE_NAME
                        + " WHERE " + DataHelper.SHARED_PREF_COLUMN_KEY
                        + " = '" + key + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                CommonUtils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        private SharedPreferenceItem cursorToItem(Cursor cursor) {
            return new SharedPreferenceItem(cursor.getString(0),
                    cursor.getString(1));
        }
    }


    /***************************** GEOFENCES TABLE *******************************/


    public class GeoFences {
        public void truncatetable() {
            open();
            if (database != null) {
                String query = "DELETE FROM " + DataHelper.GEOFENCES_TABLE_NAME;
                database.execSQL(query);
            }
            close();
        }


        public void deleteGeofences(long dktNo) {
            open();
            if (database != null) {
                String query = "DELETE FROM " + DataHelper.GEOFENCES_TABLE_NAME +
                        " WHERE " + DataHelper.GEOFENCES_COLUMN_DKT_NO + " = " + dktNo;
                database.execSQL(query);
            }
            close();
        }

        public void insertData(List<GeoFenceModel> data) {
            int noOfRecords = 0;
//            truncatetable();

            open();
            try {
                if (CommonUtils.isValidArraylist(data)) {

                    for (GeoFenceModel model : data) {
                        ContentValues values = new ContentValues();
                        String dktNodeIdNodeId = model.getDktNodeId() + "-" + model.getNodeId();
                        String sql = "SELECT COUNT(" + DataHelper.GEOFENCES_COLUMN_DKT_NODE_ID_NODE_ID
                                + ") FROM " + DataHelper.GEOFENCES_TABLE_NAME + " WHERE "
                                + DataHelper.GEOFENCES_COLUMN_DKT_NODE_ID_NODE_ID + " ='" + dktNodeIdNodeId + "'";
                        Cursor cursor = database.rawQuery(sql, null);
                        CommonUtils.logD(sql);
                        int count = 0;checkGeofenceTransitionlat:
                        if (cursor.moveToFirst()) {
                            do {
                                count = cursor.getInt(0);

                            } while (cursor.moveToNext());
                        }
                        CommonUtils.logD(String.valueOf(count));

                        if (count <= 0) {


                            values.put(DataHelper.GEOFENCES_COLUMN_DKT_NODE_ID_NODE_ID, dktNodeIdNodeId);
                            values.put(DataHelper.GEOFENCES_COLUMN_NODE_ID, model.getNodeId());
                            values.put(DataHelper.GEOFENCES_COLUMN_DKT_NO, model.getDktNo());
                            values.put(DataHelper.GEOFENCES_COLUMN_SUB_PROJECT_ID, model.getSubProjectId());
                            values.put(DataHelper.GEOFENCES_COLUMN_DKT_NODE_ID, model.getDktNodeId());
                            values.put(DataHelper.GEOFENCES_COLUMN_LAT, model.getLatitude());
                            values.put(DataHelper.GEOFENCES_COLUMN_LONG, model.getLongitude());
                            values.put(DataHelper.GEOFENCES_COLUMN_ISSTARTNODE, model.getStartNode());
                            values.put(DataHelper.GEOFENCES_COLUMN_ISENDNODE, model.getIsEndNode());

                            if (database.insert(DataHelper.GEOFENCES_TABLE_NAME, null,
                                    values) > 0) {
                                noOfRecords++;
                            } else {
                            }
                            CommonUtils.logD(noOfRecords + " Records inserted sucessful");

                        }
                    }
                }
            } catch (Exception e) {
                CommonUtils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void updateTransition(LatLng latLng, int status) {
            open();
            int transitionStatus = -1;
            Cursor cursor = null;
            try {
                ContentValues values = new ContentValues();
                values.put(DataHelper.GEOFENCE_TRANSITION_STATUS, status);
                int rows = database.update(DataHelper.GEOFENCES_TABLE_NAME, values,
                        DataHelper.GEOFENCES_COLUMN_LAT + " = '" + latLng.getLatitude() + "' "
                                + " AND " + DataHelper.GEOFENCES_COLUMN_LONG + " = '" + latLng.getLongitude() + "'",
                        null);
                if (rows > 0) {
                    Log.d("TAG", "updateTransition: " + rows);
                } else {
                    Log.d("TAG", "updateTransition: error ");
                }

                String selectQuery = "SELECT *  FROM "
                        + DataHelper.GEOFENCES_TABLE_NAME + " WHERE " + DataHelper.GEOFENCES_COLUMN_LAT + " = '" + latLng.getLatitude()  + "' "
                        + " AND " + DataHelper.GEOFENCES_COLUMN_LONG + " = '" + latLng.getLongitude() + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    int transitionStatusIdx = cursor.getColumnIndex(DataHelper.GEOFENCE_TRANSITION_STATUS);
                    transitionStatus = cursor.getInt(transitionStatusIdx);
                } while (cursor.moveToNext());

            } catch (Exception e) {
                CommonUtils.logE(e.toString());
                cursor = null;
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            Log.d("TAG", "updateTransition --- status: " + transitionStatus);

        }

        public int getTransition(Double lat, Double lang) {
            openRead();
            Cursor cursor = null;
            int transitionStatus = -1;
            try {
                String selectQuery1 = "SELECT * FROM "
                        + DataHelper.GEOFENCES_TABLE_NAME;

                String selectQuery = "SELECT *  FROM "
                        + DataHelper.GEOFENCES_TABLE_NAME + " WHERE " + DataHelper.GEOFENCES_COLUMN_LAT + " = '" + lat + "' "
                        + " AND " + DataHelper.GEOFENCES_COLUMN_LONG + " = '" + lang + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    int transitionStatusIdx = cursor.getColumnIndex(DataHelper.GEOFENCE_TRANSITION_STATUS);
                    transitionStatus = cursor.getInt(transitionStatusIdx);
                } while (cursor.moveToNext());

            } catch (Exception e) {
                CommonUtils.logE(e.toString());
                cursor = null;
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            Log.d("TAG", "updateTransition --- status: " + transitionStatus);
            return transitionStatus;
        }

        public HashMap<LatLng, List<GeoFenceModel>> getAllNodesData() {
            HashMap<LatLng, List<GeoFenceModel>> map = new HashMap<>();
            openRead();
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT * FROM "
                        + DataHelper.GEOFENCES_TABLE_NAME;

                cursor = database.rawQuery(selectQuery, null);

                if (cursor.moveToFirst()) {
                    int nodeIdIdx = cursor.getColumnIndex(DataHelper.GEOFENCES_COLUMN_NODE_ID);
                    int dktNoIdx = cursor.getColumnIndex(DataHelper.GEOFENCES_COLUMN_DKT_NO);
                    int dktNodeIdIdx = cursor.getColumnIndex(DataHelper.GEOFENCES_COLUMN_DKT_NODE_ID);
                    int subProjectIdIdx = cursor.getColumnIndex(DataHelper.GEOFENCES_COLUMN_SUB_PROJECT_ID);
                    int isEndNodeIdx = cursor.getColumnIndex(DataHelper.GEOFENCES_COLUMN_ISENDNODE);
                    int isStartNodeIdx = cursor.getColumnIndex(DataHelper.GEOFENCES_COLUMN_ISSTARTNODE);
                    int latitudeIdx = cursor.getColumnIndex(DataHelper.GEOFENCES_COLUMN_LAT);
                    int longitudeIdx = cursor.getColumnIndex(DataHelper.GEOFENCES_COLUMN_LONG);
                    int transitionStatusIdx = cursor.getColumnIndex(DataHelper.GEOFENCE_TRANSITION_STATUS);

                    do {
                        GeoFenceModel model = new GeoFenceModel();
                        model.setNodeId(cursor.getInt(nodeIdIdx));
                        model.setDktNo(cursor.getInt(dktNoIdx));
                        model.setDktNodeId(cursor.getInt(dktNodeIdIdx));
                        double latitude = cursor.getDouble(latitudeIdx);
                        double longitude = cursor.getDouble(longitudeIdx);
                        model.setLatitude(latitude);
                        model.setLongitude(longitude);
                        model.setIsStartNode(cursor.getString(isStartNodeIdx));
                        model.setIsEndNode(cursor.getString(isEndNodeIdx));
                        model.setSubProjectId(cursor.getInt(subProjectIdIdx));
                        int transitionStatus = cursor.getInt(transitionStatusIdx);
                        model.setIsEntered(transitionStatus != 0 ? 1 : 0);

                        LatLng latLng = new LatLng(latitude, longitude);
                        List<GeoFenceModel> existingList = map.containsKey(latLng) ? map.get(latLng) : new ArrayList<GeoFenceModel>();
                        existingList.add(model);

                        map.put(latLng, existingList);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                CommonUtils.logE(e.toString());
                cursor = null;
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return map;
        }

        public GeoFenceModel getValue(String id) {
            openRead();

            Cursor cursor = null;
            int dktNo, dktNodeId, subProjectId;
            GeoFenceModel model = null;
            String isStartNode = "";
            String isendNode = "";
            int nodeId;
            List<GeoFenceModel>list = new ArrayList<>();


            try {
                String selectQuery = "SELECT  "
                        + DataHelper.GEOFENCES_COLUMN_DKT_NO + ", "
                        + DataHelper.GEOFENCES_COLUMN_SUB_PROJECT_ID + ", "
                        + DataHelper.GEOFENCES_COLUMN_DKT_NODE_ID + ","
                        + DataHelper.GEOFENCES_COLUMN_ISSTARTNODE + ","
                        + DataHelper.GEOFENCES_COLUMN_ISENDNODE + ","
                        + DataHelper.GEOFENCES_COLUMN_NODE_ID
                        + " FROM "
                        + DataHelper.GEOFENCES_TABLE_NAME;
//                        + " WHERE "
//                        + DataHelper.GEOFENCES_COLUMN_DKT_NODE_ID_NODE_ID + " = '" + id + "'";

                cursor = database.rawQuery(selectQuery, null);
                CommonUtils.logD(selectQuery);

                if (cursor.moveToFirst()) {

                    dktNo = cursor.getInt(0);
                    subProjectId = cursor.getInt(1);
                    dktNodeId = cursor.getInt(2);
                    isStartNode = cursor.getString(3);
                    isendNode = cursor.getString(4);
                    nodeId = cursor.getInt(5);
                    String nodeIdDktNodeId = String.valueOf(nodeId)+String.valueOf(dktNodeId);
                    if(nodeIdDktNodeId.equalsIgnoreCase(id)){
                    model = new GeoFenceModel();
                    model.setDktNo(dktNo);
                    model.setDktNodeId(dktNodeId);
                    model.setStartNode(isStartNode);
                    model.setSubProjectId(subProjectId);
                    model.setIsEndNode(isendNode);
                    model.setNodeId(nodeId);
                    list.add(model);

                    CommonUtils.logD("&&"+model.toString());
                }
                }


            } catch (Exception e) {
                CommonUtils.logE(e.toString());
                cursor = null;
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return model;
        }
    }

    /******************GeoFence Arrival data **************************/

    public class GeoFenceArrivalData {
        public void truncatetable() {
            open();
            if (database != null) {
                String query = "DELETE FROM " + DataHelper.GEOFENCE_ARRIVAL_TABLE_NAME;
                database.execSQL(query);
            }
            close();
        }

        public void delete(int id) {
            open();
            if (database != null) {
                String query = "DELETE FROM " + DataHelper.GEOFENCE_ARRIVAL_TABLE_NAME + " WHERE "
                        + DataHelper.GEOFENCE_ARRIVAL_COLUMN_DKT_NODE_ID + " = " + id;
                database.execSQL(query);
                CommonUtils.logD(query);
            }
            close();
        }

        public void insertData(Context context, List<UpdateNodeModel> data) {
            int noOfRecords = 0;
            truncatetable();

            open();
            try {
                if (CommonUtils.isValidArraylist(data)) {
                    for (UpdateNodeModel model : data) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.GEOFENCE_ARRIVAL_COLUMN_NODE_ID, model.getNodeId());
                        values.put(DataHelper.GEOFENCE_ARRIVAL_COLUMN_DKT_NO, model.getDktNo());
                        values.put(DataHelper.GEOFENCE_ARRIVAL_COLUMN_DKT_NODE_ID, model.getDktNodeId());
                        values.put(DataHelper.GEOFENCE_ARRIVAL_COLUMN_ARRIVAL_TIME, model.getTimestamp());
                        values.put(DataHelper.GEOFENCE_ARRIVAL_COLUMN_IS_DEPARTURE, model.getIsDeparture());
                        values.put(DataHelper.GEOFENCE_ARRIVAL_COLUMN_SUB_PROJECT_ID, model.getSubprojectId());
                        values.put(DataHelper.GEOFENCE_ARRIVAL_COLUMN_LAT, (model.getLatitude()));
                        values.put(DataHelper.GEOFENCE_ARRIVAL_COLUMN_LONG, model.getLongitude());

                        CommonUtils.logD("Arrival time : " + model.getTimestamp());
                        if (database.insert(DataHelper.GEOFENCE_ARRIVAL_TABLE_NAME, null,
                                values) > 0) {
                            noOfRecords++;
                        } else {
                        }
                    }
                    CommonUtils.logD(noOfRecords + " Records inserted sucessful in arrival");

//                    Intent intent = new Intent(context, GeoFenceArrivalPushService.class);
//                    context.startService(intent);


                }

            } catch (Exception e) {
                CommonUtils.logE(e.toString());
            } finally {
                close();
            }
        }


        public List<UpdateNodeModel> getAll() {
            List<UpdateNodeModel> list = new ArrayList<>();
            openRead();

            Cursor cursor = null;

            try {
                String selectQuery = "SELECT * FROM "
                        + DataHelper.GEOFENCE_ARRIVAL_TABLE_NAME;

                cursor = database.rawQuery(selectQuery, null);

                if (cursor.moveToFirst()) {

//                    int id = cursor.getColumnIndex(DataHelper.GEOFENCE_ARRIVAL_COLUMN_ID);
                    int nodeId = cursor.getColumnIndex(DataHelper.GEOFENCE_ARRIVAL_COLUMN_NODE_ID);
                    int dktNo = cursor.getColumnIndex(DataHelper.GEOFENCE_ARRIVAL_COLUMN_DKT_NO);
                    int dktNodeId = cursor.getColumnIndex(DataHelper.GEOFENCE_ARRIVAL_COLUMN_DKT_NODE_ID);
                    int arrivalTime = cursor.getColumnIndex(DataHelper.GEOFENCE_ARRIVAL_COLUMN_ARRIVAL_TIME);
                    int subProjectId = cursor.getColumnIndex(DataHelper.GEOFENCE_ARRIVAL_COLUMN_SUB_PROJECT_ID);
                    int isDepart = cursor.getColumnIndex(DataHelper.GEOFENCE_ARRIVAL_COLUMN_IS_DEPARTURE);
                    int latitude = cursor.getColumnIndex(DataHelper.GEOFENCE_ARRIVAL_COLUMN_LAT);
                    int longitude = cursor.getColumnIndex(DataHelper.GEOFENCE_ARRIVAL_COLUMN_LONG);
                    do {
                        UpdateNodeModel model = new UpdateNodeModel();
//                        model.setId(cursor.getInt(id));
                        model.setNodeId(cursor.getInt(nodeId));
                        model.setDktNo(cursor.getInt(dktNo));
                        model.setDktNodeId(cursor.getInt(dktNodeId));
                        model.setTimestamp(cursor.getString(arrivalTime));
                        model.setSubprojectId(cursor.getInt(subProjectId));
                        model.setIsDeparture(cursor.getString(isDepart));
                        model.setLatitude(cursor.getDouble(latitude));
                        model.setLongitude(cursor.getDouble(longitude));
                        if (model != null) {
                            list.add(model);
                        }
                    } while (cursor.moveToNext());

                }

            } catch (Exception e) {
                CommonUtils.logE(e.toString());
                cursor = null;
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return list;

        }


    }


    public class ReasonCodes {

        public long saveReasonCodes(List<ReasonsModel> reasonsModels) {

            clearAll();

            open();

            long retVal = -1, id = -1;
            try {
                for (ReasonsModel reasonCodes1 : reasonsModels) {
//                    id = exists(reasonCodes1.getId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.REASON_CODES_ID, reasonCodes1.getId());
                        values.put(DataHelper.REASON_CODES_NAME, reasonCodes1.getValue());
                        values.put(DataHelper.REASON_CODES_TYPE, reasonCodes1.getType());

                        retVal = database.insert(DataHelper.REASON_CODES_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                CommonUtils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int reasonId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.REASON_CODES_KEY_ID
                        + " FROM " + DataHelper.REASON_CODES_TABLE_NAME
                        + " WHERE " + DataHelper.REASON_CODES_ID
                        + " = " + reasonId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                CommonUtils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<ReasonsModel> getExceptionTypes() {
            ArrayList<ReasonsModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.REASON_CODES_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int reasonIdIdx = cursor.getColumnIndex(DataHelper.REASON_CODES_ID);
                int reasonIdx = cursor.getColumnIndex(DataHelper.REASON_CODES_NAME);
                int typeIdx = cursor.getColumnIndex(DataHelper.REASON_CODES_TYPE);

                do {
                    ReasonsModel model = new ReasonsModel();
                    model.setId(cursor.getInt(reasonIdIdx));
                    model.setValue(cursor.getString(reasonIdx));
                    model.setType(cursor.getString(typeIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public String getExceptionId(String desc) {
            String depsId = "";

            openRead();

            String sql = "SELECT " + DataHelper.REASON_CODES_ID + " FROM " + DataHelper.REASON_CODES_TABLE_NAME
                    + " WHERE " + DataHelper.REASON_CODES_NAME + " = '" + desc + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                depsId = cursor.getString(0);
            }

            cursor.close();
            close();

            return depsId;
        }


        public ArrayList<String> getExceptionList() {
            ArrayList<String> codes = new ArrayList<String>();

            openRead();

            String sql = "SELECT * FROM " + DataHelper.REASON_CODES_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int descId = cursor
                        .getColumnIndex(DataHelper.REASON_CODES_NAME);
                do {
                    codes.add(cursor.getString(descId));
                } while (cursor.moveToNext());
            }

            cursor.close();
            close();

            return codes;
        }

        public ArrayList<String> getReasonsList(String type) {
            ArrayList<String> codes = new ArrayList<String>();

            openRead();

            String sql = "SELECT * FROM " + DataHelper.REASON_CODES_TABLE_NAME
                    + " WHERE " + DataHelper.REASON_CODES_TYPE + " = '" + type
                    + "'";

            CommonUtils.logD(sql);

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int descId = cursor
                        .getColumnIndex(DataHelper.REASON_CODES_NAME);
                do {
                    codes.add(cursor.getString(descId));
                } while (cursor.moveToNext());
            }

            cursor.close();
            close();

            for (String code : codes)
                CommonUtils.logD(code.toString());
            return codes;
        }


        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.REASON_CODES_TABLE_NAME;
            database.execSQL(query);

            CommonUtils.logD(query);
            close();
        }
    }


}
