package com.spoton.spl.lmapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.spoton.spl.lmapp.datacache.DataSource;
import com.spoton.spl.lmapp.models.RegistrationDataModel;
import com.spoton.spl.lmapp.models.RegistrationModel;
import com.spoton.spl.lmapp.responses.ResgistrationResponseModel;
import com.spoton.spl.lmapp.utils.CommonUtils;
import com.spoton.spl.lmapp.utils.Constants;
import com.spoton.spl.lmapp.utils.Globals;
import com.spoton.spl.lmapp.utils.HttpRequest;
import com.spoton.spl.lmapp.utils.LMDUtils;
import com.spoton.spl.lmapp.utils.LastMileDeliveryException;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

public class LoginScreen extends AppCompatActivity {
    Context context;
    RegistrationDataModel registrationDataModel;
    DataSource dataSource;
    private EditText mobileNoEt;
    String mobileNo;
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {

                case R.id.login_tv:

                     mobileNo = mobileNoEt.getText().toString();
                    if (CommonUtils.isValidString(mobileNo) && mobileNo.length() == 10) {

                        RegistrationModel model = new RegistrationModel();
                        if (CommonUtils.isValidString(dataSource.sharedPreferences.getValue(Constants.DEVICE_TOKEN)))
                            model.setPushToken(dataSource.sharedPreferences.getValue(Constants.DEVICE_TOKEN));

                        if (CommonUtils.isValidString(CommonUtils.getImei(context)))
                            model.setImei(CommonUtils.getImei(context));

                        model.setMobileNo(mobileNo);

                        try {
                            String versionName  = getPackageManager()
                                    .getPackageInfo(getPackageName(), 0).versionName;
                            if(CommonUtils.isValidString(versionName)){
                                model.setVersionName(versionName);
                            }
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }

                        new LoginUser().execute(model);

                    } else {
                        CommonUtils.showToastShort(context, "Please enter valid mobile number");
                    }


                    break;
            }
        }
    };
    private TextView loginTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);


        context = LoginScreen.this;

        dataSource = new DataSource(context);

        mobileNoEt = (EditText) findViewById(R.id.mobile_no_et);

        loginTv = (TextView) findViewById(R.id.login_tv);
        loginTv.setOnClickListener(onClickListener);


    }

    class LoginUser extends AsyncTask<RegistrationModel, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CommonUtils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(RegistrationModel... params) {

            String loginUrl = LMDUtils.getDeviceRegistration();

            RegistrationModel registrationModel = params[0];

            ObjectMapper mapper = new ObjectMapper();

            try {

                String data = mapper.writeValueAsString(registrationModel);
                CommonUtils.logD(registrationModel.toString());
                ResgistrationResponseModel response = (ResgistrationResponseModel) HttpRequest.postData(loginUrl, data, Constants.CONTENT_TYPE, HttpRequest.CONTENT_TYPE, ResgistrationResponseModel.class);

                if (response != null) {
                    CommonUtils.logD(response.toString());
                    if (response.getStatus()) {
//                        registrationDataModel = response.getData();
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }


                }
            } catch (RuntimeException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (LastMileDeliveryException e) {
                e.printStackTrace();
            }


            return null;

        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            CommonUtils.dismissProgressDialog();
            CommonUtils.showErrorDialog(context);
//            if (registrationDataModel != null) {
                dataSource.sharedPreferences.set(Constants.MOBILE_NUMBER,mobileNo);
                dataSource.sharedPreferences.set(Constants.IS_LOGIN, Constants.TRUE);
                Intent intent = new Intent(context, HomeScreen.class);
//                Intent intent = new Intent(context, TCDetailsScreen.class);
                startActivity(intent);
                finish();


//            } else
//                dataSource.sharedPreferences.set(Constants.IS_LOGIN, Constants.FALSE);
        }

    }

}
