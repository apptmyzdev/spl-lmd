package com.spoton.spl.lmapp;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.spoton.spl.lmapp.GeoFencing.GatherLocation;
import com.spoton.spl.lmapp.GeoFencing.GeoFenceModel;
import com.spoton.spl.lmapp.datacache.DataSource;
import com.spoton.spl.lmapp.models.LatLng;
import com.spoton.spl.lmapp.models.NodeModel;
import com.spoton.spl.lmapp.models.RegistrationDataModel;
import com.spoton.spl.lmapp.models.RegistrationModel;
import com.spoton.spl.lmapp.models.UpdateNodeModel;
import com.spoton.spl.lmapp.models.UserLocationModel;
import com.spoton.spl.lmapp.responses.GeneralResponseModel;
import com.spoton.spl.lmapp.responses.ResgistrationResponseModel;
import com.spoton.spl.lmapp.utils.CommonUtils;
import com.spoton.spl.lmapp.utils.Constants;
import com.spoton.spl.lmapp.utils.Globals;
import com.spoton.spl.lmapp.utils.HttpRequest;
import com.spoton.spl.lmapp.utils.LMDUtils;
import com.spoton.spl.lmapp.utils.LastMileDeliveryException;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class EmployeeLocationService extends Service implements GetTcData.TcDataListener {
    private static final String TAG = "LocationManagerService";
    static Integer minutes = 1;
    UserLocationModel userLocationModel;
    SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    Context context;
    LocationManager locationManager;
    // flag for Passive Provider status
    boolean isPassiveEnabled = false;
    // flag for GPS status
    boolean isGPSEnabled = false;
    // flag for network status
    boolean isNetworkEnabled = false;
    Location currentLocation;
    DataSource dataSource;
    private Integer locationOnFlag;
    private FusedLocationProviderClient fusedLocationClient = null;
    private GeneralResponseModel response = null;
//    private Location lastLocation = null;
//    private Location enteredLocation = null;


    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            CommonUtils.appendLog(DateFormat.getDateTimeInstance().format(new Date()) + " " + "onLocationChanged" + "lat: " + location.getLatitude() + "long " + location.getLongitude(), Constants.GEOFENCE_FILE);

//            currentLocation = location;
//            Toast.makeText(getBaseContext(),  "LOCATION MANAGER :: location changed ::: " + currentLocation.getLatitude() + ":" +
//                    currentLocation.getLongitude(),Toast.LENGTH_SHORT).show();
            DecimalFormat df = new DecimalFormat("##.#########");
            df.setRoundingMode(RoundingMode.CEILING);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {
            locationOnFlag = 1;
            Log.e("location", "turned on");
            Log.e("locationFlagOn", String.valueOf(locationOnFlag));


        }

        @Override
        public void onProviderDisabled(String provider) {
            locationOnFlag = 0;

            Location location = null;
            setData(location);
            Log.e("location", "turned off");
            Log.e("locationFlagOff", String.valueOf(locationOnFlag));

        }


    };

    private void checkGeofenceTransition(Location location) {
        if(location == null) {
            return;
        }

        CommonUtils.logD("Calculating the distance method");
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        String format = s.format(calendar.getTime());
        CommonUtils.appendLog("Current Location at:: " + format + "Node:: Current Entered Location:: " +
                location.getLatitude() + " :: " + location.getLongitude(), Constants.GEOFENCE_FILE);
        HashMap<LatLng, List<GeoFenceModel>> locationMap = dataSource.geoFences.getAllNodesData();
        for (LatLng latLng : locationMap.keySet()) {
            Location pointLocation = new Location("");
            pointLocation.setLatitude(latLng.getLatitude());
            pointLocation.setLongitude(latLng.getLongitude());
            if (location.distanceTo(pointLocation) <= Constants.GEOFENCE_RADIUS) {
//                CommonUtils.appendLog("Difference is less than 500", Constants.GEOFENCE_FILE);
                List<GeoFenceModel> list = locationMap.get(latLng);

                for (GeoFenceModel model : list) {
                    if (model.getIsEntered() == 0) {
//                        dataSource.geoFences.updateTransition(latLng, 1); //entered
                        callArrivalDepatureApi(list, location, latLng, 1, "A");  //commenting geofence for prod release
                        CommonUtils.appendLog("Entered geofence at:: " + format + " Node:: " + model.getNodeId() + " lat: "
                                + latLng.getLatitude() + " long: " + latLng.getLongitude() + " Current Entered Location:: " +
                                location.getLatitude() + " :: " + location.getLongitude(), Constants.GEOFENCE_FILE);
                    }
                }

            } else {
                List<GeoFenceModel> list = locationMap.get(latLng);
                if (location.distanceTo(pointLocation) >= Constants.GEOFENCE_RADIUS) {
//                    CommonUtils.appendLog("Difference is greater than 500", Constants.GEOFENCE_FILE);
                    for (GeoFenceModel model : list) {
                        if (model.getIsEntered() == 1) {

                            CommonUtils.appendLog("Exited geofence at::" + format + " Node:: " + model.getNodeId() + " lat: "
                                    + latLng.getLatitude() + " long: " + latLng.getLongitude() + " Current Entered Location:: " +
                                    location.getLatitude() + " :: " + location.getLongitude(), Constants.GEOFENCE_FILE);
//                            dataSource.geoFences.updateTransition(latLng, 2); //exited
                            callArrivalDepatureApi(list, location, latLng, 0,"D");  //commenting geofence for prod release
                        }
                    }
                }
            }
        }
    }

    private void callArrivalDepatureApi(List<GeoFenceModel> list, Location location, LatLng latLng, int state, String arrivalDepatureStr) {
        List<UpdateNodeModel> inputList = new ArrayList<>();
        for (GeoFenceModel model : list) {
            UpdateNodeModel node = new UpdateNodeModel();
            node.setNodeId(model.getNodeId());
            node.setDktNo(model.getDktNo());
            node.setDktNodeId(model.getDktNodeId());
            node.setTimestamp(CommonUtils.getTime(context));
            node.setSubprojectId(model.getSubProjectId());
            node.setLatitude(location.getLatitude());
            node.setLongitude(location.getLongitude());
            inputList.add(node);
        }

        try {
            ObjectMapper mapper = new ObjectMapper();
            String data = mapper.writeValueAsString(inputList);
            String url = LMDUtils.getCaptureGeofenceUrl(arrivalDepatureStr);
            CommonUtils.logD("url from emp loc " + url);
            CommonUtils.appendLog("Posting data from emp loc to " + url + "data" + data, Constants.GEOFENCE_FILE);
            ResgistrationResponseModel response = (ResgistrationResponseModel) HttpRequest.postData(url,
                    data, Constants.CONTENT_TYPE, HttpRequest.CONTENT_TYPE, ResgistrationResponseModel.class);
            if (response != null) {
                CommonUtils.logD(response.toString());
                if (response.getStatus()) {
                    if(response.getData() != null && response.getData().size() > 0) {
                        RegistrationDataModel registrationDataModel = response.getData().get(0);

                        for(RegistrationDataModel model: Globals.tcList) {
                            if(model.getDktNo().equals(registrationDataModel.getDktNo())) {
                                model.setNodeList(registrationDataModel.getNodeList());
                                model.setStartNode(registrationDataModel.getStartNode());
                                model.setEndNode(registrationDataModel.getEndNode());
                                dataSource.geoFences.updateTransition(latLng, state);
                                updateTcDataResult();
                            }
                        }

                    }
                } else {
                    Globals.lastErrMsg = response.getMessage();
                }
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LastMileDeliveryException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//
        try {
            if (intent.getExtras() != null) {
//                minutes = intent.getIntExtra("minutes", 0);
//                Log.e("mins", String.valueOf(minutes));
            }
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                if(fusedLocationClient != null) {
                    fusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                currentLocation = task.getResult();
                                new CheckTransitionTask(currentLocation).execute();
                            } else {
                                Log.w(TAG, "Failed to get location.");
                            }
                        }
                    });
                } else {
                    fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
                    fusedLocationClient.getLastLocation()
                            .addOnCompleteListener(new OnCompleteListener<Location>() {
                                @Override
                                public void onComplete(@NonNull Task<Location> task) {
                                    if (task.isSuccessful() && task.getResult() != null) {
                                        currentLocation = task.getResult();
                                        new CheckTransitionTask(currentLocation).execute();
                                    } else {
                                        Log.w(TAG, "Failed to get location.");
                                    }
                                }
                            });
                    LocationRequest locationRequest = LocationRequest.create();
                    locationRequest.setInterval(10000);
                    locationRequest.setFastestInterval(5000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    fusedLocationClient.requestLocationUpdates(locationRequest,
                            locationCallback,
                            Looper.getMainLooper());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  START_STICKY;
    }

    LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (locationResult == null) {
                return;
            }

            currentLocation = locationResult.getLastLocation();
            setData(currentLocation);
//            new CheckTransitionTask(currentLocation).execute();
//            Toast.makeText(getBaseContext(), "FUSED LOCATION MGR:location changed ::: " + currentLocation.getLatitude() + ":" +
//                    currentLocation.getLongitude(), Toast.LENGTH_SHORT).show();

        }
    };


    @Override
    public void onCreate() {
        super.onCreate();
        context = EmployeeLocationService.this;
        dataSource = new DataSource(context);
        if (locationManager == null) {
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        }

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        try {
            //minutes = Integer.valueOf(dataSource.sharedPreferences.getValue(Constants.LOCATION_INTERVAL));


            // getting Passive Provider's status
            isPassiveEnabled = locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);

            // getting GPS status
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


            if (!isGPSEnabled && !isNetworkEnabled && !isPassiveEnabled) {
//                Utils.writeToLocationsLog(context, DateFormat.getDateTimeInstance().format(new Date()) + " No Location Provider Available");
//                Utils.appendLog(DateFormat.getDateTimeInstance().format(new Date()) + " No Location Provider Available", Constants.LOCATION_FILE);
            } else {
                if (isNetworkEnabled) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }

                    // First get location from Network Provider
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER, minutes * 10000, 0, locationListener);
                }
                // get location from Passive Provider
                if (isPassiveEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.PASSIVE_PROVIDER, minutes * 10000, 0, locationListener);
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER, minutes * 10000, 0, locationListener);
                }
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
                fusedLocationClient.getLastLocation()
                        .addOnCompleteListener(new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                if (task.isSuccessful() && task.getResult() != null) {
                                    currentLocation = task.getResult();
                                    new CheckTransitionTask(currentLocation).execute();
                                } else {
                                    Log.w(TAG, "Failed to get location.");
                                }
                            }
                        });
                LocationRequest locationRequest = LocationRequest.create();
                locationRequest.setInterval(10000);
                locationRequest.setFastestInterval(5000);
//                locationRequest.setSmallestDisplacement(0);
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                fusedLocationClient.requestLocationUpdates(locationRequest,
                        locationCallback,
                        Looper.getMainLooper());

            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            startMyOwnForeground();
        else
            startServiceinForeground();
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                if(currentLocation != null) {
                    CommonUtils.logD("timer"+currentLocation.getLatitude()+"  "+ currentLocation.getLongitude());
                    setData(currentLocation);
                }
            }
        };
        timer.schedule(timerTask, 0, minutes * 30000);    //minutes 15
    }


    public void setData(Location location) {
        checkLocationTask(location);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public Integer locationStatus() {
        Integer locationOnFlag = null;
        if (locationManager != null) {
            // getting GPS status
            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (isGPSEnabled) {
                locationOnFlag = 1;
            } else {
                locationOnFlag = 0;
            }
        }
        return locationOnFlag;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartServiceTask = new Intent(getApplicationContext(), this.getClass());
        restartServiceTask.setPackage(getPackageName());
        PendingIntent restartPendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceTask, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager myAlarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        myAlarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartPendingIntent);

        super.onTaskRemoved(rootIntent);
    }

    private void startServiceinForeground() {
        Intent notificationIntent = new Intent(this, EmployeeLocationService.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText("Location Capture")
                .setContentIntent(pendingIntent).build();

        int m = (int) (System.currentTimeMillis() % 10000);
        startForeground(m, notification);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        super.onDestroy();
        context.startService(new Intent(context, EmployeeLocationService.class));
        Intent intent = new Intent(context, SplashScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startMyOwnForeground() {
        String NOTIFICATION_CHANNEL_ID = "com.spoton.spl.lmapp";
        String channelName = "My Background Service";
        NotificationChannel chan = null;
        chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("App is running in background")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(2, notification);
    }

//    public void refreshData() {
//        RegistrationModel model = setRegistrationModel();
//        new GetTcData(this).execute(model);
//    }

    private RegistrationModel setRegistrationModel() {

        RegistrationModel model = new RegistrationModel();
        if (CommonUtils.isValidString(dataSource.sharedPreferences.getValue(Constants.DEVICE_TOKEN)))
            model.setPushToken(dataSource.sharedPreferences.getValue(Constants.DEVICE_TOKEN));

        if (CommonUtils.isValidString(CommonUtils.getImei(context)))
            model.setImei(CommonUtils.getImei(context));

        if (CommonUtils.isValidString(dataSource.sharedPreferences.getValue(Constants.MOBILE_NUMBER)))
            model.setMobileNo(dataSource.sharedPreferences.getValue(Constants.MOBILE_NUMBER));
        return model;
    }

    class CheckTransitionTask extends AsyncTask {
        Location location;

        CheckTransitionTask(Location location) {
            this.location = location;
        }

        @Override
        protected Object doInBackground(Object[] objects) {
//            if(currentLocation.distanceTo(location) > 10) {
            checkGeofenceTransition(location);
//            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
        }
    }

    public class PostEmployeeLocationUpdates extends AsyncTask<UserLocationModel, Void, Void> {

        GeneralResponseModel generalResponseModel;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(UserLocationModel... params) {
            String url = null;
            UserLocationModel model = params[0];
            CommonUtils.logD(model.toString());

            model.setBattery(String.valueOf(CommonUtils.getBatteryPercentage(context)));
            url = LMDUtils.getLocationUrlTag();

            ObjectMapper objectMapper = new ObjectMapper();
            if (CommonUtils.isValidString(url)) {
                try {


                    String data = objectMapper.writeValueAsString(model);
                    generalResponseModel = (GeneralResponseModel) HttpRequest.postData(url, data, Constants.CONTENT_TYPE, HttpRequest.CONTENT_TYPE, GeneralResponseModel.class);
                    CommonUtils.appendLog(DateFormat.getDateTimeInstance().format(new Date())
                            + "posting data from emp loc " + url + " data " + data, Constants.LOCATIONS_FILE);
                    if (generalResponseModel != null) {
                        if (generalResponseModel.getStatus()) {
                            Log.e("employeelocationmodel", generalResponseModel.toString());
                        } else {
                            Globals.lastErrMsg = generalResponseModel.getMessage();
                        }
                    }
                } catch (LastMileDeliveryException e) {
                    e.printStackTrace();
//                    for (EmployeeLocationDataModel data : employeeLocationDataModelList) {
//                        try {
//                            dataSource.employeeLocationdata.insertEmployeeLocationData(context, data);
//                        } catch (SQLException ex) {
//                            ex.printStackTrace();
//                        }
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // employeeLocationProgress.dismiss();

            if (generalResponseModel != null && generalResponseModel.getStatus()) {
//               refreshData();
            }


            GatherLocation gatherlocation = new GatherLocation();

            Location location = gatherlocation.getLocation(context);
            if (location != null) {
                double lat = location.getLatitude();
                double lng = location.getLongitude();
                CommonUtils.logD("Lat : " + lat + " , Lng : " + lng);
                if(location != null/* && location.distanceTo(currentLocation) > 5*/) {//user moved atleast 5meteres from previous position
                    currentLocation = location;
                    new CheckTransitionTask(currentLocation).execute();
                }

            }

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                if(fusedLocationClient != null) {
                    fusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                Location location = task.getResult();
                                if(location != null/* && location.distanceTo(currentLocation) > 5*/) {//user moved atleast 5meteres from previous position
                                    currentLocation = location;
                                    new CheckTransitionTask(currentLocation).execute();
                                }
                            } else {
                                Log.w(TAG, "Failed to get location.");
                            }
                        }
                    });
                }
            }
        }
    }

    private void checkLocationTask(Location location) {

        userLocationModel = new UserLocationModel();
        GatherLocation gatherlocation = new GatherLocation();
        Location newlocation = gatherlocation.getLocation(context);
        if (newlocation != null) {
            double lat = newlocation.getLatitude();
            double lng = newlocation.getLongitude();
//                Toast.makeText(getBaseContext(),  "LOCATION MANAGER :: location changed ::: " + newlocation.getLatitude() + ":" +
//                        newlocation.getLongitude(),Toast.LENGTH_LONG).show();
            CommonUtils.logD("Latitude : " + lat + " , Lng : " + lng);
            CommonUtils.appendLog(DateFormat.getDateTimeInstance().format(new Date())+"Latitude : " + lat + " , Lng : " + lng,Constants.LOCATIONS_PATH_FILE);
            userLocationModel.setLatitude(lat);
            userLocationModel.setLongitude(lng);
        } else {
            if (location != null) {
                userLocationModel.setLatitude(location.getLatitude());
                userLocationModel.setLongitude(location.getLongitude    ());
                Log.e("latinservice", String.valueOf(location.getLatitude()));
                Log.e("lnginservice", String.valueOf(location.getLongitude()));
            }
        }


        CommonUtils.logD(userLocationModel.toString());
        try {
            if (CommonUtils.isValidString(dataSource.sharedPreferences.getValue(Constants.MOBILE_NUMBER))) {
                userLocationModel.setMobileNo(dataSource.sharedPreferences.getValue(Constants.MOBILE_NUMBER));
            }
            Calendar calendar = Calendar.getInstance();
            String format = s.format(calendar.getTime());
            if (CommonUtils.isValidString(format)) {
                userLocationModel.setDeviceTimestamp(format);
            }
            if (CommonUtils.isValidString(CommonUtils.getImei(context))) {
                userLocationModel.setImei(CommonUtils.getImei(context));
            }
            if (CommonUtils.isValidString(dataSource.sharedPreferences.getValue(Constants.TC_NUMBER))) {
                userLocationModel.setDktNo(dataSource.sharedPreferences.getValue(Constants.TC_NUMBER));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        new PostEmployeeLocationUpdates().execute(userLocationModel);
    }

    public class LocalBinder extends Binder {
        EmployeeLocationService getService() {
            // Return this instance of LocalService so clients can call public methods
            return EmployeeLocationService.this;
        }
    }

    @Override
    public void updateTcDataResult() {
        if (Globals.registrationDataModel != null) {
            if (CommonUtils.isValidArraylist(Globals.tcList)) {
                for (RegistrationDataModel model : Globals.tcList) {
                    if (model.getEndNode().getDlyStatus() == 3) {
                       updateNodeAndCloseTC(model);
                    }
                    if (Globals.registrationDataModel.getDktNo().equalsIgnoreCase(model.getDktNo())) {
                        Globals.registrationDataModel = model;

                        Intent in = new Intent("com.spoton.spl.lmapp.tcdata");
                        Bundle extras = new Bundle();
                        extras.putBoolean("registrationDataModelUpdated", true);
                        in.putExtras(extras);
                        EmployeeLocationService.this.getApplicationContext().sendBroadcast(in);
                    }
                }
            }
        }
    }

    private void updateNodeAndCloseTC(RegistrationDataModel nodeModel) {

        UpdateNodeModel model = new UpdateNodeModel();
        if (nodeModel != null && nodeModel.getEndNode() != null) {
            NodeModel endModel = nodeModel.getEndNode();

            if (currentLocation != null) {
                CommonUtils.logD("Lat : " + currentLocation.getLatitude() + " , Lng : " +
                        currentLocation.getLongitude());
                model.setLatitude(currentLocation.getLatitude());
                model.setLongitude(currentLocation.getLongitude());
            }
            model.setDktNo(Integer.parseInt(Globals.registrationDataModel.getDktNo()));
            model.setSubprojectId((Globals.registrationDataModel.getSubprojectId()));
            model.setDktNodeId(endModel.getDktNodeId());
            model.setNodeId(endModel.getNodeId());
            SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Calendar calendar = Calendar.getInstance();
            String format = s.format(calendar.getTime());
            if (CommonUtils.isValidString(format)) {
                model.setTimestamp(format);
            }
            Globals.action = Constants.END;
            new EndNodeUpdateTask().execute(model);
        }
    }

    class EndNodeUpdateTask extends AsyncTask<UpdateNodeModel, Void, Void> {
        UpdateNodeModel updateNodeModel;
        @Override
        protected Void doInBackground(UpdateNodeModel... params) {
            String geofence = "0";

            String loginUrl = LMDUtils.getEndTripUrlTag(geofence);//1- api called from geofencing 0 api called when user clicks arrived
            updateNodeModel = params[0];
            ObjectMapper mapper = new ObjectMapper();

            try {
                String data = mapper.writeValueAsString(updateNodeModel);
                CommonUtils.logD(updateNodeModel.toString());
                response = (GeneralResponseModel) HttpRequest.postData(loginUrl, data, Constants.CONTENT_TYPE, HttpRequest.CONTENT_TYPE, GeneralResponseModel.class);

                if (response != null) {
                    CommonUtils.logD(response.toString());
                    if (response.getStatus()) {
                        Globals.lastErrMsg = response.getMessage();

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                } else {
                    Globals.lastErrMsg = "Response is null";
                }
            } catch (Exception e) {
                if (!CommonUtils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (response != null && response.getStatus()) {
                dataSource.geoFences.truncatetable();
                RegistrationModel model = setRegistrationModel();
                new GetTcData(EmployeeLocationService.this).execute(model);
            }
            if(Globals.registrationDataModel != null && updateNodeModel != null) {
                if (Integer.parseInt(Globals.registrationDataModel.getDktNo()) == updateNodeModel.getDktNo()) {
                    Globals.registrationDataModel = null;
                }
            }
        }
    }
}