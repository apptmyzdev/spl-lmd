package com.spoton.spl.lmapp;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.IBinder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.spl.lmapp.GeoFencing.GeoFenceModel;
import com.spoton.spl.lmapp.datacache.DataSource;
import com.spoton.spl.lmapp.models.LatLng;
import com.spoton.spl.lmapp.models.UpdateNodeModel;
import com.spoton.spl.lmapp.responses.GeneralResponseModel;
import com.spoton.spl.lmapp.responses.UpdateNodeResponse;
import com.spoton.spl.lmapp.utils.CommonUtils;
import com.spoton.spl.lmapp.utils.Constants;
import com.spoton.spl.lmapp.utils.Globals;
import com.spoton.spl.lmapp.utils.HttpRequest;
import com.spoton.spl.lmapp.utils.LMDUtils;
import com.spoton.spl.lmapp.utils.LastMileDeliveryException;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class GeoFenceArrivalPushService extends Service {
    public String userName;
    public String imei;
    public String scCode;
    public String route;
    public String routeType;

    public boolean isVeh;

    public DataSource dataSource;

    private Gson gson = new GsonBuilder().disableHtmlEscaping().create(); //new Gson();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        if (intent != null) {
        dataSource = new DataSource(getApplicationContext());

        CommonUtils.logD("in start command");
        List<UpdateNodeModel> list = dataSource.geoFenceArrivalData.getAll();

        if (CommonUtils.isValidArraylist(list)) {
            for (UpdateNodeModel model : list) {
                new NodeUpdateTask().execute(model);
            }
        }
        return Service.START_STICKY;
    }

    class NodeUpdateTask extends AsyncTask<UpdateNodeModel, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(UpdateNodeModel... params) {
            String geofence = "1"; //1- api called from geofencing 0 api called when user clicks arrived
            String url = "";
            UpdateNodeModel updateNodeModel = params[0];
            String isDepart = "";
            String isEndNode = "";
            isDepart = updateNodeModel.getIsDeparture();
            isEndNode = updateNodeModel.getIsEndNode();
            if (CommonUtils.isValidString(isEndNode) && isEndNode.equalsIgnoreCase(Constants.TRUE)) {
                url = LMDUtils.getEndTripUrlTag(geofence);
            } else {
                if (CommonUtils.isValidString(isDepart) && isDepart.equalsIgnoreCase(Constants.TRUE))
                    url = LMDUtils.getDepartureUrlTag(geofence);
                else
                    url = LMDUtils.getArrivalUrlTag(geofence);
            }
            ObjectMapper mapper = new ObjectMapper();

            try {
                String data = mapper.writeValueAsString(updateNodeModel);
                if (isDepart.equalsIgnoreCase(Constants.TRUE))
                    CommonUtils.logD("Arrival : " + updateNodeModel.toString());
                else
                    CommonUtils.logD("Departure : " + updateNodeModel.toString());

                UpdateNodeResponse response = (UpdateNodeResponse) HttpRequest.postData(url, data, Constants.CONTENT_TYPE, HttpRequest.CONTENT_TYPE, UpdateNodeResponse.class);

                if (response != null) {
                    CommonUtils.logD(response.toString());
                    if (response.getStatus()) {
                        dataSource.geoFenceArrivalData.delete(updateNodeModel.getDktNodeId());
                        HashMap<LatLng, List<GeoFenceModel>> locationMap = dataSource.geoFences.getAllNodesData();
                        for (LatLng latLng : locationMap.keySet()) {
                            if (locationMap.get(latLng).get(0).getDktNo() == updateNodeModel.getDktNo() &&
                                    locationMap.get(latLng).get(0).getNodeId() == updateNodeModel.getNodeId()) {
                               if (isDepart.equalsIgnoreCase(Constants.FALSE)) {
                                    dataSource.geoFences.updateTransition(latLng, 1);
                                } else if (isDepart.equalsIgnoreCase(Constants.TRUE)) {
                                    dataSource.geoFences.updateTransition(latLng, 0);
                                }
                            }
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (RuntimeException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (LastMileDeliveryException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            CommonUtils.dismissProgressDialog();
        }
    }
}
