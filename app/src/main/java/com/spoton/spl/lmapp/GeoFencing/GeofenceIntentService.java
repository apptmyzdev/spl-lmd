package com.spoton.spl.lmapp.GeoFencing;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.spoton.spl.lmapp.GetTcData;
import com.spoton.spl.lmapp.R;
import com.spoton.spl.lmapp.datacache.DataSource;
import com.spoton.spl.lmapp.models.LatLng;
import com.spoton.spl.lmapp.models.RegistrationDataModel;
import com.spoton.spl.lmapp.models.UpdateNodeModel;
import com.spoton.spl.lmapp.responses.ResgistrationResponseModel;
import com.spoton.spl.lmapp.utils.CommonUtils;
import com.spoton.spl.lmapp.utils.Constants;
import com.spoton.spl.lmapp.utils.Globals;
import com.spoton.spl.lmapp.utils.HttpRequest;
import com.spoton.spl.lmapp.utils.LMDUtils;
import com.spoton.spl.lmapp.utils.LastMileDeliveryException;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class GeofenceIntentService extends IntentService implements OnCompleteListener<Void>, GetTcData.TcDataListener {

    private final String TAG = Constants.LOG_TAG;
    //    private final String TAG = this.getClass().getCanonicalName();
    GatherLocation gatherlocation = new GatherLocation();
    List<String> requestIds = new ArrayList<>();
    GeofencingClient mGeofencingClient;
    DataSource dataSource;
    int userId;
    String userName;

    public GeofenceIntentService() {
        super("GeofenceIntentService");
        Log.v(TAG, "Constructor.");
    }

    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "onCreate");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "onDestroy");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mGeofencingClient = LocationServices.getGeofencingClient(this);

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        Log.v(TAG, "onHandleIntent");
        if (!geofencingEvent.hasError()) {
            int transition = geofencingEvent.getGeofenceTransition();
            List<Geofence> geofences = geofencingEvent.getTriggeringGeofences();
            Location loc = geofencingEvent.getTriggeringLocation();
//            Log.d("LocationTriggered", loc.toString());
            String notificationTitle;
            int deviceId = 0;
            dataSource = new DataSource(getApplicationContext());
//            if (dataSource.sharedPreferences.getValue(Constants.DEVICE_ID_PREF).length() != 0) {
////                deviceId = Integer.parseInt(dataSource.sharedPreferences.getValue(Constants.DEVICE_ID_PREF));
////            }  //gayatri
            String userIdStr = null, userNameStr = null;
//            userIdStr = dataSource.sharedPreferences.getValue(Constants.USER_ID_PREF);  //gayatri
//            if (userIdStr.length() != 0) {
//                userId = Integer.parseInt(userIdStr);
//            }

//            userNameStr = dataSource.sharedPreferences.getValue(Constants.USERNAME_PREF);  //gayatri
//            userName = userNameStr;
//            appendLog("Username: " + userName, Constants.GEOFENCE_FILE);

            switch (transition) {
                case Geofence.GEOFENCE_TRANSITION_ENTER:
                    notificationTitle = "Geofence Entered";
                    Log.v(TAG, "Geofence Entered " + geofences.toString());
                    List<GeoFenceUpdateModel> array = new ArrayList<>();
                    GeoFenceUpdateModel obj = new GeoFenceUpdateModel();
                    List<UpdateNodeModel> updateNodeModelList = new ArrayList<>();
                    for (Geofence gf : geofences) {
                        GeoFenceModel model = dataSource.geoFences.getValue(gf.getRequestId());
                        if(model!=null){
                        model.setLatitude(geofencingEvent.getTriggeringLocation().getLatitude());
                        model.setNodeId(Integer.parseInt(gf.getRequestId()));
                        model.setLongitude(geofencingEvent.getTriggeringLocation().getLongitude());
                        List<GeoFenceModel> geoFenceList = new ArrayList<>();
                        geoFenceList.add(model);

//                        callArrivalDepatureApi(geoFenceList,"A");  //commenting geofence for prod release
                        CommonUtils.appendLog(" Arrived "+model.getDktNo() + " node id "+ model.getNodeId()+"lat:"+geofencingEvent.getTriggeringLocation()
                                .getLatitude()+"long"+geofencingEvent.getTriggeringLocation().getLongitude(),Constants.GEOFENCE_FILE);
//                            if (!model.getStartNode().equalsIgnoreCase(Constants.TRUE)|| (model.getIsEndNode().equalsIgnoreCase(Constants.TRUE))) {
                                UpdateNodeModel updateNodeModel = new UpdateNodeModel();
                                CommonUtils.logD("data from geo fence" + model.toString());
                                updateNodeModel.setNodeId(Integer.parseInt(gf.getRequestId()));
                                updateNodeModel.setDktNodeId(model.getDktNodeId());
                                updateNodeModel.setDktNo((model.getDktNo()));
                                updateNodeModel.setSubprojectId((model.getSubProjectId()));
                                SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                                Calendar calendar = Calendar.getInstance();
                                String format = s.format(calendar.getTime());
                                updateNodeModel.setTimestamp(format);
                                updateNodeModel.setLatitude(geofencingEvent.getTriggeringLocation().getLatitude());
                                updateNodeModel.setLongitude(geofencingEvent.getTriggeringLocation().getLongitude());
                                updateNodeModel.setIsDeparture(Constants.FALSE);
                                updateNodeModel.setIsEndNode(model.getIsEndNode());
                                CommonUtils.logD("geo fence arrival "+updateNodeModel.toString());

                                updateNodeModelList.add(updateNodeModel);
//                            } else {
////                            Globals.isInGeoFence = true;
////                            CommonUtils.logD("test0"+Globals.isInGeoFence);
//
//                            }

                        }

                    }

                    dataSource.geoFenceArrivalData.insertData(GeofenceIntentService.this, updateNodeModelList);

//                    context.startService(new Intent(getApplicationContext(), GeofenceIntentService.class));

                    break;
                case Geofence.GEOFENCE_TRANSITION_DWELL:
                    notificationTitle = "Geofence Dwell";
                    Log.v(TAG, "Dwelling in Geofence");
                        CommonUtils.appendLog("Dwelling "+"lat:"+geofencingEvent.getTriggeringLocation().
                                getLatitude()+"long"+geofencingEvent.getTriggeringLocation().getLongitude(),Constants.GEOFENCE_FILE);
//                    for (Geofence gf : geofences) {
//
//                        GeoFenceModel model = dataSource.geoFences.getValue(Integer.parseInt(gf.getRequestId()));
//                        if (model.getStartNode().equalsIgnoreCase(Constants.TRUE)) {
//                            Globals.isInGeoFence = true;
//                            CommonUtils.logD("test1"+Globals.isInGeoFence);
//
//                        }
//                    }
                    break;

                case Geofence.GEOFENCE_TRANSITION_EXIT:
                    Log.v(TAG, "Geofence Exited");
                    GeoFenceUpdateModel obj1 = new GeoFenceUpdateModel();
                    List<GeoFenceUpdateModel> array1 = new ArrayList<>();
//                    for (Geofence gf : geofences) {
//                        GeoFenceModel model = dataSource.geoFences.getValue(Integer.parseInt(gf.getRequestId()));
//
//                        if (model.getStartNode().equalsIgnoreCase(Constants.TRUE)) {
//                            Globals.isInGeoFence = false;
//                            CommonUtils.logD("test2"+Globals.isInGeoFence);
//
//                        }
//                    }


                    List<UpdateNodeModel> updateNodeModelList1 = new ArrayList<>();
                    for (Geofence gf : geofences) {
                        GeoFenceModel model = dataSource.geoFences.getValue(gf.getRequestId());
                        if (model != null) {
                        model.setLatitude(geofencingEvent.getTriggeringLocation().getLatitude());
                        model.setLongitude(geofencingEvent.getTriggeringLocation().getLongitude());
                        model.setNodeId(Integer.parseInt(gf.getRequestId()));

                        List<GeoFenceModel> geoFenceList = new ArrayList<>();

                        geoFenceList.add(model);

//                        callArrivalDepatureApi(geoFenceList,"D"); //commenting geofence for prod release
                        CommonUtils.appendLog(" Departed "+model.getDktNo()+"node id"+model.getNodeId()+"lat:"+geofencingEvent
                                .getTriggeringLocation().getLatitude()+"long"+geofencingEvent.getTriggeringLocation().getLongitude(),Constants.GEOFENCE_FILE);


//                            if (!model.getStartNode().equalsIgnoreCase(Constants.TRUE) || (model.getIsEndNode().equalsIgnoreCase(Constants.TRUE))) {
                                UpdateNodeModel updateNodeModel = new UpdateNodeModel();
                                CommonUtils.logD("data from geo fence" + model.toString());
                                updateNodeModel.setNodeId(Integer.parseInt(gf.getRequestId()));
                                updateNodeModel.setDktNodeId(model.getDktNodeId());
                                updateNodeModel.setDktNo((model.getDktNo()));
                                updateNodeModel.setSubprojectId((model.getSubProjectId()));
                                updateNodeModel.setLatitude(geofencingEvent.getTriggeringLocation().getLatitude());
                                updateNodeModel.setLongitude(geofencingEvent.getTriggeringLocation().getLongitude());
                                SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                                Calendar calendar = Calendar.getInstance();
                                String format = s.format(calendar.getTime());
                                updateNodeModel.setTimestamp(format);
                                updateNodeModel.setIsDeparture(Constants.TRUE);
                                updateNodeModel.setIsEndNode(model.getIsEndNode());

                                CommonUtils.logD("geo fence exit "+updateNodeModel.toString());

                                updateNodeModelList1.add(updateNodeModel);
//                            } else {
////                            Globals.isInGeoFence = true;
////                            CommonUtils.logD("test0"+Globals.isInGeoFence);
//
//                            }
                        }

                    }

                    dataSource.geoFenceArrivalData.insertData(GeofenceIntentService.this, updateNodeModelList1);

                    if (requestIds.size() != 0) {
                        if (requestIds.contains(userName.substring(0, 4))) {
                            requestIds.remove(userName.substring(0, 4)); // do not remove sc code geofence
                        }
                        if (requestIds.size() != 0) {
                            mGeofencingClient.removeGeofences(requestIds).addOnCompleteListener(this);
                        }
                    }
                    break;
                default:
                    break;
            }

            String geofenceTransitionDetails = getGeofenceTransitionDetails(transition,
                    geofences);

            appendLog(DateFormat.getDateTimeInstance().format(new Date()) + " " + geofenceTransitionDetails, Constants.GEOFENCE_FILE);

            Log.i(TAG, geofenceTransitionDetails);

//            Intent serviceIntent = new Intent(GeofenceIntentService.this, OfflinePushService.class);
//            startService(serviceIntent);  //gayatri

        } else {
            String errorMessage = GeofenceErrorMessages.getErrorString(this,
                    geofencingEvent.getErrorCode());
            Log.e(TAG, errorMessage);
            appendLog(DateFormat.getDateTimeInstance().format(new Date()) + " " + errorMessage, Constants.GEOFENCE_FILE);
            return;
        }
    }

    /**
     * Gets transition details and returns them as a formatted string.
     *
     * @param geofenceTransition  The ID of the geofence transition.
     * @param triggeringGeofences The geofence(s) triggered.
     * @return The transition details formatted as String.
     */
    private String getGeofenceTransitionDetails(
            int geofenceTransition,
            List<Geofence> triggeringGeofences) {

        String geofenceTransitionString = getTransitionString(geofenceTransition);

        ArrayList<String> triggeringGeofencesIdsList = new ArrayList<>();
        for (Geofence geofence : triggeringGeofences) {
            triggeringGeofencesIdsList.add(geofence.getRequestId());
        }
        String triggeringGeofencesIdsString = TextUtils.join(", ", triggeringGeofencesIdsList);

        return geofenceTransitionString + ": " + triggeringGeofencesIdsString;
    }

    /**
     * Maps geofence transition types to their human-readable equivalents.
     *
     * @param transitionType A transition type constant defined in Geofence
     * @return A String indicating the type of transition
     */
    private String getTransitionString(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return getString(R.string.geofence_transition_entered);
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return getString(R.string.geofence_transition_exited);
            default:
                return getString(R.string.unknown_geofence_transition);
        }
    }

    public void appendLog(String text, String filename) {
        File logFile = new File(filename);
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void sendNotification(Context context, String notificationText,
                                  String notificationTitle) {

        PowerManager pm = (PowerManager) context
                .getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK, "RTS:Last Mile Delivery");
        wakeLock.acquire();

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
                context).setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(notificationTitle)
                .setContentText(notificationText)
                .setDefaults(Notification.DEFAULT_ALL).setAutoCancel(false);

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());

        wakeLock.release();
    }

    private String getTriggeringGeofences(Intent intent) {
        GeofencingEvent geofenceEvent = GeofencingEvent.fromIntent(intent);
        List<Geofence> geofences = geofenceEvent
                .getTriggeringGeofences();

        String[] geofenceIds = new String[geofences.size()];

        for (int i = 0; i < geofences.size(); i++) {
            geofenceIds[i] = geofences.get(i).getRequestId();
        }

        return TextUtils.join(", ", geofenceIds);
    }

    @Override
    public void onComplete(Task<Void> task) {
        if (task.isSuccessful()) {
            int messageId = R.string.geofences_removed;
            for (String s : requestIds) {
                appendLog(DateFormat.getDateTimeInstance().format(new Date()) + " " + this.getString(messageId) + " : " + s, Constants.GEOFENCE_FILE);
//                if (dataSource.pudGeofences.exists(s) != -1) {
//                    dataSource.pudGeofences.delRecord(s);
//                } //gayatri
            }
        } else {
            String errorMessage = GeofenceErrorMessages.getErrorString(this, task.getException());
            Log.w(TAG, errorMessage);
            appendLog(errorMessage, Constants.GEOFENCE_FILE);
        }
    }

    private void callArrivalDepatureApi(List<GeoFenceModel> list, String arrivalDepatureStr) {
        List<UpdateNodeModel> inputList = new ArrayList<>();
        for (GeoFenceModel model : list) {
            UpdateNodeModel node = new UpdateNodeModel();
            node.setNodeId(model.getNodeId());
            node.setDktNo(model.getDktNo());
            node.setDktNodeId(model.getDktNodeId());
            SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Calendar calendar = Calendar.getInstance();
            String format = s.format(calendar.getTime());
            node.setTimestamp(format);
            node.setSubprojectId(model.getSubProjectId());
            node.setLatitude(model.getLatitude());
            node.setLongitude(model.getLongitude());
            inputList.add(node);
        }

        try {
            ObjectMapper mapper = new ObjectMapper();
            String data = mapper.writeValueAsString(inputList);
            String url = LMDUtils.getCaptureGeofenceUrl(arrivalDepatureStr);
            CommonUtils.appendLog("Posting data to "+url+" data "+data,Constants.GEOFENCE_FILE);
            ResgistrationResponseModel response = (ResgistrationResponseModel) HttpRequest.postData(url,
                    data, Constants.CONTENT_TYPE, HttpRequest.CONTENT_TYPE, ResgistrationResponseModel.class);
            if (response != null) {
                CommonUtils.logD(response.toString());
                if (response.getStatus()) {
                    if(response.getData() != null && response.getData().size() > 0) {
                        RegistrationDataModel registrationDataModel = response.getData().get(0);

                        for(RegistrationDataModel model: Globals.tcList) {
                            if(model.getDktNo().equals(registrationDataModel.getDktNo())) {
                                model.setNodeList(registrationDataModel.getNodeList());
                                model.setStartNode(registrationDataModel.getStartNode());
                                model.setEndNode(registrationDataModel.getEndNode());
                                updateTcDataResult();
                            }
                        }
                        HashMap<LatLng, List<GeoFenceModel>> locationMap = dataSource.geoFences.getAllNodesData();
                        for (LatLng latLng : locationMap.keySet()) {
                            int dktNo = Integer.parseInt(registrationDataModel.getDktNo());
                            if (locationMap.get(latLng).get(0).getDktNo() == dktNo &&
                                    locationMap.get(latLng).get(0).getNodeId() == inputList.get(0).getNodeId()) {
                                if (arrivalDepatureStr.equals("A")) {
                                    dataSource.geoFences.updateTransition(latLng, 1);
                                } else if (arrivalDepatureStr.equals("D")) {
                                    dataSource.geoFences.updateTransition(latLng, 0);
                                }
                            }
                        }
                    }
                } else {
                    Globals.lastErrMsg = response.getMessage();
                }
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LastMileDeliveryException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateTcDataResult() {
        if (Globals.registrationDataModel != null) {
            if (CommonUtils.isValidArraylist(Globals.tcList)) {
                for (RegistrationDataModel model : Globals.tcList) {
                    if (Globals.registrationDataModel.getDktNo().equalsIgnoreCase(model.getDktNo())) {
                        Globals.registrationDataModel = model;

                        Intent in = new Intent("com.spoton.spl.lmapp.tcdata");
                        Bundle extras = new Bundle();
                        extras.putBoolean("registrationDataModelUpdated", true);
                        in.putExtras(extras);
                        GeofenceIntentService.this.getApplicationContext().sendBroadcast(in);
                    }
                }
            }
        }
    }
}