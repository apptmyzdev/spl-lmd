package com.spoton.spl.lmapp;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.spoton.spl.lmapp.GeoFencing.GeoFenceModel;
import com.spoton.spl.lmapp.GeoFencing.SetGeofence;
import com.spoton.spl.lmapp.datacache.DataSource;
import com.spoton.spl.lmapp.models.NodeModel;
import com.spoton.spl.lmapp.models.ReasonsResponse;
import com.spoton.spl.lmapp.models.RegistrationDataModel;
import com.spoton.spl.lmapp.models.RegistrationModel;
import com.spoton.spl.lmapp.utils.CommonUtils;
import com.spoton.spl.lmapp.utils.Constants;
import com.spoton.spl.lmapp.utils.Globals;
import com.spoton.spl.lmapp.utils.HttpRequest;
import com.spoton.spl.lmapp.utils.LMDUtils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class HomeScreen extends AppCompatActivity implements GetTcData.TcDataListener {

//    List<RegistrationDataModel> tcList;
    private RegistrationModel model;
    private DataSource dataSource;
    private Context context;

    ListView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            RegistrationDataModel model = (RegistrationDataModel) adapterView.getAdapter().getItem(i);
            if (model != null) {
                if (checkGPSStatus()) {
                    Globals.registrationDataModel = model;
                    if(Globals.registrationDataModel!=null) {
                        dataSource.sharedPreferences.set(Constants.maxImageSize, String.valueOf(Globals.registrationDataModel.getPodCount()));
                    }
                    Intent intent = new Intent(context, TCDetailsScreen.class);
                    startActivity(intent);
                }

            }

        }
    };
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {

                case R.id.capture_node_tv:
                    if (checkGPSStatus()) {
                        Intent intent = new Intent(context, CaptureNodeScreen.class);
                        startActivity(intent);
                    }
                    break;
                case R.id.logout:
                    HomeScreen.this.finish();
                    startActivity(new Intent(HomeScreen.this, LoginScreen.class));
                    break;

            }
        }
    };

    @Override
    public void updateTcDataResult() {
        swipeRefreshLayout.setRefreshing(false);
        if (CommonUtils.isValidArraylist(Globals.tcList)) {
            tcAdapter = new HomeScreen.TcAdapter(context, R.layout.routes_row, Globals.tcList);
            tcLv.setAdapter(tcAdapter);
//            createGeoFences(Globals.tcList);
        }
        new ReasonsTask().execute();
    }

    private TcAdapter tcAdapter;
    private ListView tcLv;
    private TextView captureNodeTv;
    private ImageView logoutBtn;
    private List<RegistrationDataModel> tcSearchList;
    private EditText tcSearchEt;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.home_screen);

        context = HomeScreen.this;

        dataSource = new DataSource(context);


        model = setRegistrationModel();
//        Globals.tcList = new ArrayList<>();


        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                model = setRegistrationModel();
                new GetTcData(HomeScreen.this).execute(model);
            }
        });
        tcSearchList = new ArrayList<>();


        tcSearchEt = (EditText) findViewById(R.id.tc_search_et);
        tcSearchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                Log.e(Tag, s.toString());
                String searchString = tcSearchEt.getText().toString();

                tcSearchList.clear();
                if (Globals.tcList != null) {
                    for (int i = 0; i < Globals.tcList.size(); i++) {
                        if (Globals.tcList.get(i).getDktNo().toLowerCase().trim().contains(
                                searchString.toLowerCase().trim())) {
                            tcSearchList.add(Globals.tcList.get(i));


                        }
                    }

                }
                tcAdapter = new TcAdapter(context, R.layout.routes_row, tcSearchList);
                tcLv.setAdapter(tcAdapter);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tcLv = (ListView) findViewById(R.id.tc_lv);
        tcLv.setOnItemClickListener(onItemClickListener);
        tcLv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (tcLv == null || tcLv.getChildCount() == 0) ? 0 : tcLv.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });


        captureNodeTv = (TextView) findViewById(R.id.capture_node_tv);
        captureNodeTv.setOnClickListener(onClickListener);
        captureNodeTv.setVisibility(View.GONE);

        logoutBtn = (ImageView) findViewById(R.id.logout);
        logoutBtn.setOnClickListener(onClickListener);

        new GetTcData(this).execute(model);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    private BroadcastReceiver broadcastReceiver;

    private void registerReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean dataModelUpdated = intent.getBooleanExtra("registrationDataModelUpdated", false);

                if(dataModelUpdated) {
                    if(tcAdapter != null) {
                        tcAdapter.notifyDataSetChanged();
                    }
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("com.spoton.spl.lmapp.tcdata"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(tcAdapter != null) {
            tcAdapter.notifyDataSetChanged();
        }
    }

    private RegistrationModel setRegistrationModel() {

        RegistrationModel model = new RegistrationModel();
        if (CommonUtils.isValidString(dataSource.sharedPreferences.getValue(Constants.DEVICE_TOKEN)))
            model.setPushToken(dataSource.sharedPreferences.getValue(Constants.DEVICE_TOKEN));

        if (CommonUtils.isValidString(CommonUtils.getImei(context)))
            model.setImei(CommonUtils.getImei(context));

        if (CommonUtils.isValidString(dataSource.sharedPreferences.getValue(Constants.MOBILE_NUMBER)))
            model.setMobileNo(dataSource.sharedPreferences.getValue(Constants.MOBILE_NUMBER));

        try {
            String versionName  = getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName;
            if(CommonUtils.isValidString(versionName)){
                model.setVersionName(versionName);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return model;
    }

    private boolean checkGPSStatus() {
        boolean isGpsTurnedon = false;
        LocationManager locationManager = null;
        boolean gps_enabled = false;
        boolean network_enabled = false;
        if (locationManager == null) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        }
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        if (!gps_enabled && !network_enabled) {
            isGpsTurnedon = false;
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setCancelable(false);
            dialog.setMessage(getString(R.string.turn_on_gps));
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //this will navigate user to the device location settings screen
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                    dialog.dismiss();
                }
            });
            AlertDialog alert = dialog.create();
            alert.show();
        } else
            isGpsTurnedon = true;

        return isGpsTurnedon;
    }

    private void createGeoFences(List<RegistrationDataModel> list) {

        List<GeoFenceModel> assignedGeofences = new ArrayList<>();
        if (CommonUtils.isValidArraylist(list)) {
            for (RegistrationDataModel model : list) {
                if (CommonUtils.isValidArraylist(model.getNodeList())) {


                    for (NodeModel m : model.getNodeList()) {
                        GeoFenceModel geoFenceModel = new GeoFenceModel(model.getSubprojectId(), m.getNodeId(), m.getDktNodeId(), Integer.parseInt(model.getDktNo()), 1, m.getLat(), m.getLng(), Constants.GEOFENCE_RADIUS, Constants.FALSE, Constants.FALSE, 0);
                        assignedGeofences.add(geoFenceModel);
                    }
                }
                if (model.getStartNode() != null && model.getStartNode().getLat() != null && model.getStartNode().getLng() != null) {
                    GeoFenceModel geoFenceModel = new GeoFenceModel(model.getSubprojectId(), model.getStartNode().getNodeId(), model.getStartNode().getDktNodeId(), Integer.parseInt(model.getDktNo()), 1, model.getStartNode().getLat(), model.getStartNode().getLng(), Constants.GEOFENCE_RADIUS, Constants.TRUE, Constants.FALSE, 0);
                    assignedGeofences.add(geoFenceModel);
                }

                if (model.getEndNode() != null && model.getEndNode().getLat() != null && model.getEndNode().getLng() != null) {
                    GeoFenceModel geoFenceModel = new GeoFenceModel(model.getSubprojectId(), model.getEndNode().getNodeId(), model.getEndNode().getDktNodeId(), Integer.parseInt(model.getDktNo()), 1, model.getEndNode().getLat(), model.getEndNode().getLng(), Constants.GEOFENCE_RADIUS, Constants.FALSE, Constants.TRUE, 0);
                    assignedGeofences.add(geoFenceModel);
                }
                CommonUtils.appendLog("creating geo fences",Constants.GEOFENCE_FILE);

                for(GeoFenceModel model1:assignedGeofences){
                    CommonUtils.appendLog("node id:"+model1.getNodeId()+" dkt node id "+model1.getDktNodeId(),Constants.GEOFENCE_FILE);
                }
                dataSource.geoFences.insertData(assignedGeofences);

//                startService(new Intent(context, EmployeeLocationService.class));





                SetGeofence setGeofence = new SetGeofence();
                setGeofence.performAction(context, assignedGeofences);
            }
        }
    }
    
    public class TcAdapter extends ArrayAdapter<RegistrationDataModel> {
        private LayoutInflater inflater;
        private int layoutId;

        public TcAdapter(Context context, int layoutId, List<RegistrationDataModel> list) {
            super(context, 0, list);
            this.layoutId = layoutId;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new Holder();
            holder.nameTv = (TextView) convertView
                    .findViewById(R.id.tv_route_code);

            RegistrationDataModel model = getItem(position);

            String code = model.getDktNo();
            if (CommonUtils.isValidString(code)) {
                holder.nameTv.setText(code);
                holder.nameTv.setTextColor(context.getResources().getColor(R.color.orange));
                if (CommonUtils.isValidString(model.getRouteName())) {
                    holder.nameTv.append(" -  " + model.getRouteName());
                }
            } else
                holder.nameTv.setText("");
            return convertView;
        }
    }

    public class Holder {
        public TextView nameTv;
    }

    class ReasonsTask extends AsyncTask<String, Object, Object> {
        ReasonsResponse reasonsResponse;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CommonUtils.getProgressDialog(context);
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";
                String url = LMDUtils.getReasonCodeUrlTag();
                InputStream response = HttpRequest.getInputStreamFromUrl(url, null, null);
                reasonsResponse = (ReasonsResponse) CommonUtils.parseResponse(response, ReasonsResponse.class);
                if (response != null) {
                    CommonUtils.logD(reasonsResponse.toString());
                    if (reasonsResponse.isStatus()) {
//                        if (CommonUtils.isValidArraylist(reasonsResponse.getData()))
//                            dataSource.reasonCodes.saveReasonCodes(reasonsResponse.getData());
                    } else {
                        Globals.lastErrMsg = reasonsResponse.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!CommonUtils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }


        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);
            CommonUtils.dismissProgressDialog();
            CommonUtils.showErrorDialog(context);
            if (reasonsResponse != null && reasonsResponse.isStatus() && CommonUtils.isValidArraylist(reasonsResponse.getData()))
                dataSource.reasonCodes.saveReasonCodes(reasonsResponse.getData());

        }
    }

}