package com.spoton.spl.lmapp.fileutils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.net.Uri;
import android.util.Base64;

import com.spoton.spl.lmapp.utils.CommonUtils;
import com.spoton.spl.lmapp.utils.Constants;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Developed by Ashish Das
 * <p>
 * Email: adas@revamobile.com ,adas@revasolutions.com
 * <p>
 * Date: October 27, 2012
 * <p>
 * All code (c) 2011 Reva Tech Solutions (India) Private Limited (Reva Mobile)
 * <p>
 * All rights reserved
 */
public class FileCache {

    public static final String FOLDER_NAME = "PLT";

    public static final String textTyp = "text";
    public static final String imageTyp = "image";
    public static final String audioTyp = "audio";
    public static final String videoTyp = "video";
    public static final String pdfTyp = "pdf";
    public static final String htmlTyp = "html";
    public static final String userAnswerImageType = "user answer";
    private final int MEGABYTE = 1024 * 1024;
    private File mainDir;
    private File childDir;
    private File subDir;

    /**
     * FileCache class
     *
     * @param context
     * @param folderName
     * @param fileType
     */

    public FileCache(Context context, String folderName, String fileType) {
        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED))
            mainDir = new File(
                    android.os.Environment.getExternalStorageDirectory(),
                    FOLDER_NAME);
        else
            mainDir = context.getCacheDir();
        if (!mainDir.exists())
            mainDir.mkdirs();

        if (CommonUtils.isValidString(folderName) && CommonUtils.isValidString(fileType)) {
            if (Constants.isUseingFileEncryption)
                folderName = String.valueOf(folderName.hashCode());

            childDir = new File(mainDir, folderName);
            if (!childDir.exists())
                childDir.mkdirs();

            subDir = new File(childDir, fileType);
            if (!subDir.exists())
                subDir.mkdirs();
        } else if (CommonUtils.isValidString(folderName)) {
            if (Constants.isUseingFileEncryption)
                folderName = String.valueOf(folderName.hashCode());
            subDir = new File(mainDir, folderName);
            if (!subDir.exists())
                subDir.mkdirs();

        } else if (CommonUtils.isValidString(fileType)) {
            subDir = new File(mainDir, fileType);
            if (!subDir.exists())
                subDir.mkdirs();

        } else
            subDir = mainDir;

    }

    /**
     * file save method
     *
     * @param is
     * @param file
     * @throws IOException
     */
    public void saveFile(InputStream is, File file) throws IOException {
        byte[] byteArr = null;
        InputStream inputStream = null;

        if (Constants.isUseingFileEncryption)
            byteArr = IOUtils.toByteArray(is);

        if (file.exists()) {
            file.delete();
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            CommonUtils.logE(e.toString());
        }

        if (file.exists()) {
            // file.setReadOnly();

            OutputStream os = new FileOutputStream(file);

            if (Constants.isUseingFileEncryption) {
                try {
                    inputStream = FileEncryption.encrypt(byteArr);
                } catch (Exception e) {
                    inputStream = null;
                }
                FileUtils.copyStream(inputStream, os);
            } else
                FileUtils.copyStream(is, os);

            os.close();
        }

    }

    public void downloadFile(String fileUrl, File directory) {
        try {
            FileOutputStream f = new FileOutputStream(directory);
            URL u = new URL(fileUrl);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            InputStream in = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void savePdfFile(String url) {
        File file = getFile(url);
        downloadFile(url, file);
    }

    public void saveHtmlFile(String url) {
        File file = getHtmlFile(url);
        downloadFile(url, file);
    }

    public File getHtmlFile(String name) {
        String filename = name;
        // if (Constants.isUseingFileEncryption)
        filename = String.valueOf(name.hashCode()) + ".html";

        File file = new File(subDir, filename);
        // file.setWritable(true);
        return file;
    }

    public File getPdfFile(String name) {
        String filename = name;
        // if (Constants.isUseingFileEncryption)
        filename = String.valueOf(name.hashCode()) + ".pdf";

        File file = new File(subDir, filename);
        // file.setWritable(true);
        return file;

    }

    public File getTxtFile(String name) {
        String filename = name;
        // if (Constants.isUseingFileEncryption)
        // filename = String.valueOf(name.hashCode());
        filename = String.valueOf(name) + ".txt";

        File file = new File(subDir, filename);
        file.setWritable(true);
        return file;

    }

    /**
     * Get File method
     *
     * @param name
     * @return File
     */
    public File getFile(String name) {
        String filename = name;
        // if (Constants.isUseingFileEncryption)
        filename = String.valueOf(name.hashCode());
        // filename = String.valueOf(name);

        File file = new File(subDir, filename);
        file.setWritable(true);
        return file;

    }

    /**
     * Delete File method
     *
     * @param name
     */
    public void deleteFile(String name) {
        File f = getFile(name);
        if (f != null)
            f.delete();
    }

    /**
     * Clear all data in sub dir folder method
     */
    public void clear() {
        clearFile(subDir);
    }

    private void clearFile(File file) {
        File[] files = file.listFiles();
        if (files == null)
            return;
        for (File f : files) {
            if (f != null) {
                if (f.isDirectory()) {
                    clearFile(f);
                } else
                    f.delete();
            }
        }
    }

    /**
     * Get Text File
     *
     * @param name
     * @return InputStream
     */
    public InputStream getTextFile(String name) {
        File file = getFile(name);
        InputStream is = null;
        try {
            if (Constants.isUseingFileEncryption)
                is = FileEncryption.decrypt(new FileInputStream(file));
            else
                is = new FileInputStream(file);
        } catch (Exception e) {
            is = null;
        }
        if (is != null) {
            return is;
        }
        return null;
    }

    public void saveTxtFile(String value, String name) throws IOException {
        File file = getTxtFile(name);
        InputStream is = FileUtils.stringToInputStream(value);
        saveFile(is, file);
    }

    /**
     * Save text file
     *
     * @param value
     * @param name
     * @throws IOException
     */
    public void saveTextFile(String value, String name) throws IOException {
        File file = getFile(name);
        InputStream is = FileUtils.stringToInputStream(value);
        saveFile(is, file);
    }

    public void saveByteDataFile(byte[] value, String name) throws IOException {
        File file = getFile(name);
        InputStream is = FileUtils.byteArrayToInputStream(value);
        saveFile(is, file);
    }

    /**
     * Save Image from a url
     *
     * @param url
     * @throws Throwable
     */
    public void saveImageToHttp(String url) throws Throwable {
        getBitmapToHttp(url);
    }

    /**
     * Save Bitmap File
     *
     * @param bitmap
     * @param name
     * @return
     */
    public Uri saveBitmapFile(Bitmap bitmap, String name) {
        Uri uri = null;
        OutputStream outStream = null;

        File file = getFile(name);

        if (file.exists()) {
            file.delete();
        }
        try {
            uri = Uri.fromFile(file);
            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.flush();
            outStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return uri;
    }

    /**
     * Get Bitmap From url
     *
     * @param url
     * @return
     * @throws Throwable
     */
    public Bitmap getBitmapToHttp(String url) throws Throwable {
        if (url == null || url.length() == 0) {
            CommonUtils.logD("Image url is Null Or length is zero");
            return null;
        }
        File file = getFile(url);
        Bitmap b = FileUtils.decodeFile(file);
        if (b != null)
            return b;
        try {
            Bitmap bitmap = null;

            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl
                    .openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            saveFile(is, file);
            bitmap = FileUtils.decodeFile(file);
            return bitmap;
        } catch (Throwable ex) {
            throw ex;
        }
    }

    public Bitmap getBitmapToBase64String(String base64String) throws Throwable {
        if (base64String == null || base64String.length() == 0) {
            CommonUtils.logD("Base64 String Image is Null Or length is zero");
            return null;
        }
        File file = getFile(base64String);
        Bitmap b = FileUtils.decodeFile(file);
        if (b != null)
            return b;
        try {

            Bitmap bitmap = null;
            byte[] byteArr = Base64.decode(base64String, Base64.DEFAULT);
            InputStream is = new ByteArrayInputStream(byteArr);
            saveFile(is, file);
            bitmap = FileUtils.decodeFile(file);

            return bitmap;
        } catch (Throwable ex) {
            throw ex;
        }
    }

    public String getBase64StringToName(String base64String, String data,
                                        Context context) throws Throwable {
        String base64img = null;
        if (base64String == null || base64String.length() == 0) {
            CommonUtils.logD("Base64 String Image is Null Or length is zero");
        } else {
            File file = getFile(base64String);
            Bitmap b = FileUtils.decodeFile(file);
            if (b != null) {
                try {
                    Bitmap newBitmap = null;
                    if (CommonUtils.isValidString(data)) {
                        Config config = b.getConfig();
                        if (config == null) {
                            config = Config.ARGB_8888;
                        }
                        // DataSource d = new DataSource(context);
                        // String s =
                        // d.sharedPreferences.getValue(Constants.IMEI);
                        // if (CommonUtils.isValidString(s))
                        // data = data + "\n" + s;
                        // data = data
                        // + "\n"
                        // + new Date(Long.valueOf(base64String))
                        // .toString();
//						CommonUtils.logD("W : " + b.getWidth() + " H : "
//								+ b.getHeight());
                        if (b.getWidth() < 300)
                            newBitmap = Bitmap.createBitmap(320,
                                    b.getHeight() + 220, config);
                        newBitmap = Bitmap.createBitmap(b.getWidth(),
                                b.getHeight() + 220, config);
                        Canvas newCanvas = new Canvas(newBitmap);
                        newCanvas.drawColor(Color.WHITE);
                        newCanvas.drawBitmap(b, 0, 0, null);

                        Paint paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
                        paintText.setColor(Color.BLUE);
                        paintText.setTextSize(18);
                        paintText.setStyle(Style.FILL);
                        // paintText.setShadowLayer(10f, 10f, 10f, Color.BLACK);

                        Rect rectText = new Rect();
                        paintText.getTextBounds(data, 0, data.length(),
                                rectText);

                        // newCanvas.drawText(data, 0, b.getHeight() + 50,
                        // paintText);
                        int x = 5, y = b.getHeight() + 25;
                        for (String line : data.split("\n")) {
                            newCanvas.drawText(line, x, y, paintText);
                            y += paintText.descent() - paintText.ascent();
                        }

//						CommonUtils.logD("W : " + newBitmap.getWidth() + " H : "
//								+ newBitmap.getHeight());

                    }
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    if (CommonUtils.isValidString(data) && newBitmap != null) {
                        newBitmap.compress(Bitmap.CompressFormat.JPEG, 70,
                                stream);
                    } else {
                        b.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                    }
                    byte[] byteArray = stream.toByteArray();

                    base64img = Base64
                            .encodeToString(byteArray, Base64.DEFAULT);

                    if (CommonUtils.isValidString(base64img)) {
                        base64img = base64img.trim();
                        base64img = base64img.replace("\n", "");
                    }

                } catch (Throwable ex) {
                    throw ex;
                }
            }
        }
        return base64img;

    }

    /**
     * Get Bitmap From url
     *
     * @param url
     * @return
     * @throws Throwable
     */
    public Bitmap getBitmapToHttp(String url, boolean isResize)
            throws Throwable {
        if (url == null || url.length() == 0) {
            CommonUtils.logD("Image url is Null Or length is zero");
            return null;
        }
        File file = getFile(url);
        if (file.exists())
            CommonUtils.logD("IMAGE URL : " + url + ", file.exists()");
        Bitmap b = FileUtils.decodeFile(file, isResize);
        if (b != null)
            return b;
        try {
            Bitmap bitmap = null;

            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl
                    .openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            saveFile(is, file);
            bitmap = FileUtils.decodeFile(file, isResize);
            return bitmap;
        } catch (Throwable ex) {
            throw ex;
        }
    }

    public Bitmap getBitmapToFilePath(String filePath, boolean isResize)
            throws Throwable {
        if (filePath == null || filePath.length() == 0) {
            CommonUtils.logD("Image File Path is Null Or length is zero");
            return null;
        }
        File file = new File(filePath);
        // Log.e("SWATHI", "Image File Path : " + filePath);
        Bitmap bitmap = FileUtils.decodeFile(file, isResize);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        if (bitmap != null)
            return bitmap;

        return null;
    }

    public Bitmap getBitmapToBase64String(String base64String, boolean isResize)
            throws Throwable {
        if (base64String == null || base64String.length() == 0) {
            CommonUtils.logD("Base64 String Image is Null Or length is zero");
            return null;
        }
        File file = getFile(base64String);
        Bitmap b = FileUtils.decodeFile(file, isResize);
        if (b != null)
            return b;
        try {

            Bitmap bitmap = null;
            byte[] byteArr = Base64.decode(base64String, Base64.DEFAULT);
            InputStream is = new ByteArrayInputStream(byteArr);
            saveFile(is, file);
            bitmap = FileUtils.decodeFile(file, isResize);

            return bitmap;
        } catch (Throwable ex) {
            CommonUtils.logE(ex.toString());
            throw ex;
        }
    }

    public String getBase64StringToName(String base64String, boolean isResize)
            throws Throwable {
        String base64img = null;
        if (base64String == null || base64String.length() == 0) {
            CommonUtils.logD("Base64 String Image is Null Or length is zero");
        } else {
            File file = getFile(base64String);
            Bitmap b = FileUtils.decodeFile(file, isResize);
            if (b != null) {
                try {

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    b.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();

                    base64img = Base64
                            .encodeToString(byteArray, Base64.DEFAULT);

                    if (CommonUtils.isValidString(base64img)) {
                        base64img = base64img.trim();
                        base64img = base64img.replace("\n", "");
                    }

                } catch (Throwable ex) {
                    throw ex;
                }
            }
        }
        return base64img;

    }
}
