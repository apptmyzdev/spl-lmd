package com.spoton.spl.lmapp.models;

import java.util.List;

public class ReasonsResponse {
    private boolean status;
    private String message;
    private List<ReasonsModel> data;
    private int statusCode;
    private String token;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ReasonsModel> getData() {
        return data;
    }

    public void setData(List<ReasonsModel> data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "ExceptionTypeResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", statusCode=" + statusCode +
                ", token='" + token + '\'' +
                '}';
    }
}
