package com.spoton.spl.lmapp.models;

import java.util.List;

public class UpdateNodeModel {
    private int nodeId;
    private int dktNodeId;
    private int subprojectId;
    private long dktNo;
    private String timestamp;
    private String remarks;
    private int pktCount;
    private Double latitude;
    private Double longitude;
    private List<String> pod;
    private int delayReasonId;
    private String isDeparture;
    private String isEndNode;

    public int getSubprojectId() {
        return subprojectId;
    }

    public void setSubprojectId(int subprojectId) {
        this.subprojectId = subprojectId;
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public int getDktNodeId() {
        return dktNodeId;
    }

    public void setDktNodeId(int dktNodeId) {
        this.dktNodeId = dktNodeId;
    }

    public long getDktNo() {
        return dktNo;
    }

    public void setDktNo(long dktNo) {
        this.dktNo = dktNo;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getPktCount() {
        return pktCount;
    }

    public void setPktCount(int pktCount) {
        this.pktCount = pktCount;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public List<String> getPod() {
        return pod;
    }

    public void setPod(List<String> pod) {
        this.pod = pod;
    }

    public int getDelayReasonId() {
        return delayReasonId;
    }

    public void setDelayReasonId(int delayReasonId) {
        this.delayReasonId = delayReasonId;
    }

    public String getIsDeparture() {
        return isDeparture;
    }

    public void setIsDeparture(String isDeparture) {
        this.isDeparture = isDeparture;
    }

    public String getIsEndNode() {
        return isEndNode;
    }

    public void setIsEndNode(String isEndNode) {
        this.isEndNode = isEndNode;
    }

    @Override
    public String toString() {
        return "UpdateNodeModel{" +
                "nodeId=" + nodeId +
                ", dktNodeId=" + dktNodeId +
                ", subprojectId=" + subprojectId +
                ", dktNo=" + dktNo +
                ", timestamp='" + timestamp + '\'' +
                ", remarks='" + remarks + '\'' +
                ", pktCount=" + pktCount +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", pod=" + pod +
                ", delayReasonId=" + delayReasonId +
                ", isDeparture='" + isDeparture + '\'' +
                ", isEndNode='" + isEndNode + '\'' +
                '}';
    }
}
