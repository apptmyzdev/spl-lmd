package com.spoton.spl.lmapp.models;

import androidx.annotation.Nullable;

public class LatLng {
    private Double latitude;
    private Double longitude;

    public LatLng(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == null && obj == null)
            return true;
        if (obj == null || !(obj instanceof LatLng))
            return false;
        LatLng other = (LatLng) obj;

        if (other.getLatitude() != null && this.latitude != null && other.getLongitude() != null
                && this.longitude != null && other.getLatitude().equals(this.latitude)
                && other.getLongitude().equals(this.longitude))
            return true;

        return false;
    }

    @Override
    public String toString() {
        return "LatLng{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
