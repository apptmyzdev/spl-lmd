package com.spoton.spl.lmapp.models;

public class NodeLocationModel {

    private int nodeId;
    private double latitude;
    private double longitude;

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "NodeLocationModel{" +
                "nodeId=" + nodeId +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
