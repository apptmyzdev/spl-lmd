package com.spoton.spl.lmapp.utils;

import android.graphics.Bitmap;

import com.spoton.spl.lmapp.models.RegistrationDataModel;
import com.spoton.spl.lmapp.models.RegistrationModel;
import com.spoton.spl.lmapp.models.UpdateNodeModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GAYATRI on 27-JUL-20.
 */

public class Globals {


    public static String lastErrMsg;
    public static int screenWidth;
    public static int screenHeight;
    public static Bitmap bitmap;
    public static Boolean isInGeoFence = false;
    public static int action = -1;
    public static RegistrationDataModel registrationDataModel;
    public static UpdateNodeModel updateNodeModel;
    public static List<RegistrationDataModel> tcList = new ArrayList<>();;


}