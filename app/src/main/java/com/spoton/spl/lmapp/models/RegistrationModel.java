package com.spoton.spl.lmapp.models;

public class RegistrationModel {
    private String imei;
    private String simCardSerialNo;
    private String mobileNo;
    private String pushToken;
    private String versionName;

    public RegistrationModel() {
    }

    public RegistrationModel(String imei, String simCardSerialNo, String mobileNo, String pushToken) {
        this.imei = imei;
        this.simCardSerialNo = simCardSerialNo;
        this.mobileNo = mobileNo;
        this.pushToken = pushToken;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSimCardSerialNo() {
        return simCardSerialNo;
    }

    public void setSimCardSerialNo(String simCardSerialNo) {
        this.simCardSerialNo = simCardSerialNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    @Override
    public String toString() {
        return "RegistrationModel{" +
                "imei='" + imei + '\'' +
                ", simCardSerialNo='" + simCardSerialNo + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", pushToken='" + pushToken + '\'' +
                ", versionName='" + versionName + '\'' +
                '}';
    }
}
