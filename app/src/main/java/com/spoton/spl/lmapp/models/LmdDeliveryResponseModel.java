package com.spoton.spl.lmapp.models;

public class LmdDeliveryResponseModel {
	private boolean delayCaptured;

	public boolean isDelayCaptured() {
		return delayCaptured;
	}

	public void setDelayCaptured(boolean isDelayCaptured) {
		this.delayCaptured = isDelayCaptured;
	}
}
