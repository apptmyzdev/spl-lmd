package com.spoton.spl.lmapp.fileutils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;


import com.spoton.spl.lmapp.utils.CommonUtils;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Developed by Ashish Das
 * <p>
 * Email: adas@revamobile.com Email: adas@revasolutions.com
 * <p>
 * Date: October 27, 2012
 * <p>
 * All code (c) 2011 Reva Tech Solutions (India) Private Limited (Reva Mobile)
 * <p>
 * All rights reserved
 */
public class ImageLoader {

    public MemoryCache memoryCache = new MemoryCache();
    public FileCache fileCache;
    public ExecutorService executorService;
    public String folderName;
    public Context context;
    private Map<ImageView, String> imageViews = Collections
            .synchronizedMap(new WeakHashMap<ImageView, String>());

    // public final int resId = R.drawable.hourglass;

    /**
     * Create ImageLoader
     *
     * @param context
     * @param folderName
     */

    public ImageLoader(Context context, String folderName) {
        super();
        setContext(context);
        setFolderName(folderName);
        setFileCache(false);
        executorService = Executors.newFixedThreadPool(5);
    }

    public void setFileCache(boolean isUserAnswer) {
        String fileTyp = FileCache.imageTyp;
        if (isUserAnswer)
            fileTyp = FileCache.userAnswerImageType;

        this.fileCache = new FileCache(context, getFolderName(), fileTyp);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    /**
     * Display image in loader
     *
     * @param url
     * @param imageView
     * @param progressBar
     */
    public void DisplayImage(String url, ImageView imageView,
                             ProgressBar progressBar) {
        imageViews.put(imageView, url);
        Bitmap bitmap = memoryCache.get(url);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
            imageView.setVisibility(View.VISIBLE);
            if (progressBar != null)
                progressBar.setVisibility(View.GONE);

        } else {
            queuePhoto(url, false, imageView, progressBar);
            // imageView.setImageResource(resId);
            if (progressBar != null)
                progressBar.setVisibility(View.VISIBLE);

        }
    }


    public void DisplayBase64Image(String base64Image, ImageView imageView,
                                   ProgressBar progressBar) {
        imageViews.put(imageView, base64Image);
        Bitmap bitmap = memoryCache.get(base64Image);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
            imageView.setVisibility(View.VISIBLE);
            if (progressBar != null)
                progressBar.setVisibility(View.GONE);

        } else {
            queuePhoto(base64Image, true, imageView, progressBar);
            // imageView.setImageResource(resId);
            if (progressBar != null)
                progressBar.setVisibility(View.VISIBLE);

        }
    }

    /**
     * start thread to getting bitmap
     *
     * @param url
     * @param imageView
     * @param progressBar
     */
    private void queuePhoto(String url, boolean isBase64Img,
                            ImageView imageView, ProgressBar progressBar) {
        PhotoToLoad p = new PhotoToLoad(url, isBase64Img, imageView,
                progressBar);
        executorService.submit(new PhotosLoader(p));
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }

    /**
     * Display image in loader
     *
     * @param url
     * @param imageView
     * @param progressBar
     */
    public void DisplayImage(String url, ImageView imageView,
                             ProgressBar progressBar, boolean isResize) {
        CommonUtils.logD("URL : " + url);
        imageViews.put(imageView, url);
        Bitmap bitmap = memoryCache.get(url);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
            imageView.setVisibility(View.VISIBLE);
            if (progressBar != null)
                progressBar.setVisibility(View.GONE);

        } else {
            queuePhoto(url, false, false, imageView, progressBar, isResize);
            // imageView.setImageResource(resId);
            if (progressBar != null)
                progressBar.setVisibility(View.VISIBLE);

        }
    }

    /**
     * start thread to getting bitmap
     *
     * @param url
     * @param imageView
     * @param progressBar
     */
    private void queuePhoto(String url, boolean isBase64Img,
                            boolean isFilePath, ImageView imageView, ProgressBar progressBar,
                            boolean isResize) {
        PhotoToLoadNew p = new PhotoToLoadNew(url, imageView, progressBar,
                isBase64Img, isFilePath);
        try {
            executorService.submit(new PhotosLoaderNew(p, imageView, isResize));
        } catch (OutOfMemoryError e) {
        }
    }

    boolean imageViewReusedNew(PhotoToLoadNew photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    /**
     * Photo To load object class
     */
    private class PhotoToLoad {
        public String url;
        public ImageView imageView;
        public ProgressBar progressBar;
        public boolean isBase64Img;

        public PhotoToLoad(String u, boolean isBase64Img, ImageView i,
                           ProgressBar p) {
            url = u;
            imageView = i;
            progressBar = p;
            this.isBase64Img = isBase64Img;
        }
    }

    /**
     * Used to getting bitmap in the thread
     */
    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            Bitmap bmp = null;
            try {
                if (photoToLoad.isBase64Img)
                    bmp = fileCache.getBitmapToBase64String(photoToLoad.url);
                else
                    bmp = fileCache.getBitmapToHttp(photoToLoad.url);
            } catch (Throwable e) {
                if (e instanceof OutOfMemoryError)
                    memoryCache.clear();
                bmp = null;
            }
            memoryCache.put(photoToLoad.url, bmp);
            if (imageViewReused(photoToLoad))
                return;
            BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
            Activity a = (Activity) photoToLoad.imageView.getContext();
            a.runOnUiThread(bd);
        }
    }

    /**
     * Used to display bitmap in the UI thread
     */
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
            bitmap = b;
            photoToLoad = p;
        }

        @Override
        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            if (bitmap != null) {
                photoToLoad.imageView.setImageBitmap(bitmap);
                photoToLoad.imageView.setVisibility(View.VISIBLE);
                if (photoToLoad.progressBar != null)
                    photoToLoad.progressBar.setVisibility(View.GONE);
            } else {
                // photoToLoad.imageView.setImageResource(resId);
                if (photoToLoad.progressBar != null)
                    photoToLoad.progressBar.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Photo To load object class
     */
    private class PhotoToLoadNew {
        public String url;
        public ImageView imageView;
        public ProgressBar progressBar;
        public boolean isBase64Img;
        public boolean isFilePath;

        public PhotoToLoadNew(String url, ImageView imageView,
                              ProgressBar progressBar, boolean isBase64Img, boolean isFilePath) {
            super();
            this.url = url;
            this.imageView = imageView;
            if (progressBar != null)
                this.progressBar = progressBar;
            this.isBase64Img = isBase64Img;
            this.isFilePath = isFilePath;
        }

    }

    /**
     * Used to getting bitmap in the thread
     */
    class PhotosLoaderNew implements Runnable {
        PhotoToLoadNew photoToLoad;
        ImageView imageView;
        boolean isResize;

        PhotosLoaderNew(PhotoToLoadNew photoToLoad, ImageView imageView,
                        boolean isResize) {
            this.photoToLoad = photoToLoad;
            this.imageView = imageView;
            this.isResize = isResize;
        }

        @Override
        public void run() {
            if (imageViewReusedNew(photoToLoad))
                return;
            Bitmap bmp = null;
            try {
                if (photoToLoad.isFilePath) {
                    bmp = fileCache.getBitmapToFilePath(photoToLoad.url,
                            isResize);
                } else if (photoToLoad.isBase64Img) {
                    bmp = fileCache.getBitmapToBase64String(photoToLoad.url,
                            isResize);
                } else {
                    bmp = fileCache.getBitmapToHttp(photoToLoad.url, isResize);
                }
            } catch (Throwable e) {
                if (e instanceof OutOfMemoryError)
                    memoryCache.clear();
                bmp = null;
            }

            memoryCache.put(photoToLoad.url, bmp);

            if (imageViewReusedNew(photoToLoad))
                return;

            BitmapDisplayerNew bd = new BitmapDisplayerNew(bmp, photoToLoad);
            Activity a = (Activity) photoToLoad.imageView.getContext();
            a.runOnUiThread(bd);

        }
    }

    /**
     * Used to display bitmap in the UI thread
     */
    class BitmapDisplayerNew implements Runnable {
        Bitmap bitmap;
        PhotoToLoadNew photoToLoad;

        public BitmapDisplayerNew(Bitmap b, PhotoToLoadNew p) {
            bitmap = b;
            photoToLoad = p;
        }

        @Override
        public void run() {
            if (imageViewReusedNew(photoToLoad))
                return;
            if (bitmap != null) {
                photoToLoad.imageView.setImageBitmap(bitmap);
                photoToLoad.imageView.setVisibility(View.VISIBLE);
                if (photoToLoad.progressBar != null)
                    photoToLoad.progressBar.setVisibility(View.GONE);
            } else {
                // photoToLoad.imageView.setImageResource(resId);
                if (photoToLoad.progressBar != null)
                    photoToLoad.progressBar.setVisibility(View.GONE);
            }
        }
    }

}
