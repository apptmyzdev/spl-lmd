package com.spoton.spl.lmapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.spoton.spl.lmapp.GeoFencing.GatherLocation;
import com.spoton.spl.lmapp.models.NodeLocationModel;
import com.spoton.spl.lmapp.models.NodeModel;
import com.spoton.spl.lmapp.models.SubProjectModel;
import com.spoton.spl.lmapp.responses.GeneralResponse;
import com.spoton.spl.lmapp.responses.GetNodeResponse;
import com.spoton.spl.lmapp.responses.SubprojectResponse;
import com.spoton.spl.lmapp.utils.CommonUtils;
import com.spoton.spl.lmapp.utils.Constants;
import com.spoton.spl.lmapp.utils.Globals;
import com.spoton.spl.lmapp.utils.HttpRequest;
import com.spoton.spl.lmapp.utils.LMDUtils;
import com.spoton.spl.lmapp.utils.LastMileDeliveryException;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class CaptureNodeScreen extends FragmentActivity implements OnMapReadyCallback {

    private Context context;
    private TextView subProjectTv, nodeNameTv;
    private RelativeLayout subProjectRl, nodeRl;
    private RelativeLayout subProjectPopup, nodePopup;
    private Integer subProjectId = -1, nodeId = -1;
    private ListView subProjectLv, nodeLv;
    private TextView captureLocationTv;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private List<SubProjectModel> subProjectList;
    private List<NodeModel> nodeModelList;
    private LatLng latLng;
    private EditText subProjectSearchEt, nodeSearchEt;
    private NodeAdapter nodeAdapter;
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.sub_project_rl:
                    new GetSubProject().execute();
                    break;
                case R.id.node_rl:
                    if (subProjectId != -1) {

                        CommonUtils.dissmissKeyboard(nodeSearchEt);
                        nodeSearchEt.setText("");
                        nodePopup.setVisibility(View.VISIBLE);
                        if (CommonUtils.isValidArraylist(nodeModelList)) {
                            nodeAdapter = new NodeAdapter(context, R.layout.sub_project_list_item, nodeModelList);
                            nodeLv.setAdapter(nodeAdapter);
                        }
                    } else {
                        CommonUtils.showToastShort(context, getString(R.string.alert_sub_project));
                    }

                    break;
                case R.id.capture_node_tv:
                    if (subProjectId != -1) {
                        if (nodeId != -1) {
                            if (latLng != null && latLng.latitude != 0.0 && latLng.longitude != 0.0) {

                                NodeLocationModel model = new NodeLocationModel();
                                model.setLatitude(latLng.latitude);
                                model.setLongitude(latLng.longitude);
                                model.setNodeId(nodeId);
                                new UpdateNodeLocation().execute(model);

                            } else {
                                CommonUtils.showToastShort(context, getString(R.string.alert_location));
                            }
                        } else {
                            CommonUtils.showToastShort(context, getString(R.string.alert_node));
                        }

                    } else {
                        CommonUtils.showToastShort(context, getString(R.string.alert_sub_project));
                    }

                    break;
            }
        }
    };
    private SubProjectAdapter subProjectAdapter;
    private List<NodeModel> nodeSortList;
    private List<SubProjectModel> subprojectSortList;
    private Integer textLength;
    private String searchString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.capture_node_screen);

        subProjectList = new ArrayList<>();
        nodeSortList = new ArrayList<>();
        subprojectSortList = new ArrayList<>();

        context = CaptureNodeScreen.this;

        subProjectTv = (TextView) findViewById(R.id.sub_project_tv);
        nodeNameTv = (TextView) findViewById(R.id.node_tv);

        captureLocationTv = (TextView) findViewById(R.id.capture_node_tv);
        captureLocationTv.setOnClickListener(onClickListener);

        subProjectRl = (RelativeLayout) findViewById(R.id.sub_project_rl);
        subProjectRl.setOnClickListener(onClickListener);

        nodeRl = (RelativeLayout) findViewById(R.id.node_rl);
        nodeRl.setOnClickListener(onClickListener);

        subProjectPopup = (RelativeLayout) findViewById(R.id.subproject_popup_rl);
        nodePopup = (RelativeLayout) findViewById(R.id.node_popup_rl);


        nodeSearchEt = (EditText) findViewById(R.id.node_search_et);
        nodeSearchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                Log.e(Tag, s.toString());
                textLength = nodeSearchEt.getText().length();
                searchString = nodeSearchEt.getText().toString();

                nodeSortList.clear();
                if (nodeModelList != null) {
                    for (int i = 0; i < nodeModelList.size(); i++) {
                        if (nodeModelList.get(i).getNodeName().toLowerCase().trim().contains(
                                searchString.toLowerCase().trim())) {
                            nodeSortList.add(nodeModelList.get(i));


                        }
                    }

                }
                nodeAdapter = new NodeAdapter(context, R.layout.sub_project_list_item, nodeSortList);
                nodeLv.setAdapter(nodeAdapter);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        subProjectSearchEt = (EditText) findViewById(R.id.subproject_search_et);
        subProjectSearchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                Log.e(Tag, s.toString());
                textLength = subProjectSearchEt.getText().length();
                searchString = subProjectSearchEt.getText().toString();

                subprojectSortList.clear();
                if (subProjectList != null) {
                    for (int i = 0; i < subProjectList.size(); i++) {
                        if (subProjectList.get(i).getName().toLowerCase().trim().contains(
                                searchString.toLowerCase().trim())) {
                            subprojectSortList.add(subProjectList.get(i));


                        }
                    }

                }
                subProjectAdapter = new SubProjectAdapter(context, R.layout.sub_project_list_item, subprojectSortList);
                subProjectLv.setAdapter(subProjectAdapter);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        subProjectLv = (ListView) findViewById(R.id.project_lv);
        subProjectLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SubProjectModel model = (SubProjectModel) adapterView.getAdapter().getItem(i);
                if (CommonUtils.isValidString(model.getName()))
                    subProjectTv.setText(model.getName());
                subProjectId = model.getSubProjectId();
                nodeNameTv.setText(getText(R.string.select_node));
                nodeId = -1;
                subProjectPopup.setVisibility(View.GONE);
                new GetNodes().execute(subProjectId);
            }
        });
//        subProjectLv.setOnItemClickListener(onItemClickListener);

        nodeLv = (ListView) findViewById(R.id.node_lv);
        nodeLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                NodeModel model = (NodeModel) adapterView.getAdapter().getItem(i);
                if (CommonUtils.isValidString(model.getNodeName()))
                    nodeNameTv.setText(model.getNodeName());
                nodeId = model.getNodeId();
                nodePopup.setVisibility(View.GONE);
            }
        });


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        latLng = null;
        if (checkGPSStatus()) {
            GatherLocation gatherlocation = new GatherLocation();


            Location location = gatherlocation.getLocation(context);
            if (location != null) {
                double lat = location.getLatitude();
                double lng = location.getLongitude();
                latLng = new LatLng(lat, lng);
            }
        }

//
        if (latLng != null) {
            placeMarker(latLng);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(18).bearing(0).tilt(30).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } else {
            latLng = new LatLng(0.000, 0.000);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(10).bearing(0).tilt(30).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                latLng = latLng;
                placeMarker(latLng);
            }
        });


    }

    public void placeMarker(LatLng latLng) {
        mMap.clear();
        MarkerOptions markerOptions1 = new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.red_marker_icon));
        mMap.addMarker(markerOptions1);
//        Geocoder geocoder = new Geocoder(context);
//        try {
//            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
//            if (CommonUtils.isValidArraylist(addresses)) {
//                CommonUtils.logD("Address" + addresses.get(0).getAddressLine(0));
//                pinCode = addresses.get(0).getPostalCode();
//                locationEt.setText(addresses.get(0).getAddressLine(0));
//                latitude = latLng.latitude;
//                longitude = latLng.longitude;
//                new GetPincodeServiceableData().execute();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    private boolean checkGPSStatus() {
        boolean isGpsTurnedon = false;
        LocationManager locationManager = null;
        boolean gps_enabled = false;
        boolean network_enabled = false;
        if (locationManager == null) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        }
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        if (!gps_enabled && !network_enabled) {
            isGpsTurnedon = false;
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setCancelable(false);
            dialog.setMessage(getString(R.string.turn_on_gps));
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //this will navigate user to the device location settings screen
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                    dialog.dismiss();
                }
            });
            AlertDialog alert = dialog.create();
            alert.show();
        } else
            isGpsTurnedon = true;

        return isGpsTurnedon;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if (nodePopup.getVisibility() == View.VISIBLE)
                nodePopup.setVisibility(View.GONE);
            else if (subProjectPopup.getVisibility() == View.VISIBLE)
                subProjectPopup.setVisibility(View.GONE);
            else
                finish();
        }
        return true;
    }

    @Override
    protected void onResume() {

        checkGPSStatus();


        super.onResume();
    }

    class GetSubProject extends AsyncTask<Void, Void, Void> {
        SubprojectResponse subprojectResponse;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CommonUtils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                String url = "";

                url = LMDUtils.getSubprojectUrlTag();


                InputStream response = HttpRequest.getInputStreamFromUrl(url, null, null);
                subprojectResponse = (SubprojectResponse) CommonUtils.parseResponse(response, SubprojectResponse.class);
                if (response != null) {
                    CommonUtils.logD(subprojectResponse.toString());
                    if (subprojectResponse.getStatus()) {
                        subProjectList = subprojectResponse.getData();
                    } else {
                        Globals.lastErrMsg = subprojectResponse.getMessage();
                    }
                }


            } catch (Exception e) {
                if (!CommonUtils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }


            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            CommonUtils.dismissProgressDialog();
            CommonUtils.showErrorDialog(context);
            if (subprojectResponse != null && subprojectResponse.getStatus()) {
                if (CommonUtils.isValidArraylist(subprojectResponse.getData())) {
                    subProjectPopup.setVisibility(View.VISIBLE);
                    subProjectSearchEt.setText("");
                    subProjectAdapter = new SubProjectAdapter(context, R.layout.sub_project_list_item, subprojectResponse.getData());
                    subProjectLv.setAdapter(subProjectAdapter);

                }

            }
        }
    }

    class GetNodes extends AsyncTask<Integer, Void, Void> {
        GetNodeResponse getNodeResponse;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CommonUtils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(Integer... params) {

            try {

                Integer id = params[0];
                String url = "";

                url = LMDUtils.getNodeUrlTag(String.valueOf(id));


                InputStream response = HttpRequest.getInputStreamFromUrl(url, null, null);
                getNodeResponse = (GetNodeResponse) CommonUtils.parseResponse(response, GetNodeResponse.class);
                if (response != null) {
                    CommonUtils.logD(getNodeResponse.toString());
                    if (getNodeResponse.getStatus()) {
                        nodeModelList = getNodeResponse.getData();
                    } else {
                        Globals.lastErrMsg = getNodeResponse.getMessage();
                    }
                }


            } catch (Exception e) {
                if (!CommonUtils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }


            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            CommonUtils.dismissProgressDialog();
            CommonUtils.showErrorDialog(context);
            if (getNodeResponse != null && getNodeResponse.getStatus()) {
                if (CommonUtils.isValidArraylist(nodeModelList)) {
//                    nodePopup.setVisibility(View.VISIBLE);
//                    NodeAdapter adapter = new NodeAdapter(context, R.layout.sub_project_list_item, nodeModelList);
//                    nodeLv.setAdapter(adapter);

                }

            }
        }
    }

    private class SubProjectAdapter extends ArrayAdapter<SubProjectModel> {
        private int layoutId;
        private LayoutInflater inflater;
        private List<SubProjectModel> modelList;

        public SubProjectAdapter(Context context, int layoutId, List<SubProjectModel> modelList) {
            super(context, 0, modelList);
            this.layoutId = layoutId;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.modelList = modelList;

        }

//        @Override
//        public int getViewTypeCount() {
//
//            return getCount();
//        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = inflater.inflate(layoutId, parent, false);
                holder = new ViewHolder();
                holder.subProjectTv = (TextView) convertView.findViewById(R.id.sub_project_name_tv);


                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            SubProjectModel model = (modelList.get(position));

            if (CommonUtils.isValidString(model.getName()))
                holder.subProjectTv.setText(model.getName());


            return convertView;
        }

        public class ViewHolder {
            public TextView subProjectTv;
        }
    }

    private class NodeAdapter extends ArrayAdapter<NodeModel> {
        private int layoutId;
        private LayoutInflater inflater;
        private List<NodeModel> modelList;

        public NodeAdapter(Context context, int layoutId, List<NodeModel> modelList) {
            super(context, 0, modelList);
            this.layoutId = layoutId;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.modelList = modelList;

        }

//        @Override
//        public int getViewTypeCount() {
//
//            return getCount();
//        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = inflater.inflate(layoutId, parent, false);
                holder = new ViewHolder();
                holder.subProjectTv = (TextView) convertView.findViewById(R.id.sub_project_name_tv);


                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            NodeModel model = (modelList.get(position));

            if (CommonUtils.isValidString(model.getNodeName()))
                holder.subProjectTv.setText(model.getNodeName());
            if (model.isCaptured()) {
                CommonUtils.logD(model.getNodeName());
                holder.subProjectTv.setTextColor(getResources().getColor(R.color.white));
                convertView.setBackgroundColor(getResources().getColor(R.color.green));
            } else {
                holder.subProjectTv.setTextColor(getResources().getColor(R.color.orange));
                convertView.setBackgroundColor(getResources().getColor(R.color.white));

            }


            return convertView;
        }

        public class ViewHolder {
            public TextView subProjectTv;
        }
    }

    class UpdateNodeLocation extends AsyncTask<NodeLocationModel, Void, Void> {
        GeneralResponse response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CommonUtils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(NodeLocationModel... params) {

            String loginUrl = LMDUtils.getUpdateNodeUrlTag();

            NodeLocationModel nodeLocationModel = params[0];

            ObjectMapper mapper = new ObjectMapper();

            try {

                String data = mapper.writeValueAsString(nodeLocationModel);
                CommonUtils.logD(nodeLocationModel.toString());
                response = (GeneralResponse) HttpRequest.postData(loginUrl, data, Constants.CONTENT_TYPE, HttpRequest.CONTENT_TYPE, GeneralResponse.class);

                if (response != null) {
                    CommonUtils.logD(response.toString());
                    if (response.getStatus()) {
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }


                }
            } catch (RuntimeException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (LastMileDeliveryException e) {
                e.printStackTrace();
            }


            return null;

        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            CommonUtils.dismissProgressDialog();
            CommonUtils.showErrorDialog(context);
            if (response != null && response.getStatus()) {
                CommonUtils.showToastShort(context, response.getMessage());
                subProjectTv.setText(getText(R.string.select_subproject));
                nodeNameTv.setText(getText(R.string.select_node));
                subProjectId = -1;
                nodeId = -1;
                latLng = null;


            }
        }

    }


}
