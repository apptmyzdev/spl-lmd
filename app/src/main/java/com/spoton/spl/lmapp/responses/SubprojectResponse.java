package com.spoton.spl.lmapp.responses;

import com.spoton.spl.lmapp.models.SubProjectModel;

import java.util.List;

public class SubprojectResponse {
    private Boolean status;
    private String  message;
    private List<SubProjectModel> data;
    private int  statusCode;
    private String  token;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SubProjectModel> getData() {
        return data;
    }

    public void setData(List<SubProjectModel> data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "SubprojectResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", statusCode=" + statusCode +
                ", token='" + token + '\'' +
                '}';
    }
}
