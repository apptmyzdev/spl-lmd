package com.spoton.spl.lmapp.models;

import java.util.List;

public class SubProjectModel {
	private Integer subProjectId;
	private String name;
	private Integer projectId;
	private String projectName;
	private Integer projectTypeId;
	private String projectType;
	private String location;
	private Integer revenueType;
	private String revenueDetails;
	private String stateCode;
	private String state;
	private String city;
	private int userId;
	private String createdBy;
	private String createdTimestamp;
	private String updatedBy;
	private String updatedTimestamp;
	private Integer branch;
	private String branchName;
	private String saacCode;
	private String description;
	private double discountPercent;

	private int revenueId;
	private String customerName;
	private String revenueTypeName;

	public Integer getSubProjectId() {
		return subProjectId;
	}

	public void setSubProjectId(Integer subProjectId) {
		this.subProjectId = subProjectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Integer getProjectTypeId() {
		return projectTypeId;
	}

	public void setProjectTypeId(Integer projectTypeId) {
		this.projectTypeId = projectTypeId;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getRevenueType() {
		return revenueType;
	}

	public void setRevenueType(Integer revenueType) {
		this.revenueType = revenueType;
	}

	public String getRevenueDetails() {
		return revenueDetails;
	}

	public void setRevenueDetails(String revenueDetails) {
		this.revenueDetails = revenueDetails;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(String createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedTimestamp() {
		return updatedTimestamp;
	}

	public void setUpdatedTimestamp(String updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}

	public Integer getBranch() {
		return branch;
	}

	public void setBranch(Integer branch) {
		this.branch = branch;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getSaacCode() {
		return saacCode;
	}

	public void setSaacCode(String saacCode) {
		this.saacCode = saacCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(double discountPercent) {
		this.discountPercent = discountPercent;
	}

	public int getRevenueId() {
		return revenueId;
	}

	public void setRevenueId(int revenueId) {
		this.revenueId = revenueId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getRevenueTypeName() {
		return revenueTypeName;
	}

	public void setRevenueTypeName(String revenueTypeName) {
		this.revenueTypeName = revenueTypeName;
	}

	@Override
	public String toString() {
		return "SubProjectModel{" +
				"subProjectId=" + subProjectId +
				", name='" + name + '\'' +
				", projectId=" + projectId +
				", projectName='" + projectName + '\'' +
				", projectTypeId=" + projectTypeId +
				", projectType='" + projectType + '\'' +
				", location='" + location + '\'' +
				", revenueType=" + revenueType +
				", revenueDetails='" + revenueDetails + '\'' +
				", stateCode='" + stateCode + '\'' +
				", state='" + state + '\'' +
				", city='" + city + '\'' +
				", userId=" + userId +
				", createdBy='" + createdBy + '\'' +
				", createdTimestamp='" + createdTimestamp + '\'' +
				", updatedBy='" + updatedBy + '\'' +
				", updatedTimestamp='" + updatedTimestamp + '\'' +
				", branch=" + branch +
				", branchName='" + branchName + '\'' +
				", saacCode='" + saacCode + '\'' +
				", description='" + description + '\'' +
				", discountPercent=" + discountPercent +
				", revenueId=" + revenueId +
				", customerName='" + customerName + '\'' +
				", revenueTypeName='" + revenueTypeName + '\'' +
				'}';
	}
}
