package com.spoton.spl.lmapp.GeoFencing;


import android.content.Context;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.spoton.spl.lmapp.models.LatLng;
import com.spoton.spl.lmapp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class SetGeofence {

    ArrayList<Geofence> mGeofences;
    ArrayList<LatLng> mGeofenceCoordinates;
    ArrayList<Integer> mGeofenceRadius;

    List<GeoFenceModel> geoFenceModels;
    GeofenceStore mGeofenceStore;


    public void performAction(Context context, List<GeoFenceModel> assignedGeofences) {
        geoFenceModels = assignedGeofences;
        if (geoFenceModels.size() != 0) {
            configure(context);
        }
    }

    public void configure(Context ctx) {
        Context context = ctx;
        Log.d("GeofenceDatas", geoFenceModels.toString());

        mGeofences = new ArrayList<Geofence>();
        mGeofenceCoordinates = new ArrayList<LatLng>();
        mGeofenceRadius = new ArrayList<Integer>();

        for (int i = 0; i < geoFenceModels.size(); i++) {
            GeoFenceModel gd = geoFenceModels.get(i);
            String nodeId = gd.getNodeId() + "";
            String dktNodeId = gd.getDktNodeId() + "";
//            String dktNo = gd.getNodeId() + "";
            String lat = gd.getLatitude() + "";
            String lon = gd.getLongitude() + "";
            String radius = gd.getRadius() + "";

            if (nodeId.equals("") || lat.equals("") || lon.equals("") || radius.equals(""))
                continue;
            if (!checkIfDouble(lat, lon, radius)) continue;

// Initializing variables
            Double radi = Double.valueOf(radius);
            int radius_set = (int) Math.round(radi);

// Adding geofence coordinates to array.
            mGeofenceCoordinates.add(new LatLng(Double.valueOf(lat), Double.valueOf(lon)));
// Adding associated geofence radius' to array.
            mGeofenceRadius.add(radius_set);
            mGeofences.add(new Geofence.Builder()
                    .setRequestId(nodeId + dktNodeId)  //request id is combination of dktnodeid and node id
// The coordinates of the center of the geofence and the radius in meters.
                    .setCircularRegion(mGeofenceCoordinates.get(i).getLatitude(), mGeofenceCoordinates.get(i).getLongitude(), mGeofenceRadius.get(i).intValue())
                    .setExpirationDuration(Constants.GEOFENCE_EXPIRATION_DURATION)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT).build());
        }
        mGeofenceStore = new GeofenceStore(context, mGeofences, geoFenceModels, 0);
    }

    boolean checkIfDouble(String lat, String lon, String radius) {
        try {
            Double.parseDouble(lat);
            Double.parseDouble(lon);
            Double.parseDouble(radius);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }
}
