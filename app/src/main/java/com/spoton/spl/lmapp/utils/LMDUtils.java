package com.spoton.spl.lmapp.utils;

public class LMDUtils {
//    public static final String baseUrl = "https://espl.spoton.co.in/erts/api/lmd/";//"https://erts.spoton.co.in/erts/api/lmd/";
//    public static final String baseUrl = "https://erts.spoton.co.in/erts/api/lmd/";
    //"http://104.237.4.37:8080/erts/api/lmd/";
    public static final String baseUrl = "https://espl.spoton.co.in/spl/api/lmd/";
    public static final String deviceRegistrationUrlTag = "device/registration";
    public static final String pickupUpdateUrlTag = "update/tc/pktcount?dktNo=";
    //    public static final String updateNodeUrlTag = "update/node";
    public static final String locationUrlTag = "capture/tcLocation";
    public static final String endTripUrlTag = "end/trip";
    public static final String deliveyUrlTag = "node/delivery";
    public static final String arrivalUrlTag = "node/arrival";
    public static final String departureUrlTag = "node/departure";
    public static final String getNodeUrlTag = "getnodes?subproject=";
    public static final String getSubprojectUrlTag = "subproject";
    public static final String getUpdateNodeUrlTag = "update/location";
    public static final String getReasonCodeUrlTag = "delay/reasoncode";

    public static String getDeviceRegistration() {
        return baseUrl + deviceRegistrationUrlTag;
    }

    public static String getPickupUpdateUrlTag(String docketNo, String count) {
        return baseUrl + pickupUpdateUrlTag + docketNo + "&pktCount=" + count;
    }

//    public static String getUpdateNodeUrlTag() {
//        return baseUrl +updateNodeUrlTag;
//    }

    public static String getLocationUrlTag() {
        return baseUrl + locationUrlTag;
    }

    public static String getEndTripUrlTag(String geofence) {
        return baseUrl + endTripUrlTag + "?geofence=" + geofence;
    }

    public static String getDeliveyUrlTag() {
        return baseUrl + deliveyUrlTag;
    }

    public static String getArrivalUrlTag(String geofence) {
        return baseUrl + arrivalUrlTag + "?geofence=" + geofence;
    }

    public static String getDepartureUrlTag(String geofence) {
        return baseUrl + departureUrlTag + "?geofence=" + geofence;
    }

    public static String getNodeUrlTag(String subProjectId) {
        return baseUrl + getNodeUrlTag + subProjectId;
    }

    public static String getSubprojectUrlTag() {
        return baseUrl + getSubprojectUrlTag;
    }

    public static String getUpdateNodeUrlTag() {
        return baseUrl + getUpdateNodeUrlTag;
    }

    public static String getReasonCodeUrlTag() {
        return baseUrl + getReasonCodeUrlTag;
    }

    public static String getCaptureGeofenceUrl(String arrivalDepatureStr) {
        return baseUrl + "/capture/geofence/" + "/type/" + arrivalDepatureStr;
    }
}
